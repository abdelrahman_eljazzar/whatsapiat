﻿	<div class="jumbotron boxico reg section" id="AddList">
		<i class="fa fa-plus-square-o fa-2x"></i>
		<h4>تعديل قائمة ارسال</h4>
		<p class="boxdesc">
		صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال.
		</p>
		<form class="navbar-form" action="" method="post">
        <input type="hidden" class="form-control" name="id" value="{$ID}">
			<div class="form-group">
				<label>اسم القائمة</label><br/>
				<input type="text" class="form-control" name="title" value="{$group->Title}">
			</div>
			<button class="btn btn-lg btn-primary btn-block" type="submit">تعديل القائمة</button>
		</form>

	</div><!-- end id="AddList" -->
