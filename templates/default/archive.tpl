﻿{if $paging.Next gt 0}
<div class="col-md-12 text-center">
  <ul class="pagination text-center">
  {if $paging.Prev neq 0}
    <li><a href="{$paging.PageURL}{$paging.Prev}">&laquo;</a></li>
  {/if}
  {section start=$paging.Start loop=$paging.End+1 name=op}
    <li {if $smarty.section.op.index eq $paging.CurrtPage}class="active-page"{/if}>
    <a href="{$paging.PageURL}{$smarty.section.op.index}">{$smarty.section.op.index}</a></li>
  {/section}
  {if $paging.Next neq 0}
    <li><a href="{$paging.PageURL}{$paging.Next}">&raquo;</a></li>
  {/if}
  </ul>
</div>
{/if}