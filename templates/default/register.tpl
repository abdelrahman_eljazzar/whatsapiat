﻿<form  action="{$homeurl}/users/register" method="post" class="navbar-form" role="form" class="form-valid" id="open-ajax-form">
<div class="modal-body">
			<div class="form-group">
            	<label>الاهتمامات</label><br/>
			   <select name="cat_id" id="cat_id"  class="form-control" style="width: 221px">
                    <option value="" >اختر</option>
                    {section name=op loop=$groups}
                    <option value="{$groups[op].ID}">{$groups[op].Title}</option>
                    {/section}
                </select>
                <br/><label>نوعية الحساب</label><br/>
			   <select name="user_type" id="user_type"  class="form-control" style="width: 221px">
                  <option value="" >اختر</option>
                  <option value="1" >شركة ناشئة</option>
                  <option value="2" >مطور منفرد</option>
                </select>

               <br/> <label>نوعية التسجيل</label><br/>
				<select name="register_type" id="register_type"  class="form-control" style="width: 221px">
                    <option value="" >اختر</option>
                    <option value="1" >عضو يستخدم الحساب الخاص به</option>
                    <option value="2" >عضو عادى</option>
                    <option value="3" >شركه تريد رقم دعم فني خاص بها </option>
                </select>
			   <br/>	<label>اسمك الكامل</label><br/>
				<input type="text" class="form-control"  name="username">

				<br/><label>البريد الإلكتروني</label><br/>
				<input type="text" class="form-control"  name="email">

				<br/><label>رقم الجوال</label><br/>
				<input type="text" class="form-control" name="number">

                <br/><label>كلمة السر</label><br/>
				<input type="password" class="form-control"  name="password">

                <br/><label>كلمة السر مره اخرى</label><br/>
				<input type="password" class="form-control"  name="pass2">
			</div>
</div>
<div class="modal-footer">
            <button class="btn btn-lg btn-primary btn-block" type="submit">حفظ البيانات</button>
             <img src="{$image_path}/loading_balls.gif" class="open-ajax-submit" style="display:none;" />
</div>
		</form>
