﻿	<div class="row section">
	<div class="col-md-4">
		<div class="plan-type boxico"><i class="fa fa-list-alt fa-3x"></i>
		<h3>ﺧﻄﺔ اﻟﺤﺴﺎﺏ</h3>
		<h2>{$user_info.Plan_Title}</h2>
		</div>
	</div>
	<div class="col-md-4">
		<div class="plan-points boxico"><i class="fa fa-clipboard fa-3x"></i>
		<h3>ﻋﺪﺩ اﻟﻨﻘﺎﻁ ﻓﻲ اﻟﺤﺴﺎﺏ</h3>
		<h2>{$user_info.Points} نقطة</h2>
		</div>
   </div>
	<div class="col-md-4">
		<div class="plan-available boxico"><i class="fa fa-file-text-o fa-3x"></i>
		<h3>ﻋﺪﺩ اﻟﻨﻘﺎﻁ اﻟﻤﺘﻮﻓﺮﺓ</h3>
		<h2>{$user_info.Rest_Points} نقطة</h2>
		</div>
	</div>
	</div><!-- end class="row" -->


	<div class="jumbotron boxico reg section" id="Messages">
	<i class="fa fa-print fa-2x"></i>
	<h4>واتسبيات</h4>

	<p class="boxdesc">
	صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال.
	</p>

<!--****************************************************************************************-->
	<div id="carousel-messages" class="slide col-md-11 multitems" data-ride="carousel">
	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<div class="item active">
			<div class="col-md-4"><div class="col-in">
				<h4><i class="fa fa-comment"></i>عنوان الرسالة</h4>
				<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها</p>
				<hr>
				<a href="#" class="category btn-sm btn fa fa-folder-open">رسائل إسلامية</a>
				<a href="#" class="btn btn-snd btn-md btn-primary">ارسال</a>
				<div class="clear"></div>
			</div></div>
			<div class="col-md-4"><div class="col-in">
				<h4><i class="fa fa-comment"></i>عنوان الرسالة</h4>
				<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها</p>
				<hr>
				<a href="#" class="category btn-sm btn fa fa-folder-open">رسائل إسلامية</a>
				<a href="#" class="btn btn-snd btn-md btn-primary">ارسال</a>
				<div class="clear"></div>
			</div></div>
			<div class="col-md-4"><div class="col-in">
				<h4><i class="fa fa-comment"></i>عنوان الرسالة</h4>
				<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها</p>
				<hr>
				<a href="#" class="category btn-sm btn fa fa-folder-open">رسائل إسلامية</a>
				<a href="#" class="btn btn-snd btn-md btn-primary">ارسال</a>
				<div class="clear"></div>
			</div></div>
		</div>


		<div class="item">
			<div class="col-md-4"><div class="col-in">
				<h4><i class="fa fa-comment"></i>عنوان الرسالة</h4>
				<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها</p>
				<hr>
				<a href="#" class="category btn-sm btn fa fa-folder-open">رسائل إسلامية</a>
				<a href="#" class="btn btn-snd btn-md btn-primary">ارسال</a>
				<div class="clear"></div>
			</div></div>
			<div class="col-md-4"><div class="col-in">
				<h4><i class="fa fa-comment"></i>عنوان الرسالة</h4>
				<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها</p>
				<hr>
				<a href="#" class="category btn-sm btn fa fa-folder-open">رسائل إسلامية</a>
				<a href="#" class="btn btn-snd btn-md btn-primary">ارسال</a>
				<div class="clear"></div>
			</div></div>
			<div class="col-md-4"><div class="col-in">
				<h4><i class="fa fa-comment"></i>عنوان الرسالة</h4>
				<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها</p>
				<hr>
				<a href="#" class="category btn-sm btn fa fa-folder-open">رسائل إسلامية</a>
				<a href="#" class="btn btn-snd btn-md btn-primary">ارسال</a>
				<div class="clear"></div>
			</div></div>
		</div>


		<div class="item">
			<div class="col-md-4"><div class="col-in">
				<h4><i class="fa fa-comment"></i>عنوان الرسالة</h4>
				<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها</p>
				<hr>
				<a href="#" class="category btn-sm btn fa fa-folder-open">رسائل إسلامية</a>
				<a href="#" class="btn btn-snd btn-md btn-primary">ارسال</a>
				<div class="clear"></div>
			</div></div>
			<div class="col-md-4"><div class="col-in">
				<h4><i class="fa fa-comment"></i>عنوان الرسالة</h4>
				<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها</p>
				<hr>
				<a href="#" class="category btn-sm btn fa fa-folder-open">رسائل إسلامية</a>
				<a href="#" class="btn btn-snd btn-md btn-primary">ارسال</a>
				<div class="clear"></div>
			</div></div>
			<div class="col-md-4"><div class="col-in">
				<h4><i class="fa fa-comment"></i>عنوان الرسالة</h4>
				<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها</p>
				<hr>
				<a href="#" class="category btn-sm btn fa fa-folder-open">رسائل إسلامية</a>
				<a href="#" class="btn btn-snd btn-md btn-primary">ارسال</a>
				<div class="clear"></div>
			</div></div>
		</div>

		<div class="clear"></div>
	</div>

	<!-- Controls -->
	<a class="left carousel-control fa fa-angle-left" href="#carousel-messages" data-slide="prev">
	</a>
	<a class="right carousel-control fa fa-angle-right" href="#carousel-messages" data-slide="next">
	</a>
	</div>

<!--****************************************************************************************-->
	<div clas="clear"></div>

	</div><!-- end id="Messages" -->

	<div class="jumbotron boxico reg section" id="SingleMessage">
	<i class="fa glyphicon glyphicon-envelope fa-2x"></i>
	<h4>إرسال رسالة</h4>

	<p class="boxdesc">
	صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال.
	</p>

	<p class="alert alert-info">ﻹﺭﺳﺎﻝ ﺻﻮﺭﺓ، ﺻﻮﺕ، ﻓﻴﺪﻳﻮ، ﺃﻭ ﻣﻮﻗﻊ ﻋﻠﻰ اﻟﺨﺮﻳﻄﺔ - اﺭﺳﻞ ﺭﺳﺎﻟﺔ ﻓﺎﺭﻏﺔ ﻣﻦ ﺣﺴﺎﺏ WhatsApp اﻟﺨﺎﺹ ﺑﻚ ﺇﻟﻰ اﻟﺮﻗﻢ 00966534306167 ﻣﻦ ﺛﻢ اﺗﺒﻊ اﻟﺘﻌﻠﻴﻤﺎﺕ.<br />
	ملحوظة: يمكن إرسال صورة أو نص في رسالة منفردة بقيمة رسالة واحدة و يمكن إرسال صورة و نص معا ولكن تكون قيمة الرسالة مضاعفة.</p>

	<form class="navbar-form" action="{$homeurl}/users/send_message" method="post">
		<div class="form-group">
			<label>إلى</label><br/>
			<input type="text" class="form-control" name="number">

			<label>نص الرسالة</label><br/>
			<textarea type="text" id="ta_SingleMessage" class="form-control charcount" placeholder="(4000 اﻟﺤﺪ اﻷﻗﺼﻰ ﻟﻌﺪﺩ اﻟﺤﺮﻭﻑ ﻓﻲ اﻟﺮﺳﺎﻟﺔ .) *"name="message"></textarea>
		</div>
		<h6 class="pull-right count_message" id="count-ta_SingleMessage"></h6>
		<button class="btn btn-lg btn-primary btn-block" type="submit">إرسال رسالة</button>
	</form>

	</div><!-- end id="SingleMessage" -->



