﻿	<footer id="footer" class="clear">
		<hr />
		<p style="float:right;margin:0px;"><span>شبكة واتس آب .</span> إحدى شبكات أرتجن</p>

		<ul class="socialicos">
			<li><a href="#" target="_blank" class="fa fa-rss"></a></li>
			<li><a href="#" target="_blank" class="fa fa-facebook"></a></li>
			<li><a href="#" target="_blank" class="fa fa-twitter"></a></li>
			<li><a href="#" target="_blank" class="fa fa-youtube"></a></li>
			<li><a href="#" target="_blank" class="fa fa-linkedin"></a></li>
			<li><a href="#" target="_blank" class="fa fa-google-plus"></a></li>
			<div class="clear"></div>
		</ul>
		<div class="clear"></div>

	</footer>

</section><!-- id="content" -->

<aside id="sidebar" class="col-sm-3 col-md-2 sidebar">
	<div class="widget" id="logo"><a href="{$homeurl}/home">لوحة التحكم</a></div>

	<div class="widget" id="member">
		<div class="icon-member fa fa-user fa-3x"></div>
		<p>
			<strong>{$smarty.session.account_name}</strong>
			<br>
		   {$user_info->Plan_Title}
		</p>

	</div>

<div class="panel-group widget" id="accordion">
<div {if $method eq 'account_conclusion' || $method eq 'charge' || $method eq 'transfer' || $method eq 'log'|| $module eq 'home'} class="panel active" {else} class="panel" {/if} id="panel-one">
<div class="panel-heading">
	<a class="panel-title fa fa-suitcase" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">اﻟﺤﺴﺎﺑﺎﺕ ﻭ اﻟﻨﻘﺎﻁ</a>
</div>
<div id="collapseOne" {if $method eq 'account_conclusion' || $method eq 'charge' || $method eq 'transfer' || $method eq 'log'|| $module eq 'home'} class="panel-collapse collapse in" {else} class="panel-collapse collapse" {/if}>
	<ul class="panel-body">
		<li><a href="{$homeurl}/users/account_conclusion">ﻣﻠﺨﺺ اﻟﺤﺴﺎﺏ</a></li>
		<li><a href="{$homeurl}/users/charge">شحن رصيد</a></li>
		<li><a href="{$homeurl}/users/transfer">ﺗﺤﻮﻳﻞ اﻟﻨﻘﺎﻁ</a></li>
		<li><a href="{$homeurl}/users/log">ﺗﻔﺎﺻﻴﻞ اﻟﻌﻤﻠﻴﺎﺕ</a></li>
	</ul>
</div>
</div>

<div class="panel" id="panel-two">
	<div class="panel-heading">
	<a class="panel-title fa fa-print" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">واتسبيات</a>
	</div>

	<div id="collapseTwo" class="panel-collapse collapse">
		<div class="panel-body">
		هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق،
		</div>
	</div>
</div>

<div  id="panel-three" {if $method eq 'send_group' || $method eq 'send_message' || $method eq 'archive' || $method eq 'notification'} class="panel active" {else} class="panel" {/if} >
	<div class="panel-heading">
		<a class="panel-title fa fa-envelope" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
		  ﺇﺭﺳﺎﻝ اﻟﺮﺳﺎﺋﻞ
		</a>
	</div>
	<div id="collapseThree" {if $method eq 'send_group' || $method eq 'send_message' || $method eq 'archive' || $method eq 'notification'} class="panel-collapse collapse in" {else} class="panel-collapse collapse" {/if} >
	   <ul class="panel-body">
		<li><a href="{$homeurl}/users/send_group"> ﺇﺭﺳﺎﻝ ﺇﻟﻰ ﻣﺠﻤﻮﻋﺔ </a></li>
		<li><a href="{$homeurl}/users/send_message">ﺇﺭﺳﺎﻝ ﺭﺳﺎﻟﺔ ﻓﺮﺩﻳﺔ </a></li>
		<li><a href="{$homeurl}/users/archive"> ﺃﺭﺷﻴﻒ اﻟﺮﺳﺎﺋﻞ </a></li>
		<li><a href="{$homeurl}/users/notification"> ﺭﺳﺎﺋﻞ اﻹﺑﻼﻍ </a></li>
	</ul>
	</div>
</div>
<div {if $method eq 'user_groups' || $method eq 'create_group'}class="panel active" {else} class="panel" {/if} id="panel-four">
	<div class="panel-heading">
		<a class="panel-title fa fa-file-text-o" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
		   ﺇﺩاﺭﺓ ﻗﻮاﺋﻢ اﻹﺭﺳﺎﻝ
		</a>
	</div>
	<div id="collapseFour" {if $method eq 'user_groups' || $method eq 'create_group'} class="panel-collapse collapse in" {else} class="panel-collapse collapse" {/if}>
		<div class="panel-body">
	  	<li><a href="{$homeurl}/users/user_groups">عرض القوائم</a></li>
        <li><a href="{$homeurl}/users/create_group">انشاء قائمة جدديدة</a></li>
		</div>
	</div>
</div>
<div {if $method eq 'editprofile'}class="panel active" {else} class="panel" {/if} id="panel-five">
	<div class="panel-heading">
		<a class="panel-title fa fa-desktop fa-2x" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
		  بيانات الحساب
		</a>
	</div>
	<div id="collapseFive" {if $method eq 'editprofile'} class="panel-collapse collapse in" {else} class="panel-collapse collapse" {/if} >
	   <div id="collapseOne" class="panel-collapse collapse in">
	<ul class="panel-body">
		<li><a href="{$homeurl}/users/editprofile">تعديل الحساب</a></li>
	</ul>
</div>
	</div>
</div>
<div class="panel" id="panel-six">
	<div class="panel-heading">
		<a class="panel-title fa fa-comment-o" href="#">
		  إتصل بنا
		</a>
	</div>
</div>

</div><!-- end id="accordion" -->

</aside>

</div><!-- end id="page" -->

<script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{$js_path}/main.js"></script>
{literal}
<script type="text/javascript">
$('#accordion').on('show.bs.collapse', function(e) {
	$(e.target).prev('.panel-heading').parent().addClass('active');
}).on('hide.bs.collapse', function(e) {
	$(e.target).prev('.panel-heading').parent().removeClass('active');
});




//////////////// char count start ////////////////
var text_max = 4000;
$('.count_message').html('الحروف المتبقية: '+text_max);

$('.charcount').keyup(function() {
	var text_id = $(this).attr('id');
	var text_length = $('.charcount').val().length;
	var text_remaining = text_max - text_length;
	$('#count-'+text_id).html('الحروف المتبقية: '+text_remaining);
});
//////////////// char count end ////////////////

$("#dashbody").height($("#page").height());
$("#sidebar").height($("#page").height()-20);
</script>
{/literal}

</body>

</html>