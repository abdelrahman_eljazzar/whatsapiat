﻿	<div class="jumbotron boxico reg section" id="GroupMessage">
	<i class="fa glyphicon glyphicon-envelope fa-2x"></i>
	<h4>إرسال رسالة جماعية</h4>

	<p class="boxdesc">
	صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال.
	</p>

	<p class="alert alert-info">ﻹﺭﺳﺎﻝ ﺻﻮﺭﺓ، ﺻﻮﺕ، ﻓﻴﺪﻳﻮ، ﺃﻭ ﻣﻮﻗﻊ ﻋﻠﻰ اﻟﺨﺮﻳﻄﺔ - اﺭﺳﻞ ﺭﺳﺎﻟﺔ ﻓﺎﺭﻏﺔ ﻣﻦ ﺣﺴﺎﺏ WhatsApp اﻟﺨﺎﺹ ﺑﻚ ﺇﻟﻰ اﻟﺮﻗﻢ 00966534306167 ﻣﻦ ﺛﻢ اﺗﺒﻊ اﻟﺘﻌﻠﻴﻤﺎﺕ.<br />
	ملحوظة: يمكن إرسال صورة أو نص في رسالة منفردة بقيمة رسالة واحدة و يمكن إرسال صورة و نص معا ولكن تكون قيمة الرسالة مضاعفة.</p>

	<form  action="" method="post">
    <div class="col-xs-12 col-md-8 navbar-form">
		<div class="form-group">
			<label>إرسال دعوة</label><br/>
			<input type="text" class="form-control">

			<label>نص الرسالة</label><br/>
			<textarea type="text" id="ta_SingleMessage" class="form-control charcount" placeholder="(4000 اﻟﺤﺪ اﻷﻗﺼﻰ ﻟﻌﺪﺩ اﻟﺤﺮﻭﻑ ﻓﻲ اﻟﺮﺳﺎﻟﺔ .) *"name="message"></textarea>
		</div>
        </div>
	<div class="col-md-5" id="gm_import">
	<a href="#" class="btn btn-md btn-link btn_c-grey">استيراد قوائم جاهزة</a>
	<a href="#" class="btn btn-md btn-link btn_c-default">القوائم الخاصة بي</a>
    <br/><br/><br/>
    {foreach from=$groups key=type item=group}
    <input type="checkbox" name="groups_send[]" value="{$group.ID}"/>{$group.Title}<br/>
     {/foreach}
	</div>
    <h6 class="pull-right count_message" id="count-ta_SingleMessage"></h6>
		<button class="btn btn-lg btn-primary btn-block" type="submit">إرسال رسالة</button>
</form>
	<div class="clear"></div>

	</div><!-- end id="GroupMessage" -->