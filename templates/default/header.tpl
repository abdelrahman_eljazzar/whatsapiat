﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" >
	<title>Dashboard - WhatsUp</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
	<!-- Optional theme -->
	<!--<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">-->

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

	<link href="{$css_path}/fonts/fonts.css" rel="stylesheet" type="text/css" />
	<link href="{$css_path}/fonts/font-awesome.min.css" rel="stylesheet" type="text/css" />

	<link href="{$css_path}/dashboard.css" rel="stylesheet" type="text/css" />

</head>

<body>

<div id="page" class="container-fluid">

<section id="dashbody" class="col-md-10 container" role="main">
	<header class="navbar-collapse collapse" id="header">
		<div class="dropdown">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="{$image_path}/demo-user_s.gif" />{$smarty.session.account_name}<b class="caret"></b></a>
			<ul class="dropdown-menu">
			<li><a href="#">Action</a></li>
			<li><a href="#">Another action</a></li>
			<li><a href="#">Something else here</a></li>
			<li class="divider"></li>
			<li class="dropdown-header">Nav header</li>
			<li><a href="#">Separated link</a></li>
			<li><a href="#">One more separated link</a></li>
			</ul>
		</div>

		<div class="headeropts">
			<a href="#" class="notifications fa fa-bell fa-1x"><i class="count">2</i></a>
			<a href="#" class="settings fa fa-cog fa-1x"></a>
			<a href="{$homeurl}/login/singout" class="logout fa fa-sign-out fa-1x"></a>
		</div>

		<!--<h3>Header - Project name</h3>-->
	</header>
    	<div class="jumbotron boxico main section" id="welcome">
		<i class="fa fa-microphone fa-4x"></i>
		<p class="boxdesc">
		صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال.

		<a href="{$homeurl}/users/charge" class="btn btn-md btn-link">شحن الرصيد</a>
		<div class="clear"></div>
		</p>
	</div>


