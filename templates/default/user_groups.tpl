﻿	<div class="jumbotron boxico reg section" id="ShowLists">
		<i class="fa fa-th-list fa-2x"></i>
		<h4>ﻋﺮﺽ ﻗﻮاﺋﻢ اﻹﺭﺳﺎﻝ</h4>
		<p class="boxdesc">
		صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال.
		</p>
		<div class="table-responsive">
         {if $groups}
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>التسلسل</th>
                  <th>اﺳﻢ ﻗﺎﺋﻤﺔ اﻹﺭﺳﺎﻝ</th>
                  <th>عدد الارقام</th>
                  <th>العمليات</th>
                </tr>
              </thead>
              <tbody>

              {assign var="i" value="1"}
              {foreach from=$groups key=type item=group}
                <tr>
					<td>{$i}</td>
					<td>{$group.Title}</td>
					<td>{$group.contacts_count}</td>
					<td>
						<ul class="opts">
							<li><a onclick="DeleteVer('{$homeurl}/users/delete_group?ID={$group.ID}')" class="opt-delete fa fa-trash-o" title="مسح"></a></li>
							<li><a href="{$homeurl}/users/edit_group?ID={$group.ID}" class="opt-edit fa fa-pencil" title="تعديل"></a></li>
							<li><a href="javascript:void(0);" class="opt-merge fa fa-plus-square-o" title="دمج"></a></li>
							<li><a href="javascript:void(0);" class="opt-clean fa fa-eraser" title="ترتيب"></a></li>
							<li><a href="javascript:void(0);" class="opt-export fa fa-upload" title="تصدير"></a></li>
							<li><a href="{$homeurl}/users/add_contacts?ID={$group.ID}" class="opt-add fa fa-plus-circle" title="إضافة"></a></li>
						</ul>
					</td>
                </tr>
                {assign var="i" value=$i+1}
                {/foreach}

              </tbody>
            </table>
             {else}
                عفوا ليس لديك اى قوائم
                {/if}
        </div>

	</div><!-- end id="ShowLists" -->