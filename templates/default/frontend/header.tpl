﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="{$css_path}/favicon.ico" type="image/x-icon" >
	<title>Homepage - WhatsUp</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<link href="{$css_path}/fonts/fonts.css" rel="stylesheet" type="text/css" />
	<link href="{$css_path}/fonts/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="{$css_path}/home.css" rel="stylesheet" type="text/css" />
    <link href="{$css_path}/dashboard.css" rel="stylesheet" type="text/css" />

</head>

<body>

<div id="page" class="container-fluid">
	<div id="prehead">
	<div class="container">
		<div id="contacts">
			<em class="fa fa-envelope">7oroof@7oroof.com</em>
			<em class="fa fa-phone" style="direction:ltr">+ 2 0106 5370701</em>
		</div>

		<ul class="socialicos">
			<li><a href="#" class="fa fa-rss" target="_blank"></a></li>
			<li><a href="#" class="fa fa-facebook" target="_blank"></a></li>
			<li><a href="#" class="fa fa-twitter" target="_blank"></a></li>
			<li><a href="#" class="fa fa-youtube" target="_blank"></a></li>
			<li><a href="#" class="fa fa-google-plus" target="_blank"></a></li>
			<li><a href="#" class="fa fa-linkedin" target="_blank"></a></li>
		</ul>

	</div>
	</div><!-- end id="prehead" -->

	<div id="header">
	<div class="container">
		<div class="col-md-5"><a href="{$homeurl}" class="logo">واتس آب</a></div>
		<div class="col-md-7">
		<ul class="nav navbar-nav">
			<li class="active"><a href="{$homeurl}">الرئيسية</a></li>
             {foreach from=$pages key=type item=page}
			<li><a href="{$homeurl}/home/display_page?ID={$page->ID}">{$page->Title}</a></li>
            {/foreach}
            <li><a href="#">إتصل بنا</a></li>
		</ul>
		<a href="{$homeurl}/users/register" class="open-ajax btn label-default " title="تسجيل عضو جديد" >تسجيل</a>
		<a href="{$homeurl}/login" class="open-ajax btn label-default " title="تسجيل دخول"  class="btn btn-primary">دخول</a>
		</div>
	</div>
	</div><!-- end id="header" -->

