﻿	<section id="carousel-main" class="carousel slide" data-ride="carousel">

	<div class="carousel-inner">
	<div class="item active">
		<img data-src="holder.js/1600x670/auto/#777:#555/text:First slide" alt="First slide" src="{$image_path}/carousel-main-00.jpg">
		<div class="carousel-caption">
		<h2>ابق على تواصل مع أقاربك وأصدقائك</h2>
		<p>صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة</p>
		<a href="#" class="btn label-default">اشترك الآن</a>
		<a href="#" class="btn btn-primary">أقرا المزيد</a>
      </div>
	</div>
	<div class="item">
		<img data-src="holder.js/1600x670/auto/#666:#444/text:Second slide" alt="Second slide" src="{$image_path}/carousel-main-01.jpg">
	</div>
	</div>
	<a class="left carousel-control" href="#carousel-main" role="button" data-slide="prev">
		<span class="fa fa-chevron-left"></span>
	</a>
	<a class="right carousel-control" href="#carousel-main" role="button" data-slide="next">
		<span class="fa fa-chevron-right"></span>
	</a>
	</section><!-- end id="carousel-main" -->


	<section id="Welcome" class="seceven">
		<div class="container">
			<h4 class="headline">أهلا بك فى واتس آب</h4>
			<p>صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال</p>
			<div id="carousel-welcome" class="slide container multitems" data-ride="carousel">

			<!-- Controls -->
			<a class="right carousel-control fa fa-angle-right" href="#carousel-welcome" data-slide="next"></a>
			<a class="left carousel-control fa fa-angle-left" href="#carousel-welcome" data-slide="prev"></a>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<div class="col-md-3"><div class="col-in">
						<h4><i class="fa fa-users"></i>إنشاء وحفظ قوائم العملاء</h4>
						<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.</p>
					</div></div>
					<div class="col-md-3"><div class="col-in">
						<h4><i class="fa fa-camera"></i>رسالة واضحة ومصورة</h4>
						<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.</p>
					</div></div>
					<div class="col-md-3"><div class="col-in">
						<h4><i class="fa fa-globe"></i>من أي مكان أو جهاز</h4>
						<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.</p>
					</div></div>
					<div class="col-md-3"><div class="col-in">
						<h4><i class="fa fa-paper-plane"></i>ضمان التسليم</h4>
						<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.</p>
					</div></div>
				</div>
				<div class="item">
					<div class="col-md-3"><div class="col-in">
						<h4><i class="fa fa-users"></i>إنشاء وحفظ قوائم العملاء</h4>
						<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.</p>
					</div></div>
					<div class="col-md-3"><div class="col-in">
						<h4><i class="fa fa-camera"></i>رسالة واضحة ومصورة</h4>
						<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.</p>
					</div></div>
					<div class="col-md-3"><div class="col-in">
						<h4><i class="fa fa-globe"></i>من أي مكان أو جهاز</h4>
						<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.</p>
					</div></div>
					<div class="col-md-3"><div class="col-in">
						<h4><i class="fa fa-paper-plane"></i>ضمان التسليم</h4>
						<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.</p>
					</div></div>
				</div>


				<div class="item">
					<div class="col-md-3"><div class="col-in">
						<h4><i class="fa fa-users"></i>إنشاء وحفظ قوائم العملاء</h4>
						<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.</p>
					</div></div>
					<div class="col-md-3"><div class="col-in">
						<h4><i class="fa fa-camera"></i>رسالة واضحة ومصورة</h4>
						<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.</p>
					</div></div>
					<div class="col-md-3"><div class="col-in">
						<h4><i class="fa fa-globe"></i>من أي مكان أو جهاز</h4>
						<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.</p>
					</div></div>
					<div class="col-md-3"><div class="col-in">
						<h4><i class="fa fa-paper-plane"></i>ضمان التسليم</h4>
						<p>هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.</p>
					</div></div>
				</div>

				<div class="clear"></div>
			</div>

			</div>




		</div>
	</section><!-- end id="welcome" -->

	<section id="Messages" class="secodd">
		<div class="container">
			<h4 class="headline">آخر الواتسبيات المضافة لدينا</h4>
			<p>صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال</p>

			<div id="carousel-messages" class="slide container multitems" data-ride="carousel">

			<!-- Controls -->
			<a class="right carousel-control fa fa-angle-right" href="#carousel-messages" data-slide="next"></a>
			<a class="left carousel-control fa fa-angle-left" href="#carousel-messages" data-slide="prev"></a>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
                {foreach from=$first_messages key=type item=message}
					<div class="col-md-3"><div class="col-in">
						<h4><i class="fa fa-comment"></i>{$message.Title}</h4>
						<p>{$message.Text}</p>
						<hr>
						<a href="#" class="category btn-sm btn fa fa-folder-open">{$message.Cat_Title}</a>
						<a href="#" class="btn btn-snd btn-md btn-primary">ارسال</a>
						<div class="clear"></div>
					</div></div>
                {/foreach}
				</div>
	    <div class="item">
                {foreach from=$second_messages key=type item=message}
					<div class="col-md-3"><div class="col-in">
						<h4><i class="fa fa-comment"></i>{$message.Title}</h4>
						<p>{$message.Text}</p>
						<hr>
						<a href="#" class="category btn-sm btn fa fa-folder-open">{$message.Cat_Title}</a>
						<a href="#" class="btn btn-snd btn-md btn-primary">ارسال</a>
						<div class="clear"></div>
					</div></div>
                {/foreach}
				</div>
				<div class="item">
                {foreach from=$third_messages key=type item=message}
					<div class="col-md-3"><div class="col-in">
						<h4><i class="fa fa-comment"></i>{$message.Title}</h4>
						<p>{$message.Text}</p>
						<hr>
						<a href="#" class="category btn-sm btn fa fa-folder-open">{$message.Cat_Title}</a>
						<a href="#" class="btn btn-snd btn-md btn-primary">ارسال</a>
						<div class="clear"></div>
					</div></div>
                {/foreach}
				</div>
				<div class="clear"></div>
			</div>

			</div>





		</div>
	</section><!-- end id="messages" -->

	<section class="secodd" id="Apps">
		<div class="container">
		<h3>تطبيقات مختلفة لتساعدك في عملية الارسال</h3>
		<p>صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف
		كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال</p>
		<a href="#" class="btn btn-lg btn-primary">اشترك الآن</a>
		</div>
	</section><!-- end id="Apps" -->

	<section id="pricing" class="seceven">
		<div class="container">
			<h4 class="headline">أسعار الباقات والرسائل</h4>
			<p>صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال</p>
			<div id="carousel-pricing" class="slide container multitems">

			<!-- Controls -->
			<a class="right carousel-control fa fa-angle-right" href="#carousel-pricing" data-slide="next"></a>
			<a class="left carousel-control fa fa-angle-left" href="#carousel-pricing" data-slide="prev"></a>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
                  {assign var="colors" value=','|explode:"color-green,color-orange,color-red,"}
                 {foreach from=$first_plans item=plan}
                  {assign var="rand_value" value=0|rand:3}
					<div class="col-md-3">
						<div class="panel panel-primary {$colors.$rand_value}">
							<div class="panel-heading">{$plan.Title}</div>
							<ul class="list-group">
								<li class="list-group-item price"><span>${$plan.Cost}</span> شهريا</li>
								<li class="list-group-item"><span>{$plan.Points}</span>نقطة</li>
								<li class="list-group-item"><a href="#" class="btn btn-primary btn-md">إشترك الآن</a></li>
							</ul>
						</div>
					</div>
                    {/foreach}
				</div><!-- end class="item" -->
				<div class="item">
                  {foreach from=$second_plans key=type item=plan}
                  {assign var="rand_value" value=0|rand:3}
				   	<div class="col-md-3">
						<div class="panel panel-primary {$colors.$rand_value}">
							<div class="panel-heading">{$plan.Title}</div>
							<ul class="list-group">
								<li class="list-group-item price"><span>${$plan.Cost}</span> شهريا</li>
								<li class="list-group-item"><span>{$plan.Points}</span>نقطة</li>
								<li class="list-group-item"><a href="#" class="btn btn-primary btn-md">إشترك الآن</a></li>
							</ul>
						</div>
					</div>
                     {/foreach}


				</div><!-- end class="item" -->

			</div>

			</div>
		</div>
	</section><!-- end id="pricing" -->

	<section id="testimonials" class="secodd">
		<div class="container">
			<h4 class="headline">عملائنا الكرام</h4>
			<p>صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال</p>


			<div class="col-sm-5 col-md-6">
			<div id="carousel-testimonials" class="col-sm-12 slide container multitems">

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<div class="col-xs-2 avatar"><img src="{$image_path}/avatar.jpg" /></div>
					<div class="feedback col-xs-10">
						<p>" افادتني الشركة بتصميم مواقعي بشكل احترافي واتقان وجودة وسرعة التنفيذ ورائعه بقدر ما اجد من فرق بينها وبين الشركات المماثلة . ليس شكرا ولا مجامله ولكن كل فريق العمل الموجود بالشركه يعد من اكثر الفرق المتعاونه في هذا المجال  و دائما يتطلع الي الافضل."
						</p>
						<h4 class="name">محمود عادل</h4>
						<h5 class="compname">شبكة حروف للتصميم</h5>
					</div>
					<div class="clear"></div>
				</div><!-- end class="item" -->
				<div class="item">
					<div class="col-xs-2 avatar"><img src="{$image_path}/avatar.jpg" /></div>
					<div class="feedback col-xs-10">
						<p>" افادتني الشركة بتصميم مواقعي بشكل احترافي واتقان وجودة وسرعة التنفيذ ورائعه بقدر ما اجد من فرق بينها وبين الشركات المماثلة . ليس شكرا ولا مجامله ولكن كل فريق العمل الموجود بالشركه يعد من اكثر الفرق المتعاونه في هذا المجال  و دائما يتطلع الي الافضل."
						</p>
						<h4 class="name">محمود عادل</h4>
						<h5 class="compname">شبكة حروف للتصميم</h5>
					</div>
					<div class="clear"></div>
				</div><!-- end class="item" -->
				<div class="item">
					<div class="col-xs-2 avatar"><img src="{$image_path}/avatar.jpg" /></div>
					<div class="feedback col-xs-10">
						<p>" افادتني الشركة بتصميم مواقعي بشكل احترافي واتقان وجودة وسرعة التنفيذ ورائعه بقدر ما اجد من فرق بينها وبين الشركات المماثلة . ليس شكرا ولا مجامله ولكن كل فريق العمل الموجود بالشركه يعد من اكثر الفرق المتعاونه في هذا المجال  و دائما يتطلع الي الافضل."
						</p>
						<h4 class="name">محمود عادل</h4>
						<h5 class="compname">شبكة حروف للتصميم</h5>
					</div>
					<div class="clear"></div>
				</div><!-- end class="item" -->
			</div>

			<!-- Controls -->
			<div class="carouselcontrolcontainer">
			<a class="right carousel-control fa fa-angle-right" href="#carousel-testimonials" data-slide="next"></a>
			<a class="left carousel-control fa fa-angle-left" href="#carousel-testimonials" data-slide="prev"></a>
			</div>

			</div>

			</div>

			<div class="col-sm-5 col-md-6" id="clients">
				<div class="col-xs-4 client"><div class="incol"><img src="{$image_path}/client01.jpg" /></div></div>
				<div class="col-xs-4 client"><div class="incol"><img src="{$image_path}/client02.jpg" /></div></div>
				<div class="col-xs-4 client"><div class="incol"><img src="{$image_path}/client03.jpg" /></div></div>
				<div class="col-xs-4 client"><div class="incol"><img src="{$image_path}/client01.jpg" /></div></div>
				<div class="col-xs-4 client"><div class="incol"><img src="{$image_path}/client02.jpg" /></div></div>
				<div class="col-xs-4 client"><div class="incol"><img src="{$image_path}/client03.jpg" /></div></div>

			</div>



		</div>
	</section><!-- end id="testimonials" -->
