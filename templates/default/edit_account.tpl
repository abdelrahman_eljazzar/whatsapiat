﻿<div class="jumbotron boxico reg section" id="EditInfo ">
		<i class="fa fa-user fa-2x"></i>
		<h4>ﺗﺤﺮﻳﺮ ﺑﻴﺎﻧﺎﺕ اﻟﺤﺴﺎﺏ</h4>
		<p class="boxdesc">
		صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال.
		</p>
		<form  action="editprofile" method="post" class="navbar-form">
			<div class="form-group">
				<label>اسمك الكامل</label><br/>
				<input type="text" class="form-control" value="{$user_info.Title}" name="username">

				<label>البريد الإلكتروني</label><br/>
				<input type="text" class="form-control" value="{$user_info.Email}" name="email">

				<label>رقم الجوال</label><br/>
				<input type="text" class="form-control" value="{$user_info.Number}" disabled="disabled" >

                <label>كلمة السر</label><br/>
				<input type="password" class="form-control" value="{$user_info.Number}" name="password">

                <label>كلمة السر مره اخرى</label><br/>
				<input type="password" class="form-control" value="{$user_info.Number}" name="pass2">
			</div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">حفظ البيانات</button>
		</form>

	</div><!-- end id="EditInfo" -->
