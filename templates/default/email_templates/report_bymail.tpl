<table style="font:bold 15px Arial;direction:rtl;width:600px;margin:auto;border: 1px solid #e5e5e5;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);-moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);box-shadow: 0 1px 2px rgba(0,0,0,.05);">
	<tr><td style="padding:5px;text-align:center;border-bottom: 1px solid #e5e5e5;padding:5px;"><a href="{$homeurl}"><img src="{$image_path}/logo.png" /></a></td></tr>
	<tr><td style="padding:5px;">مرحبا بك {$name},</td></tr>
	<tr><td style="padding:5px;">نرسل لك تقريرك اليومي بأخر التحديثات التي تمت على التنبيهات الخاصة بكم:</td></tr>
	<tr>
		<td>
		<table style="direction:rtl;border: 1px solid #e5e5e5;width: 100%;">
			<tr>
				<th style="font:bold 16px Arial;">التنبيه</th>
				<th style="font:bold 16px Arial;">نتائج جديدة</th>
				<th style="font:bold 16px Arial;">مطابقة</th>
				<th style="font:bold 16px Arial;">ايجابية</th>
				<th style="font:bold 16px Arial;">سلبية</th>
			</tr>
            {foreach from=$stats item=stat}
			<tr>
				<td style="text-align:center;font:bold 15px Arial;">{$stat.alert}</td>
				<td style="text-align:center;font:bold 15px Arial;">{$stat.new}</td>
				<td style="text-align:center;font:bold 15px Arial;">{$stat.smartfilter}</td>
				<td style="text-align:center;font:bold 15px Arial;">{$stat.positive}</td>
				<td style="text-align:center;font:bold 15px Arial;">{$stat.negative}</td>
			</tr>
            {/foreach}
		</table>
		</td>
	</tr>
</table>