<table style="font:bold 15px Arial;direction:rtl;width:600px;margin:auto;border: 1px solid #e5e5e5;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);-moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);box-shadow: 0 1px 2px rgba(0,0,0,.05);">
	<tr><td style="padding:5px;text-align:center;border-bottom: 1px solid #e5e5e5;padding:5px;"><a href="{$homeurl}"><img src="{$image_path}/logo.png" /></a></td></tr>
	<tr><td style="padding:5px;">مرحبا بك {$alert->name},</td></tr>
	<tr><td style="padding:5px;">نرسل اليك تقرير بالتنبيه الدوري الذي اخترته مسبقاً ان يصل اليك عن "{$alert->alertname}"</td></tr>
    {foreach from=$data item=mention}
    {assign var="content" value=$mention.content_encoded|base64_decode|unserialize}
	<tr>
		<td style="border-top:1px solid #ccc;padding:10px 0;font:bold 15px Arial;">
		<table style="direction:rtl;width:100%;">
			<tr>
				<td style="width:55px"><img src="{if empty($content.image)}{$image_path}/noimage.png{else}{$content.image}{/if}" width="50" height="50"></td>
				<td style="color:#333;text-decoration: none;"><a href="{$content.link}">{$content.title}</a></td>
			</tr>
            <tr>
				<td colspan="2">{$content.content|strip_tags|truncate:300}</td>
			</tr>
		</table>
		</td>
	</tr>
    {/foreach}
</table>