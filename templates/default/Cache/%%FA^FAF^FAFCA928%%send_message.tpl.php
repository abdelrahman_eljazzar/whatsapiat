<?php /* Smarty version 2.6.26, created on 2014-07-06 08:20:39
         compiled from send_message.tpl */ ?>
﻿
	<div class="jumbotron boxico reg section" id="SingleMessage">
	<i class="fa glyphicon glyphicon-envelope fa-2x"></i>
	<h4>إرسال رسالة</h4>

	<p class="boxdesc">
	صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال.
	</p>

	<p class="alert alert-info">ﻹﺭﺳﺎﻝ ﺻﻮﺭﺓ، ﺻﻮﺕ، ﻓﻴﺪﻳﻮ، ﺃﻭ ﻣﻮﻗﻊ ﻋﻠﻰ اﻟﺨﺮﻳﻄﺔ - اﺭﺳﻞ ﺭﺳﺎﻟﺔ ﻓﺎﺭﻏﺔ ﻣﻦ ﺣﺴﺎﺏ WhatsApp اﻟﺨﺎﺹ ﺑﻚ ﺇﻟﻰ اﻟﺮﻗﻢ 00966534306167 ﻣﻦ ﺛﻢ اﺗﺒﻊ اﻟﺘﻌﻠﻴﻤﺎﺕ.<br />
	ملحوظة: يمكن إرسال صورة أو نص في رسالة منفردة بقيمة رسالة واحدة و يمكن إرسال صورة و نص معا ولكن تكون قيمة الرسالة مضاعفة.</p>

	<form class="navbar-form" action="" method="post">
		<div class="form-group">
			<label>إلى</label><br/>
			<input type="text" class="form-control" name="number">

			<label>نص الرسالة</label><br/>
			<textarea type="text" id="ta_SingleMessage" class="form-control charcount" placeholder="(4000 اﻟﺤﺪ اﻷﻗﺼﻰ ﻟﻌﺪﺩ اﻟﺤﺮﻭﻑ ﻓﻲ اﻟﺮﺳﺎﻟﺔ .) *" name="message"></textarea>
		</div>
		<h6 class="pull-right count_message" id="count-ta_SingleMessage"></h6>
		<button class="btn btn-lg btn-primary btn-block" type="submit">إرسال رسالة</button>
	</form>

	</div><!-- end id="SingleMessage" -->