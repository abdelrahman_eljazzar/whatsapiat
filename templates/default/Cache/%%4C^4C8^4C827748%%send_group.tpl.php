<?php /* Smarty version 2.6.26, created on 2014-07-06 09:23:30
         compiled from send_group.tpl */ ?>
﻿	<div class="jumbotron boxico reg section" id="GroupMessage">
	<i class="fa glyphicon glyphicon-envelope fa-2x"></i>
	<h4>إرسال رسالة جماعية</h4>

	<p class="boxdesc">
	صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال.
	</p>

	<p class="alert alert-info">ﻹﺭﺳﺎﻝ ﺻﻮﺭﺓ، ﺻﻮﺕ، ﻓﻴﺪﻳﻮ، ﺃﻭ ﻣﻮﻗﻊ ﻋﻠﻰ اﻟﺨﺮﻳﻄﺔ - اﺭﺳﻞ ﺭﺳﺎﻟﺔ ﻓﺎﺭﻏﺔ ﻣﻦ ﺣﺴﺎﺏ WhatsApp اﻟﺨﺎﺹ ﺑﻚ ﺇﻟﻰ اﻟﺮﻗﻢ 00966534306167 ﻣﻦ ﺛﻢ اﺗﺒﻊ اﻟﺘﻌﻠﻴﻤﺎﺕ.<br />
	ملحوظة: يمكن إرسال صورة أو نص في رسالة منفردة بقيمة رسالة واحدة و يمكن إرسال صورة و نص معا ولكن تكون قيمة الرسالة مضاعفة.</p>

	<form  action="" method="post">
    <div class="col-xs-12 col-md-8 navbar-form">
		<div class="form-group">
			<label>إرسال دعوة</label><br/>
			<input type="text" class="form-control">

			<label>نص الرسالة</label><br/>
			<textarea type="text" id="ta_SingleMessage" class="form-control charcount" placeholder="(4000 اﻟﺤﺪ اﻷﻗﺼﻰ ﻟﻌﺪﺩ اﻟﺤﺮﻭﻑ ﻓﻲ اﻟﺮﺳﺎﻟﺔ .) *"name="message"></textarea>
		</div>
        </div>
	<div class="col-md-5" id="gm_import">
	<a href="#" class="btn btn-md btn-link btn_c-grey">استيراد قوائم جاهزة</a>
	<a href="#" class="btn btn-md btn-link btn_c-default">القوائم الخاصة بي</a>
    <br/><br/><br/>
    <?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['type'] => $this->_tpl_vars['group']):
?>
    <input type="checkbox" name="groups_send[]" value="<?php echo $this->_tpl_vars['group']['ID']; ?>
"/><?php echo $this->_tpl_vars['group']['Title']; ?>
<br/>
     <?php endforeach; endif; unset($_from); ?>
	</div>
    <h6 class="pull-right count_message" id="count-ta_SingleMessage"></h6>
		<button class="btn btn-lg btn-primary btn-block" type="submit">إرسال رسالة</button>
</form>
	<div class="clear"></div>

	</div><!-- end id="GroupMessage" -->