<?php /* Smarty version 2.6.26, created on 2014-07-06 06:42:42
         compiled from footer.tpl */ ?>
﻿	<footer id="footer" class="clear">
		<hr />
		<p style="float:right;margin:0px;"><span>شبكة واتس آب .</span> إحدى شبكات أرتجن</p>

		<ul class="socialicos">
			<li><a href="#" target="_blank" class="fa fa-rss"></a></li>
			<li><a href="#" target="_blank" class="fa fa-facebook"></a></li>
			<li><a href="#" target="_blank" class="fa fa-twitter"></a></li>
			<li><a href="#" target="_blank" class="fa fa-youtube"></a></li>
			<li><a href="#" target="_blank" class="fa fa-linkedin"></a></li>
			<li><a href="#" target="_blank" class="fa fa-google-plus"></a></li>
			<div class="clear"></div>
		</ul>
		<div class="clear"></div>

	</footer>

</section><!-- id="content" -->

<aside id="sidebar" class="col-sm-3 col-md-2 sidebar">
	<div class="widget" id="logo"><a href="<?php echo $this->_tpl_vars['homeurl']; ?>
/home">لوحة التحكم</a></div>

	<div class="widget" id="member">
		<div class="icon-member fa fa-user fa-3x"></div>
		<p>
			<strong><?php echo $_SESSION['account_name']; ?>
</strong>
			<br>
		   <?php echo $this->_tpl_vars['user_info']->Plan_Title; ?>

		</p>

	</div>

<div class="panel-group widget" id="accordion">
<div <?php if ($this->_tpl_vars['method'] == 'account_conclusion' || $this->_tpl_vars['method'] == 'charge' || $this->_tpl_vars['method'] == 'transfer' || $this->_tpl_vars['method'] == 'log' || $this->_tpl_vars['module'] == 'home'): ?> class="panel active" <?php else: ?> class="panel" <?php endif; ?> id="panel-one">
<div class="panel-heading">
	<a class="panel-title fa fa-suitcase" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">اﻟﺤﺴﺎﺑﺎﺕ ﻭ اﻟﻨﻘﺎﻁ</a>
</div>
<div id="collapseOne" <?php if ($this->_tpl_vars['method'] == 'account_conclusion' || $this->_tpl_vars['method'] == 'charge' || $this->_tpl_vars['method'] == 'transfer' || $this->_tpl_vars['method'] == 'log' || $this->_tpl_vars['module'] == 'home'): ?> class="panel-collapse collapse in" <?php else: ?> class="panel-collapse collapse" <?php endif; ?>>
	<ul class="panel-body">
		<li><a href="<?php echo $this->_tpl_vars['homeurl']; ?>
/users/account_conclusion">ﻣﻠﺨﺺ اﻟﺤﺴﺎﺏ</a></li>
		<li><a href="<?php echo $this->_tpl_vars['homeurl']; ?>
/users/charge">شحن رصيد</a></li>
		<li><a href="<?php echo $this->_tpl_vars['homeurl']; ?>
/users/transfer">ﺗﺤﻮﻳﻞ اﻟﻨﻘﺎﻁ</a></li>
		<li><a href="<?php echo $this->_tpl_vars['homeurl']; ?>
/users/log">ﺗﻔﺎﺻﻴﻞ اﻟﻌﻤﻠﻴﺎﺕ</a></li>
	</ul>
</div>
</div>

<div class="panel" id="panel-two">
	<div class="panel-heading">
	<a class="panel-title fa fa-print" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">واتسبيات</a>
	</div>

	<div id="collapseTwo" class="panel-collapse collapse">
		<div class="panel-body">
		هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق،
		</div>
	</div>
</div>

<div  id="panel-three" <?php if ($this->_tpl_vars['method'] == 'send_group' || $this->_tpl_vars['method'] == 'send_message' || $this->_tpl_vars['method'] == 'archive' || $this->_tpl_vars['method'] == 'notification'): ?> class="panel active" <?php else: ?> class="panel" <?php endif; ?> >
	<div class="panel-heading">
		<a class="panel-title fa fa-envelope" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
		  ﺇﺭﺳﺎﻝ اﻟﺮﺳﺎﺋﻞ
		</a>
	</div>
	<div id="collapseThree" <?php if ($this->_tpl_vars['method'] == 'send_group' || $this->_tpl_vars['method'] == 'send_message' || $this->_tpl_vars['method'] == 'archive' || $this->_tpl_vars['method'] == 'notification'): ?> class="panel-collapse collapse in" <?php else: ?> class="panel-collapse collapse" <?php endif; ?> >
	   <ul class="panel-body">
		<li><a href="<?php echo $this->_tpl_vars['homeurl']; ?>
/users/send_group"> ﺇﺭﺳﺎﻝ ﺇﻟﻰ ﻣﺠﻤﻮﻋﺔ </a></li>
		<li><a href="<?php echo $this->_tpl_vars['homeurl']; ?>
/users/send_message">ﺇﺭﺳﺎﻝ ﺭﺳﺎﻟﺔ ﻓﺮﺩﻳﺔ </a></li>
		<li><a href="<?php echo $this->_tpl_vars['homeurl']; ?>
/users/archive"> ﺃﺭﺷﻴﻒ اﻟﺮﺳﺎﺋﻞ </a></li>
		<li><a href="<?php echo $this->_tpl_vars['homeurl']; ?>
/users/notification"> ﺭﺳﺎﺋﻞ اﻹﺑﻼﻍ </a></li>
	</ul>
	</div>
</div>
<div <?php if ($this->_tpl_vars['method'] == 'user_groups' || $this->_tpl_vars['method'] == 'create_group'): ?>class="panel active" <?php else: ?> class="panel" <?php endif; ?> id="panel-four">
	<div class="panel-heading">
		<a class="panel-title fa fa-file-text-o" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
		   ﺇﺩاﺭﺓ ﻗﻮاﺋﻢ اﻹﺭﺳﺎﻝ
		</a>
	</div>
	<div id="collapseFour" <?php if ($this->_tpl_vars['method'] == 'user_groups' || $this->_tpl_vars['method'] == 'create_group'): ?> class="panel-collapse collapse in" <?php else: ?> class="panel-collapse collapse" <?php endif; ?>>
		<div class="panel-body">
	  	<li><a href="<?php echo $this->_tpl_vars['homeurl']; ?>
/users/user_groups">عرض القوائم</a></li>
        <li><a href="<?php echo $this->_tpl_vars['homeurl']; ?>
/users/create_group">انشاء قائمة جدديدة</a></li>
		</div>
	</div>
</div>
<div <?php if ($this->_tpl_vars['method'] == 'editprofile'): ?>class="panel active" <?php else: ?> class="panel" <?php endif; ?> id="panel-five">
	<div class="panel-heading">
		<a class="panel-title fa fa-desktop fa-2x" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
		  بيانات الحساب
		</a>
	</div>
	<div id="collapseFive" <?php if ($this->_tpl_vars['method'] == 'editprofile'): ?> class="panel-collapse collapse in" <?php else: ?> class="panel-collapse collapse" <?php endif; ?> >
	   <div id="collapseOne" class="panel-collapse collapse in">
	<ul class="panel-body">
		<li><a href="<?php echo $this->_tpl_vars['homeurl']; ?>
/users/editprofile">تعديل الحساب</a></li>
	</ul>
</div>
	</div>
</div>
<div class="panel" id="panel-six">
	<div class="panel-heading">
		<a class="panel-title fa fa-comment-o" href="#">
		  إتصل بنا
		</a>
	</div>
</div>

</div><!-- end id="accordion" -->

</aside>

</div><!-- end id="page" -->

<script type="text/javascript" language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['js_path']; ?>
/main.js"></script>
<?php echo '
<script type="text/javascript">
$(\'#accordion\').on(\'show.bs.collapse\', function(e) {
	$(e.target).prev(\'.panel-heading\').parent().addClass(\'active\');
}).on(\'hide.bs.collapse\', function(e) {
	$(e.target).prev(\'.panel-heading\').parent().removeClass(\'active\');
});




//////////////// char count start ////////////////
var text_max = 4000;
$(\'.count_message\').html(\'الحروف المتبقية: \'+text_max);

$(\'.charcount\').keyup(function() {
	var text_id = $(this).attr(\'id\');
	var text_length = $(\'.charcount\').val().length;
	var text_remaining = text_max - text_length;
	$(\'#count-\'+text_id).html(\'الحروف المتبقية: \'+text_remaining);
});
//////////////// char count end ////////////////

$("#dashbody").height($("#page").height());
$("#sidebar").height($("#page").height()-20);
</script>
'; ?>


</body>

</html>