<?php /* Smarty version 2.6.26, created on 2014-07-03 06:44:58
         compiled from frontend/footer.tpl */ ?>
﻿
	<footer id="footer">
		<div class="container">
		<p class="col-md-5"><span>شبكة واتس آب .</span> إحدى شبكات أرتجن</p>
		<ul class="socialicos">
			<li><a href="#" target="_blank" class="fa fa-rss"></a></li>
			<li><a href="#" target="_blank" class="fa fa-facebook"></a></li>
			<li><a href="#" target="_blank" class="fa fa-twitter"></a></li>
			<li><a href="#" target="_blank" class="fa fa-youtube"></a></li>
			<li><a href="#" target="_blank" class="fa fa-linkedin"></a></li>
			<li><a href="#" target="_blank" class="fa fa-google-plus"></a></li>
			<div class="clear"></div>
		</ul>
		<div class="clear"></div>
		</div><!-- end class="container" -->
	</div><!-- end id="footer" -->

</div><!-- end id="page" -->


<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo $this->_tpl_vars['js_path']; ?>
/jquery.js"></script>
<script src="<?php echo $this->_tpl_vars['js_path']; ?>
/bootstrap.min.js"></script>
<script src="<?php echo $this->_tpl_vars['js_path']; ?>
/jquery.validationEngine.js"></script>
<script src="<?php echo $this->_tpl_vars['js_path']; ?>
/jquery.easing.1.3.js"></script>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['js_path']; ?>
/main.js"></script>
<div class="modal fade" id="open-ajax-form">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <p class="open-ajax-loading"><img src="<?php echo $this->_tpl_vars['image_path']; ?>
/loadingball.gif" alt="" /></p>
      <div class="open-ajax-body"></div>
    </div>
  </div>
</div>
</body>

</html>