<?php /* Smarty version 2.6.26, created on 2014-07-03 07:00:09
         compiled from user_log.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'user_log.tpl', 24, false),)), $this); ?>
﻿	<div class="jumbotron boxico reg section" id="data">
		<i class="fa fa-copy fa-2x"></i>
		<h4>ﺗﻔﺎﺻﻴﻞ اﻟﻌﻤﻠﻴﺎﺕ</h4>
		<p class="boxdesc">
		صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال.
		</p>
		<div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>التسلسل</th>
                  <th>الوصف</th>
                  <th>النقاط</th>
                  <th>التاريخ</th>
                </tr>
              </thead>
              <tbody>
               <?php $this->assign('i', '1'); ?>
              <?php $_from = $this->_tpl_vars['logs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['type'] => $this->_tpl_vars['log']):
?>
                <tr>
                  <td><?php echo $this->_tpl_vars['i']; ?>
</td>
                  <td><?php echo $this->_tpl_vars['log']['Text']; ?>
</td>
                  <td><?php echo $this->_tpl_vars['log']['Points']; ?>
</td>
                  <td><?php echo ((is_array($_tmp=$this->_tpl_vars['log']['Date'])) ? $this->_run_mod_handler('date_format', true, $_tmp) : smarty_modifier_date_format($_tmp)); ?>
</td>
                </tr>
  <?php $this->assign('i', $this->_tpl_vars['i']+1); ?>
                <?php endforeach; endif; unset($_from); ?>

              </tbody>
            </table>
        </div>

	</div>