<?php /* Smarty version 2.6.26, created on 2014-07-06 09:26:42
         compiled from archive.tpl */ ?>
﻿<?php if ($this->_tpl_vars['paging']['Next'] > 0): ?>
<div class="col-md-12 text-center">
  <ul class="pagination text-center">
  <?php if ($this->_tpl_vars['paging']['Prev'] != 0): ?>
    <li><a href="<?php echo $this->_tpl_vars['paging']['PageURL']; ?>
<?php echo $this->_tpl_vars['paging']['Prev']; ?>
">&laquo;</a></li>
  <?php endif; ?>
  <?php unset($this->_sections['op']);
$this->_sections['op']['start'] = (int)$this->_tpl_vars['paging']['Start'];
$this->_sections['op']['loop'] = is_array($_loop=$this->_tpl_vars['paging']['End']+1) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['op']['name'] = 'op';
$this->_sections['op']['show'] = true;
$this->_sections['op']['max'] = $this->_sections['op']['loop'];
$this->_sections['op']['step'] = 1;
if ($this->_sections['op']['start'] < 0)
    $this->_sections['op']['start'] = max($this->_sections['op']['step'] > 0 ? 0 : -1, $this->_sections['op']['loop'] + $this->_sections['op']['start']);
else
    $this->_sections['op']['start'] = min($this->_sections['op']['start'], $this->_sections['op']['step'] > 0 ? $this->_sections['op']['loop'] : $this->_sections['op']['loop']-1);
if ($this->_sections['op']['show']) {
    $this->_sections['op']['total'] = min(ceil(($this->_sections['op']['step'] > 0 ? $this->_sections['op']['loop'] - $this->_sections['op']['start'] : $this->_sections['op']['start']+1)/abs($this->_sections['op']['step'])), $this->_sections['op']['max']);
    if ($this->_sections['op']['total'] == 0)
        $this->_sections['op']['show'] = false;
} else
    $this->_sections['op']['total'] = 0;
if ($this->_sections['op']['show']):

            for ($this->_sections['op']['index'] = $this->_sections['op']['start'], $this->_sections['op']['iteration'] = 1;
                 $this->_sections['op']['iteration'] <= $this->_sections['op']['total'];
                 $this->_sections['op']['index'] += $this->_sections['op']['step'], $this->_sections['op']['iteration']++):
$this->_sections['op']['rownum'] = $this->_sections['op']['iteration'];
$this->_sections['op']['index_prev'] = $this->_sections['op']['index'] - $this->_sections['op']['step'];
$this->_sections['op']['index_next'] = $this->_sections['op']['index'] + $this->_sections['op']['step'];
$this->_sections['op']['first']      = ($this->_sections['op']['iteration'] == 1);
$this->_sections['op']['last']       = ($this->_sections['op']['iteration'] == $this->_sections['op']['total']);
?>
    <li <?php if ($this->_sections['op']['index'] == $this->_tpl_vars['paging']['CurrtPage']): ?>class="active-page"<?php endif; ?>>
    <a href="<?php echo $this->_tpl_vars['paging']['PageURL']; ?>
<?php echo $this->_sections['op']['index']; ?>
"><?php echo $this->_sections['op']['index']; ?>
</a></li>
  <?php endfor; endif; ?>
  <?php if ($this->_tpl_vars['paging']['Next'] != 0): ?>
    <li><a href="<?php echo $this->_tpl_vars['paging']['PageURL']; ?>
<?php echo $this->_tpl_vars['paging']['Next']; ?>
">&raquo;</a></li>
  <?php endif; ?>
  </ul>
</div>
<?php endif; ?>