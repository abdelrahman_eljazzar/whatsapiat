<?php /* Smarty version 2.6.26, created on 2014-07-06 06:28:33
         compiled from user_groups.tpl */ ?>
﻿	<div class="jumbotron boxico reg section" id="ShowLists">
		<i class="fa fa-th-list fa-2x"></i>
		<h4>ﻋﺮﺽ ﻗﻮاﺋﻢ اﻹﺭﺳﺎﻝ</h4>
		<p class="boxdesc">
		صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال.
		</p>
		<div class="table-responsive">
         <?php if ($this->_tpl_vars['groups']): ?>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>التسلسل</th>
                  <th>اﺳﻢ ﻗﺎﺋﻤﺔ اﻹﺭﺳﺎﻝ</th>
                  <th>عدد الارقام</th>
                  <th>العمليات</th>
                </tr>
              </thead>
              <tbody>

              <?php $this->assign('i', '1'); ?>
              <?php $_from = $this->_tpl_vars['groups']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['type'] => $this->_tpl_vars['group']):
?>
                <tr>
					<td><?php echo $this->_tpl_vars['i']; ?>
</td>
					<td><?php echo $this->_tpl_vars['group']['Title']; ?>
</td>
					<td><?php echo $this->_tpl_vars['group']['contacts_count']; ?>
</td>
					<td>
						<ul class="opts">
							<li><a onclick="DeleteVer('<?php echo $this->_tpl_vars['homeurl']; ?>
/users/delete_group?ID=<?php echo $this->_tpl_vars['group']['ID']; ?>
')" class="opt-delete fa fa-trash-o" title="مسح"></a></li>
							<li><a href="<?php echo $this->_tpl_vars['homeurl']; ?>
/users/edit_group?ID=<?php echo $this->_tpl_vars['group']['ID']; ?>
" class="opt-edit fa fa-pencil" title="تعديل"></a></li>
							<li><a href="javascript:void(0);" class="opt-merge fa fa-plus-square-o" title="دمج"></a></li>
							<li><a href="javascript:void(0);" class="opt-clean fa fa-eraser" title="ترتيب"></a></li>
							<li><a href="javascript:void(0);" class="opt-export fa fa-upload" title="تصدير"></a></li>
							<li><a href="<?php echo $this->_tpl_vars['homeurl']; ?>
/users/add_contacts?ID=<?php echo $this->_tpl_vars['group']['ID']; ?>
" class="opt-add fa fa-plus-circle" title="إضافة"></a></li>
						</ul>
					</td>
                </tr>
                <?php $this->assign('i', $this->_tpl_vars['i']+1); ?>
                <?php endforeach; endif; unset($_from); ?>

              </tbody>
            </table>
             <?php else: ?>
                عفوا ليس لديك اى قوائم
                <?php endif; ?>
        </div>

	</div><!-- end id="ShowLists" -->