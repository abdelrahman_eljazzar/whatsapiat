﻿	<div class="jumbotron boxico reg section" id="data">
		<i class="fa fa-copy fa-2x"></i>
		<h4>ملخص الحساب</h4>
		<p class="boxdesc">
		صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال.
		</p>
		<div class="table-responsive">
            <table class="table table-striped">
              <tbody>
                <tr>
                  <td>نوع الخطة</td>
                  <td>{$user_info.Plan_Title}</td>
                </tr>
                <tr>
                  <td>ﺇﺟﻤﺎﻟﻲ ﻋﺪﺩ اﻟﻨﻘﺎﻁ ﻓﻲ اﻟﺤﺴﺎﺏ </td>
                  <td>{$user_info.Points}</td>
                </tr>
                 <tr>
                  <td>عدد النقاط المستخدمة</td>
                  <td>{$user_info.Used_Points}</td>
                </tr>
                 <tr>
                  <td>ﻋﺪﺩ اﻟﻨﻘﺎﻁ اﻟﻤﺘﻮﻓﺮﺓ </td>
                  <td>{$user_info.Rest_Points}</td>
                </tr>
              </tbody>
            </table>
        </div>

	</div>