﻿	<div class="jumbotron boxico reg section" id="data">
		<i class="fa fa-copy fa-2x"></i>
		<h4>ﺗﻔﺎﺻﻴﻞ اﻟﻌﻤﻠﻴﺎﺕ</h4>
		<p class="boxdesc">
		صممت هذه الخدمة خصيصا لتمكن المعلنين من إرسال الرسائل الجماعية والاعلانية وغير الاعلانية عبر برنامح واتس آب الشهير والذي سيمنح المرسل امكانية ارسال الصور والفيديو بالاضافة عدد حروف كبير ليمكن المرسل من تحقيق اكبر استفادة ممكنة من عملية الارسال.
		</p>
		<div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>التسلسل</th>
                  <th>الوصف</th>
                  <th>النقاط</th>
                  <th>التاريخ</th>
                </tr>
              </thead>
              <tbody>
               {assign var="i" value="1"}
              {foreach from=$logs key=type item=log}
                <tr>
                  <td>{$i}</td>
                  <td>{$log.Text}</td>
                  <td>{$log.Points}</td>
                  <td>{$log.Date|date_format}</td>
                </tr>
  {assign var="i" value=$i+1}
                {/foreach}

              </tbody>
            </table>
        </div>

	</div>