<?php
require_once("includes/config.php");

$thumb_filepath = 'thumbcache/'.$_GET['width'].'_'.$_GET['height'].'_'.$_GET['file'];
$_filepath = 'uploads/'.$_GET['file'];

if(file_exists($thumb_filepath)){
	header('Location: '.$realurl.'/'.$thumb_filepath);
}
elseif(file_exists($_filepath)){
	include_once('includes/PhpThumb/ThumbLib.inc.php');
	if($_GET['q']) $quality = $_GET['q'];
	else $quality = 100;

	$thumb = PhpThumbFactory::create($_filepath, array('jpegQuality' => $quality));
	$thumb->Resize($_GET['width'], $_GET['height'])->save($thumb_filepath);
	$thumb->show();
}
else{
	return '';
}

?>