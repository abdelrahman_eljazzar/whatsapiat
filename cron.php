<?php
header('Content-Type: text/html; charset=utf-8');
require_once("init.php");
include(INC_DIR.'/helper.class.php');

$lstcronid = $db->get_var("SELECT lstcronid FROM config WHERE id='1'");

$user = $db->get_row("SELECT * FROM users WHERE id>$lstcronid ORDER BY id ASC LIMIT 0,1");
if(!$user){
  $user = $db->get_row("SELECT * FROM users ORDER BY id ASC LIMIT 0,1");
}
$plan = $db->get_row("SELECT * FROM plans WHERE id='".$user->planid."'");
$db->query("UPDATE config SET lstcronid='".$user->id."' WHERE id='1'");
if($user->quota >= $plan->quota){
  die('Quota Exceeded');
}
if(TIMENOW < $user->lst_crontime+($plan->cron_ratio*60)){
  die('Time Exceeded');
}
$db->query("UPDATE users SET lst_crontime='".TIMENOW."' WHERE id='$user->id'");

$alerts = $db->get_results("SELECT * FROM alerts WHERE user_id='".$user->id."' ORDER BY RAND()");
if($alerts){
  foreach($alerts as $alert){
    $search = preg_split('/\r\n|[\r\n]/', $alert->search);
    foreach($search as $query){
      $alert->search = trim($query);
      $libs = scandir(DIGG_DIR);
      foreach($libs as $lib){
        if($lib != '.' && $lib != '..'){
          $class_file = $lib.'.php';
          $class_name = $lib.'Digger';
          include_once(DIGG_DIR.'/'.$lib.'/'.$class_file);
          $class = new $class_name($alert);
        }
      }
    }
  }
}
?>