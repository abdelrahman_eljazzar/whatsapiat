<?php
include('email_function.php');

function email_forget_pass($id){
	global $homeurl;
	global $config;

	$id = intval($id);
    $newpass = salt();
    $secpass = md5($newpass);
	mysql_query("UPDATE user SET password='$secpass' WHERE id='$id'");
    $sql = mysql_query("SELECT username,email FROM user WHERE id='$id'");
    $user = mysql_fetch_array($sql);

	$emailsql = mysql_query("SELECT * FROM email_templates WHERE id='1' AND active='1'");
	if(mysql_affected_rows() > 0){
		$email = mysql_fetch_array($emailsql);
		$message = $email['message'];
		$message = str_replace("{username}",$user['username'],$message);
		$message = str_replace("{password}",$newpass,$message);
		$message = str_replace("{homeurl}",$homeurl,$message);
		$subject = $email['subject'];
		$to = $user['email'];
		$from = $config['email'];
		if($config['email_send_type'] == "smtp"){send_smtp($from,$to,$subject,$message);}
		else if($config['email_send_type'] == "php"){send_php($from,$to,$subject,$message);}
	}
}

function NewRegister($username,$password,$toemail){
	global $homeurl;
	global $config;

	$emailsql = mysql_query("SELECT * FROM email_templates WHERE id='2' AND active='1'");
	if(mysql_affected_rows() > 0){
		$email = mysql_fetch_array($emailsql);
		$message = $email['message'];
		$message = str_replace("{username}",$username,$message);
		$message = str_replace("{password}",$password,$message);
		$message = str_replace("{sitename}",$config['sitename'],$message);
		$message = str_replace("{homeurl}",$homeurl,$message);
		$subject = 'مرحبا بك كعميل جديد لدى '.$config['sitename'];

		$to = $toemail;
		$from = $config['email'];

		if($config['email_send_type'] == "smtp"){send_smtp($from,$to,$subject,$message);}
		else if($config['email_send_type'] == "php"){send_php($from,$to,$subject,$message);}
	}
}

function VerRegister($username, $timehash, $toemail){
	global $homeurl;
	global $config;

	$emailsql = mysql_query("SELECT * FROM email_templates WHERE id='3' AND active='1'");
	if(mysql_affected_rows() > 0){
		$link = $homeurl.'/login.php?action=activate&hash='.$timehash;
		$email = mysql_fetch_array($emailsql);
		$message = $email['message'];
		$message = str_replace("{user}",$username,$message);
		$message = str_replace("{link}",$link,$message);
		$message = str_replace("{sitename}",$config['sitename'],$message);
		$message = str_replace("{homeurl}",$homeurl,$message);
		$subject = 'طلب تفعيل الاشتراك في '.$config['sitename'];

		$to = $toemail;
		$from = $config['email'];

		if($config['email_send_type'] == "smtp"){send_smtp($from,$to,$subject,$message);}
		else if($config['email_send_type'] == "php"){send_php($from,$to,$subject,$message);}
	}
}

function Contact($message, $name, $femail, $domain){
	global $homeurl;
	global $config;

	$emailsql = mysql_query("SELECT * FROM email_templates WHERE id='4' AND active='1'");
	if(mysql_affected_rows() > 0){
		$email = mysql_fetch_array($emailsql);
		$content = $email['message'];
		$content = str_replace("{user}",$name,$content);
		$content = str_replace("{email}",$femail,$content);
		$content = str_replace("{domain}",$domain,$content);
		$content = str_replace("{message}",$message,$content);
		$subject = 'New contact message in '.$config['sitename'];

		$to = $config['email'];
		$from = $femail;

		if($config['email_send_type'] == "smtp"){send_smtp($from,$to,$subject,$content);}
		else if($config['email_send_type'] == "php"){send_php($from,$to,$subject,$content);}
	}
}

function email_when_open($id){
	global $homeurl;
	global $config;

	$emailsql = mysql_query("SELECT * FROM email_templates WHERE id='5' AND active='1'");
	if(mysql_affected_rows() > 0){
		$ticket = Sqlread("SELECT ticket.subject,ticket.id,user.username,user.email FROM ticket
		 INNER JOIN user ON(user.id=ticket.userid) WHERE ticket.id='$id'");
		$email = mysql_fetch_array($emailsql);
		$content = $email['message'];
		$content = str_replace("{subject}", $ticket['subject'],$content);
		$content = str_replace("{sid}", $ticket['id'],$content);
		$content = str_replace("{name}", $ticket['username'],$content);
		$content = str_replace("{homeurl}", $homeurl, $content);

		$subject = 'New ticket alert from '.$config['sitename'];

		$to = $ticket['email'];
		$from = $config['email'];

		if($config['email_send_type'] == "smtp"){send_smtp($from,$to,$subject,$content);}
		else if($config['email_send_type'] == "php"){send_php($from,$to,$subject,$content);}
	}
}

function email_when_reply($id){
	global $homeurl;
	global $config;

	$emailsql = mysql_query("SELECT * FROM email_templates WHERE id='6' AND active='1'");
	if(mysql_affected_rows() > 0){
		$ticket = Sqlread("SELECT ticket.subject,ticket.id,user.username,user.email FROM ticket
		 INNER JOIN user ON(user.id=ticket.userid) WHERE ticket.id='$id'");
		$email = mysql_fetch_array($emailsql);

		$content = $email['message'];
		$content = str_replace("{subject}", $ticket['subject'],$content);
		$content = str_replace("{sid}", $ticket['sid'],$content);
		$content = str_replace("{name}", $ticket['username'],$content);
		$content = str_replace("{homeurl}", $homeurl, $content);

		$subject = 'Update to Ticket ID #'.$id.' in '.$config['sitename'];

		$to = $ticket['email'];
		$from = $config['email'];

		if($config['email_send_type'] == "smtp"){send_smtp($from,$to,$subject,$content);}
		else if($config['email_send_type'] == "php"){send_php($from,$to,$subject,$content);}
	}
}

?>