<?php

class helper{
  public $olds_IDS;
  public $new_IDS = array();
  public $curl_status;
  private $badWords = array();
  private $goodWords = array();

  public function __construct(){
    $words = $this->db->get_results("SELECT * FROM words");
    foreach($words as $word){
      if($word->type == 1){
        $this->goodWords[] = $word->word;
      }
      elseif($word->type == 0){
        $this->badWords[] = $word->word;
      }
    }
  }

  public function __destruct(){
    $this->db->query("INSERT INTO `log` (`Title`,`Date`,`alert_id`,`Result`) VALUES ('".get_called_class()."','".TIMENOW."','".$this->alert->id."',".$this->newdata_counter.")");
    $this->db->query("UPDATE alerts SET lastupdate='".TIMENOW."',`counter`=`counter`+".$this->newdata_counter.",notify_counter=notify_counter+".$this->newdata_counter." WHERE id='".$this->alert->id."'");
    $this->db->query("UPDATE users SET quota=quota+".$this->newdata_counter." WHERE id='".$this->alert->user_id."'");
  }

  public function curl($url) {
    if(TRACKER_DEBUG){
      //echo $url.'<br />';
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.6 (KHTML, like Gecko) Chrome/16.0.897.0 Safari/535.6');
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
    curl_setopt($ch, CURLOPT_TIMEOUT, 40);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    $result = curl_exec($ch);
    $this->curl_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return $result;
  }

  function getSocialToken($type){
    global $db;
    $token = $db->get_row("SELECT token,secret_token FROM social_tokens WHERE type='$type' AND userid='".$this->alert->user_id."'");
    if(!$token){
      $token = $db->get_row("SELECT token,secret_token FROM social_tokens WHERE type='$type' ORDER BY id DESC LIMIT 0,1");
    }
    return $token;
  }

  public function insertData($content, $id, $type){
    if(empty($id)){
      return false;
    }
    if($this->is_exist($id, $type) === false){
      return false;
    }
    $this->new_IDS[$type][] = $id;
    $Data['data_id'] = $id;
    $Data['alert_id'] = $this->alert->id;
    $Data['user_id'] = $this->alert->user_id;
    $Data['realtime'] = $content['date'];
    $Data['content'] = addslashes(serialize($content));
    $Data['content_encoded'] = base64_encode((serialize($content)));
    $Data['type'] = $type;
    $Data['smart_filter'] = $this->DataFilter($Data['content']);
    if($this->excludeResult($Data['content'])){
      return;
    }
    if($this->exactResult($Data['content'])){
      $Data['exactvalue'] = 1;
    }
    $this->db->insert('data', $Data);
    $this->newdata_counter++;
  }

  public function excludeResult($content){
    if(empty($this->alert->exclude_search)){
      return false;
    }
    $search_word = $this->FixToSearch($this->alert->exclude_search);
    $searchin_string = $this->FixToSearch($content);
    $search_word = explode(' ', $search_word);
    $pattern = '';
    if($this->isArabic($content)){
      $regsearch = '(.){1,10}';
    }
    else{
      $regsearch = '(.){1,2}';
    }
    foreach($search_word as $searchword){
      $pattern .= $searchword.$regsearch;
    }
    $pattern = rtrim($pattern, $regsearch);
    if(preg_match('/'.$pattern.'/i', $searchin_string)){
      return true;
    }
    else{
      return false;
    }
  }

  public function exactResult($content){
    $search_word = $this->FixToSearch($this->alert->search);
    $searchin_string = $this->FixToSearch($content);
    $search_word = explode(' ', $search_word);
    $pattern = '';
    if($this->isArabic($content)){
      $regsearch = '(.){1,10}';
    }
    else{
      $regsearch = '(.){1,2}';
    }
    foreach($search_word as $searchword){
      $pattern .= $searchword.$regsearch;
    }
    $pattern = rtrim($pattern, $regsearch);
    if(preg_match('/'.$pattern.'/i', $searchin_string)){
      return true;
    }
    else{
      return false;
    }
    /*if(preg_match("/\b".preg_quote($this->alert->search)."\b/i", $content)|| preg_match("/(?:[[:space:]]|^)".$this->alert->search."(?:[^\w]|$)/i", $content) || strpos($content, $this->alert->search) || strpos($content, str_replace(' ', '_', $this->alert->search))){
      return true;
    }
    else{
      return false;
    }*/
  }

  private function FixToSearch($text){
    $text = str_replace(array('أ','إ','آ','ة','ى','-','_','@','#','!'), array('ا','ا','ا','ه','ي',' ',' ',' ',' ', ' '), $text);
    $text = preg_replace('/((ال)\B|(ا)\w{2}\B)/i', '', $text);
    return preg_quote($text);
  }

  private function isArabic($text){
    $arabic = "دجحخهعغفقثصضطكمنتالبيسشظزوةىلارؤءئإلإألأآلآ";
    $len = mb_strlen($arabic, 'UTF-8');
    for($i=0;$i<=$len;$i++){
      if(strpos($text, substr($arabic, $i, $i+1)) !== false){
        return true;
      }
    }
    return false;
  }

  private function DataFilter($content){
    $filter = 0;
    foreach($this->goodWords as $word){
      if($count = preg_match_all("/$word/i", $content, $matches)){
        $filter += $count;
      }
    }
    foreach($this->badWords as $word){
      if($count = preg_match_all("/$word/i", $content, $matches)){
        $filter -= $count;
      }
    }
    if($filter > 0){
      return 1;
    }
    if($filter < 0){
      return 2;
    }
    return 0;
  }

  public function uniqueID($string) {
    $alphabet = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
    $string = preg_replace('~[^a-zA-Z0-9]+~', '', str_replace(array('http://','www.'), '', $string));
    $string = str_split(strtoupper($string));
    $id = '';
    foreach($string as $char){
      $pos = array_keys($alphabet, $char);
      if($pos[0] > 0){
        $id .= (string)$pos[0];
      }
    }
    return md5($id);
  }

  public function is_exist($id, $type) {
    if($this->olds_IDS != '') {
      if(@in_array($id, $this->olds_IDS[$type])) {
        return false;
      }
    }
    if(@in_array($id, $this->new_IDS[$type])) {
      return false;
    }
    return true;
  }

  public function old_IDS($types) {
    $old_IDS = '';
    foreach($types as $type){
      $gets = $this->db->get_results("SELECT data_id FROM data WHERE `type`='$type' AND alert_id=".$this->alert->id." AND MONTH(timepost)='".date('m')."'");
      if($gets){
        foreach($gets as $get) {
          $old_IDS[$type][] = $get->data_id;
        }
      }
    }
    $this->olds_IDS = $old_IDS;
  }

}

?>