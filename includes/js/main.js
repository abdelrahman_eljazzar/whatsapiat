var autoload_timer;
$(document).ready(function(){
	//$('button').tooltip();
    //$(".form-valid").validationEngine();
    $('.open-ajax').click(function (e) {
        e.preventDefault();
        OpenAjaxEvent(this);
    });
	$("#tracker-type-filter").change(function() {
	  $("#tracker-data").html("");
	  $(".open-frame").html("");
	  loadTrackerData('new');
	});
    $("#tracker-query-exact").change(function() {
		loadTrackerData('new');
	});
    $("#tracker-query-recent").change(function() {
		loadTrackerData('new');
	});
	$('#tracker-data').scroll(function () {
		if ($(this).scrollTop() > $(this).prop('scrollHeight')-1000 && $(this).prop('scrollHeight')>600) {
			loadTrackerData('bottom');
		}
	});
	$("#tracker-query-filter").keyup(function(event){
		if(event.keyCode == 13){
			loadTrackerData('new');
		}
	});
	$("#change-my-plan").click(function(event) {
	  event.preventDefault();
	  $('#my-plan').modal('show');
    });
	$(".tracker-cat-filter li").click(function() {
		var type = $(".tracker-cat-filter li.active").attr('data-name');
		$(".tracker-cat-filter li").removeClass('active');
		$(this).addClass('active');
		loadTracker('new', type);
	});
    $(".tracker-edit-alert").click(function() {
		editAlert();
	});
    $(".tracker-delete-alert").click(function() {
		deleteAlert();
	});
    $("#notify-alert").click(function() {
      $('.tracker-loading').show();
      $('#mention-stat-holder').remove();
      $.get("notify/showlist/", {}
      , function(data) {
        $('.tracker-loading').hide();
      	$('#tracker-body .tracker-data-holder').append(data);
        $('#tracker-body .tracker-data-holder .row:first').hide();
        $('#mention-stat-holder').show();
        $('#tracker-body .tracker-data-holder .open-ajax').click(function (e) {
            e.preventDefault();
            OpenAjaxEvent(this);
        });
        $(".del_notify_alert").click(function(e) {
          e.preventDefault();
          var alertid = $(this).attr("data-alertid");
          var elm = this;
          if(confirm("عملية الحذف لا يمكن التراجع بها, هل تريد الأستمرار؟")){
            $('.tracker-loading').show();
            $.get("notify/delete/", {"id" : alertid}
              , function(data) {
                $('.tracker-loading').hide();
                $(elm).closest("tr").fadeOut( "slow", function() {
                  $(elm).parent("tr").closest();
                });
              });
          }
        });
      });
	});
    $("#employee-list").click(function() {
      $('.tracker-loading').show();
      $('#mention-stat-holder').remove();
      $.get("employee/showlist/", {}
      , function(data) {
        $('.tracker-loading').hide();
      	$('#tracker-body .tracker-data-holder').append(data);
        $('#tracker-body .tracker-data-holder .row:first').hide();
        $('#mention-stat-holder').show();
        $('#tracker-body .tracker-data-holder .open-ajax').click(function (e) {
            e.preventDefault();
            OpenAjaxEvent(this);
        });
        $(".del_employee").click(function(e) {
          e.preventDefault();
          var alertid = $(this).attr("data-alertid");
          var elm = this;
          if(confirm("عملية الحذف لا يمكن التراجع بها, هل تريد الأستمرار؟")){
            $('.tracker-loading').show();
            $.get("employee/delete/", {"id" : alertid}
              , function(data) {
                $('.tracker-loading').hide();
                $(elm).closest("tr").fadeOut( "slow", function() {
                  $(elm).parent("tr").closest();
                });
              });
          }
        });
      });
	});
    $("#employee-log").click(function() {
      $('.tracker-loading').show();
      $('#mention-stat-holder').remove();
      $.get("employee/log/", {}
      , function(data) {
        $('.tracker-loading').hide();
      	$('#tracker-body .tracker-data-holder').append(data);
        $('#tracker-body .tracker-data-holder .row:first').hide();
        $('#mention-stat-holder').show();
        $('#tracker-body .tracker-data-holder .open-ajax').click(function (e) {
            e.preventDefault();
            OpenAjaxEvent(this);
        });
        $(".del_log").click(function(e) {
          e.preventDefault();
          var id = $(this).attr("data-id");
          var elm = this;
          if(confirm("عملية الحذف لا يمكن التراجع بها, هل تريد الأستمرار؟")){
            $('.tracker-loading').show();
            $.get("employee/logdelete/", {"id" : id}
              , function(data) {
                $('.tracker-loading').hide();
                $(elm).closest("tr").fadeOut( "slow", function() {
                  $(elm).parent("tr").closest();
                });
              });
          }
        });
      });
	});
	$(".tracker-alert-filter").click(function() {
		$(".sidebar .sidemenu").hide();
		$(this).next(".sidemenu").show();
		var type = $(".tracker-cat-filter li.active").attr('data-name');
		$(".tracker-cat-filter li").removeClass('active');
		$(".tracker-alert-filter").removeClass('active');
		$(this).addClass('active');
		$("#tracker-title").text($(this).attr('data-title'));
		loadTracker('new', type);
	});
	if ($('#tracker-dashboard').length) {
		$('html,body').animate({scrollTop: $("#tracker-dashboard").offset().top}, 'slow');
		autoload_timer = setTimeout(function(){loadTrackerData('top')}, 30000);
		init();
	}
});

function OpenAjaxEvent(element) {
    var href = $(element).attr('href');
    var title = $(element).attr('title');
    var view = $(element).attr('data-content');
    OpenAjax(href, title, view);
}

function OpenAjax(link, title, view) {
    $('#open-ajax-form .modal-title').text(title);
    $('.open-ajax-body').html("");
    $('.open-ajax-loading').show();
    $('#open-ajax-form').modal();
    $.get(link, {
        "no_header": 1
    }, function (data) {
        $('.open-ajax-loading').hide();
        $(".open-ajax-body").html(data);
        if (typeof view != 'undefined') {
            $('.open-ajax-body .modal-body').append(view);
        }
        var options = {
          beforeSubmit: function () {
            $('.open-ajax-submit').show();
          },
          success: function (responseText, statusText) {
              $('.open-ajax-submit').hide();
              var response = "";
              try {
                response = JSON.parse(responseText);
              } catch (e) {}

             if (typeof response == "object") {
                if(response["status"] == 1){
                  $('#open-ajax-form button[type="submit"]').hide();
                  show_error_msg(response["message"], "info");
                }
                else if(response["status"] == 0){
                  show_error_msg(response["message"], "danger");
                }
                else if(response["status"] == 2){
                  $('#open-ajax-form').modal('hide');
                  return;
                }
              }
              else if (responseText != 1) {
                show_error_msg(responseText, "danger");
              } else {
                var action = $(".open-ajax-body #open-ajax-form").attr("data-action");
                $('#open-ajax-form').modal('hide');
                if (action == "refresh") {
                    location.reload();
                } else if (typeof action != 'undefined') {
                    window.location = action;
                }
                window.location = "http://artgine.com/artgine/projects/whatsu/home";
              }

          }
      };
      $("form#open-ajax-form").ajaxForm(options);
      $("form#open-ajax-form.form-valid").validationEngine();
      $("form#open-ajax-form #tw_status").keydown(function(){countChars('tw_status','char_count');});
      $("form#open-ajax-form #tw_status").keyup(function(){countChars('tw_status','char_count');});
    });
}

function show_error_msg(Text, Type){
  if ($('#open-ajax-form .modal-body').length) {
      $('#open-ajax-form .modal-body .alert').remove();
      $('#open-ajax-form .modal-body').prepend('<div class="alert alert-'+Type+'"> <i class="fa fa-close whiteTXT"></i> ' + Text + '</div>');
  } else {
      $('#open-ajax-form .open-ajax-body .alert').remove();
      $('#open-ajax-form .open-ajax-body').prepend('<div class="alert alert-'+Type+'"> <i class="fa fa-close whiteTXT"></i> ' + Text + '</div>');
  }
}

function init(){
	$("#tracker-data .tracker-btn-action button").click(function() {
		trackDataAction(this);
	});
	$("#tracker-data li").click(function() {
		openData(this);
	});
	$('#tracker-data button').tooltip();
}

function editAlert(){
  var alertid = $(".tracker-alert-filter.active").attr('data-id');
  $('.tracker-loading').show();
  $.get("home/editalert", {'id':alertid}
  , function(data) {
    $('.tracker-loading').hide();
    $('#edit-alert .modal-content').html(data);
    $('#edit-alert').modal('show');
  });
}

function deleteAlert(){
  if(!confirm("العملية لا يمكن التراجع بها, هل تريد الأستمرار؟")){
    return;
  }
  var alertid = $(".tracker-alert-filter.active").attr('data-id');
  $('.tracker-loading').show();
  $.get("home/delalert", {'id':alertid}
  , function(data) {
    $(".tracker-loading").hide();
    $(".tracker-alert-filter.active").next(".sidemenu").remove();
    $(".tracker-alert-filter.active").addClass("willremove");
    $(".tracker-alert-filter.active").removeClass("active");

    $(".tracker-alert-filter.willremove").next(".tracker-alert-filter").addClass("active");
    $(".tracker-alert-filter.active").trigger("click");
    $(".tracker-alert-filter.willremove").remove();

  });
}

function openData(li){
	var url = $(li).find(".open-frame-data").attr('data-frame');
	var alertid = $(li).attr('data-id');
    $('.tracker-loading').show();
    $(li).find('.time i').hide();
	if(url != ""){
	    $.get("tracker/actions", {'id':alertid, "action":"read", "type":1}, function(data) {
	      $('.tracker-loading').hide();
        });
		url = decodeURIComponent(url);
		$('.open-frame').attr("style", "padding:0");
		$('.open-frame').html('<iframe src="'+url+'" width="100%" height="600"></iframe>');
	}
	else{
	  $.get("tracker/readone", {'id':alertid}, function(data) {
        $('.tracker-loading').hide();
        $('.open-frame').attr("style", "padding: 5px 40px;");
        $('.open-frame').html(data);
        $('.open-frame .open-ajax').click(function (e) {
            e.preventDefault();
            OpenAjaxEvent(this);
        });
        $(".open-frame .ajax-twitter-retweet").click(function(e){
          var btn = $(this);
          var link = btn.attr('data-href');
          $('.tracker-loading').show();
          $.get(link, {}
          , function(data) {
            $('.tracker-loading').hide();
            btn.attr("disabled", "disabled");
          });
        });
      });
	}
}

function trackDataAction(btn){
  var li = $(btn).parents("li");
  var id = li.attr('data-id');
  var action = $(btn).attr('data-action');
  if($(btn).hasClass('btn-active')){
    var type = 0;
  }
  else{
    var type = 1;
  }
  $('.tracker-loading').show();
  if(action == "vote_neg" || action == "vote_pos"){
    li.removeClass('good-border');
    li.removeClass('bad-border');
    li.find('.tracker-btn-action button[data-action="vote_neg"]').removeClass('btn-active');
    li.find('.tracker-btn-action button[data-action="vote_pos"]').removeClass('btn-active');
    if(type == 1){
      if(action == "vote_neg"){
        li.addClass('bad-border');
      }
      else if(action == "vote_pos"){
        li.addClass('good-border');
      }
      $(btn).addClass('btn-active');
    }
  }
  else{
    $(btn).toggleClass('btn-active');
  }
  $.get("tracker/actions", {'id':id, 'action':action, 'type':type}
  , function(data) {
  $('.tracker-loading').hide();
  });
}

function loadTracker(direction, oldtype){
	var type = $(".tracker-cat-filter li.active").attr('data-name');
	var alertid = $(".tracker-alert-filter.active").attr('data-id');
	if(type == 'stats'){
	  clearInterval(autoload_timer);
	  $('.tracker-loading').show();
      $('#mention-stat-holder').remove();
      $.get("stats/index/", {'alertid':alertid}
      , function(data) {
        $('.tracker-loading').hide();
      	$('#tracker-body .tracker-data-holder').append(data);
        $('#tracker-body .tracker-data-holder .row').hide();
        $('#mention-stat-holder').show();
      });
	}
    else if(type == 'reports'){
	  clearInterval(autoload_timer);
	  $('.tracker-loading').show();
      $('#mention-stat-holder').remove();
      $.get("reports/index/", {'alertid':alertid}
      , function(data) {
        $('.tracker-loading').hide();
      	$('#tracker-body .tracker-data-holder').append(data);
        $('#tracker-body .tracker-data-holder .row').hide();
        $('#mention-stat-holder').show();
      });
	}
	else{
	  $('#mention-stat-holder').remove();
	  $('#tracker-data').html("");
	  $('#tracker-body .tracker-data-holder .row').show();
      loadTrackerData(direction);
	}
}

function loadTrackerData(direction){
  var type = $(".tracker-cat-filter li.active").attr('data-name');
  var alertid = $(".tracker-alert-filter.active").attr('data-id');
  var category = $("#tracker-type-filter").val();
  var query = $("#tracker-query-filter").val();
  var page = 0;
  if(query.length > 0 && query.length < 3){
	alert('طول نص البحث يجب الا يقل عن 3 احرف');
	return;
  }
  if(query.length > 0){
    clearInterval(autoload_timer);
  }
  if(type == "unexact"){
    clearInterval(autoload_timer);
    var page = $('#tracker-data li:last').attr('data-page');
  }
  if(direction == 'bottom'){
	var lastid = $('#tracker-data li:last').attr('data-id');
  }
  else if(direction == 'new'){
    var lastid = $('#tracker-data li:first').attr('data-id');
  }
  else if(direction == 'top'){
  	var lastid = $('#tracker-data li:first').attr('data-id');
  }
  if($('#tracker-query-exact').is(':checked')){
    var exactvalue = 1;
    var page = $('#tracker-data li:last').attr('data-page');
  }
  else{
    var exactvalue = 0;
  }
  if($('#tracker-query-recent').is(':checked')){
    clearInterval(autoload_timer);
    var showrecent = 1;
  }
  else{
    var showrecent = 0;
  }
  $('.tracker-loading').show();
  $.get("tracker/ajax", {'category':category, 'type':type,"showrecent":showrecent, 'exactvalue':exactvalue, "page":page, 'lastid':lastid, 'alertid':alertid, 'query':query, 'direction':direction}
  , function(data) {
    $('.tracker-loading').hide();
    $('#tracker-data li.bg-info').remove();
    if(direction == 'top'){
    	$("#tracker-data").prepend(data);
    }
    else if(direction == 'new'){
            $("#tracker-data").html(data);
            $("#tracker-data").scrollTop(0);
    }
    else{
    	$("#tracker-data").append(data);
    }
          if($("#tracker-data li").length == 0){
            $("#tracker-data").html('<li class="bg-info"><p>لم يتم العثور على نتائج تطابق عملية بحثك</p></li>');
          }
    init();
  });
}

function countChars(limitField, limitCount) {
    var limitNum = 140;
	var limitCount = document.getElementById(limitCount);
    var regex = /\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\"\\.,<>?\u00AB\u00BB\u201C\u201D\u2018\u2019]))/i;
	var text = document.getElementById(limitField).value;
    var textsplit = text.split(' ');
    var urlscount = 0;
    for (i=0;i<textsplit.length;i++){
        if(regex.test(textsplit[i])){
          urlscount++;
          text = text.replace(textsplit[i], '');
        }
    }
	limitCount.value = limitNum-text.length-(22*urlscount);
}

function stripslashes (str) {
  return (str + '').replace(/\\(.?)/g, function (s, n1) {
    switch (n1) {
    case '\\':
      return '\\';
    case '0':
      return '\u0000';
    case '':
      return '';
    default:
      return n1;
    }
  });
}
function DeleteVer(url) {
if (confirm("هل انت متاكد من اكمال تنفيذ العمليه..لا يمكن التراجع بها؟")) {
window.location = url;
}

}
function openSocial(url){
  window.open(url,"","width=500,height=600");
}

;(function(e){"function"==typeof define&&define.amd?define(["$"],e):e("undefined"!=typeof $?$:window.Zepto)})(function(e){"use strict";function t(t){var r=t.data;t.isDefaultPrevented()||(t.preventDefault(),e(t.target).ajaxSubmit(r))}function r(t){var r=t.target,a=e(r);if(!a.is("[type=submit],[type=image]")){var n=a.closest("[type=submit]");if(0===n.length)return;r=n[0]}var i=this;if(i.clk=r,"image"==r.type)if(void 0!==t.offsetX)i.clk_x=t.offsetX,i.clk_y=t.offsetY;else if("function"==typeof e.fn.offset){var o=a.offset();i.clk_x=t.pageX-o.left,i.clk_y=t.pageY-o.top}else i.clk_x=t.pageX-r.offsetLeft,i.clk_y=t.pageY-r.offsetTop;setTimeout(function(){i.clk=i.clk_x=i.clk_y=null},100)}function a(){if(e.fn.ajaxSubmit.debug){var t="[$.form] "+Array.prototype.join.call(arguments,"");window.console&&window.console.log?window.console.log(t):window.opera&&window.opera.postError&&window.opera.postError(t)}}var n={};n.fileapi=void 0!==e("<input type='file'/>").get(0).files,n.formdata=void 0!==window.FormData;var i=!!e.fn.prop;e.fn.attr2=function(){if(!i)return this.attr.apply(this,arguments);var e=this.prop.apply(this,arguments);return e&&e.$||"string"==typeof e?e:this.attr.apply(this,arguments)},e.fn.ajaxSubmit=function(t){function r(r){var a,n,i=e.param(r,t.traditional).split("&"),o=i.length,s=[];for(a=0;o>a;a++)i[a]=i[a].replace(/\+/g," "),n=i[a].split("="),s.push([decodeURIComponent(n[0]),decodeURIComponent(n[1])]);return s}function o(a){for(var n=new FormData,i=0;a.length>i;i++)n.append(a[i].name,a[i].value);if(t.extraData){var o=r(t.extraData);for(i=0;o.length>i;i++)o[i]&&n.append(o[i][0],o[i][1])}t.data=null;var s=e.extend(!0,{},e.ajaxSettings,t,{contentType:!1,processData:!1,cache:!1,type:u||"POST"});t.uploadProgress&&(s.xhr=function(){var r=e.ajaxSettings.xhr();return r.upload&&r.upload.addEventListener("progress",function(e){var r=0,a=e.loaded||e.position,n=e.total;e.lengthComputable&&(r=Math.ceil(100*(a/n))),t.uploadProgress(e,a,n,r)},!1),r}),s.data=null;var c=s.beforeSend;return s.beforeSend=function(e,r){r.data=t.formData?t.formData:n,c&&c.call(this,e,r)},e.ajax(s)}function s(r){function n(e){var t=null;try{e.contentWindow&&(t=e.contentWindow.document)}catch(r){a("cannot get iframe.contentWindow document: "+r)}if(t)return t;try{t=e.contentDocument?e.contentDocument:e.document}catch(r){a("cannot get iframe.contentDocument: "+r),t=e.document}return t}function o(){function t(){try{var e=n(g).readyState;a("state = "+e),e&&"uninitialized"==e.toLowerCase()&&setTimeout(t,50)}catch(r){a("Server abort: ",r," (",r.name,")"),s(k),j&&clearTimeout(j),j=void 0}}var r=f.attr2("target"),i=f.attr2("action");w.setAttribute("target",p),(!u||/post/i.test(u))&&w.setAttribute("method","POST"),i!=m.url&&w.setAttribute("action",m.url),m.skipEncodingOverride||u&&!/post/i.test(u)||f.attr({encoding:"multipart/form-data",enctype:"multipart/form-data"}),m.timeout&&(j=setTimeout(function(){T=!0,s(D)},m.timeout));var o=[];try{if(m.extraData)for(var c in m.extraData)m.extraData.hasOwnProperty(c)&&(e.isPlainObject(m.extraData[c])&&m.extraData[c].hasOwnProperty("name")&&m.extraData[c].hasOwnProperty("value")?o.push(e('<input type="hidden" name="'+m.extraData[c].name+'">').val(m.extraData[c].value).appendTo(w)[0]):o.push(e('<input type="hidden" name="'+c+'">').val(m.extraData[c]).appendTo(w)[0]));m.iframeTarget||v.appendTo("body"),g.attachEvent?g.attachEvent("onload",s):g.addEventListener("load",s,!1),setTimeout(t,15);try{w.submit()}catch(l){var d=document.createElement("form").submit;d.apply(w)}}finally{w.setAttribute("action",i),r?w.setAttribute("target",r):f.removeAttr("target"),e(o).remove()}}function s(t){if(!x.aborted&&!F){if(M=n(g),M||(a("cannot access response document"),t=k),t===D&&x)return x.abort("timeout"),S.reject(x,"timeout"),void 0;if(t==k&&x)return x.abort("server abort"),S.reject(x,"error","server abort"),void 0;if(M&&M.location.href!=m.iframeSrc||T){g.detachEvent?g.detachEvent("onload",s):g.removeEventListener("load",s,!1);var r,i="success";try{if(T)throw"timeout";var o="xml"==m.dataType||M.XMLDocument||e.isXMLDoc(M);if(a("isXml="+o),!o&&window.opera&&(null===M.body||!M.body.innerHTML)&&--O)return a("requeing onLoad callback, DOM not available"),setTimeout(s,250),void 0;var u=M.body?M.body:M.documentElement;x.responseText=u?u.innerHTML:null,x.responseXML=M.XMLDocument?M.XMLDocument:M,o&&(m.dataType="xml"),x.getResponseHeader=function(e){var t={"content-type":m.dataType};return t[e.toLowerCase()]},u&&(x.status=Number(u.getAttribute("status"))||x.status,x.statusText=u.getAttribute("statusText")||x.statusText);var c=(m.dataType||"").toLowerCase(),l=/(json|script|text)/.test(c);if(l||m.textarea){var f=M.getElementsByTagName("textarea")[0];if(f)x.responseText=f.value,x.status=Number(f.getAttribute("status"))||x.status,x.statusText=f.getAttribute("statusText")||x.statusText;else if(l){var p=M.getElementsByTagName("pre")[0],h=M.getElementsByTagName("body")[0];p?x.responseText=p.textContent?p.textContent:p.innerText:h&&(x.responseText=h.textContent?h.textContent:h.innerText)}}else"xml"==c&&!x.responseXML&&x.responseText&&(x.responseXML=X(x.responseText));try{E=_(x,c,m)}catch(b){i="parsererror",x.error=r=b||i}}catch(b){a("error caught: ",b),i="error",x.error=r=b||i}x.aborted&&(a("upload aborted"),i=null),x.status&&(i=x.status>=200&&300>x.status||304===x.status?"success":"error"),"success"===i?(m.success&&m.success.call(m.context,E,"success",x),S.resolve(x.responseText,"success",x),d&&e.event.trigger("ajaxSuccess",[x,m])):i&&(void 0===r&&(r=x.statusText),m.error&&m.error.call(m.context,x,i,r),S.reject(x,"error",r),d&&e.event.trigger("ajaxError",[x,m,r])),d&&e.event.trigger("ajaxComplete",[x,m]),d&&!--e.active&&e.event.trigger("ajaxStop"),m.complete&&m.complete.call(m.context,x,i),F=!0,m.timeout&&clearTimeout(j),setTimeout(function(){m.iframeTarget?v.attr("src",m.iframeSrc):v.remove(),x.responseXML=null},100)}}}var c,l,m,d,p,v,g,x,b,y,T,j,w=f[0],S=e.Deferred();if(S.abort=function(e){x.abort(e)},r)for(l=0;h.length>l;l++)c=e(h[l]),i?c.prop("disabled",!1):c.removeAttr("disabled");if(m=e.extend(!0,{},e.ajaxSettings,t),m.context=m.context||m,p="jqFormIO"+(new Date).getTime(),m.iframeTarget?(v=e(m.iframeTarget),y=v.attr2("name"),y?p=y:v.attr2("name",p)):(v=e('<iframe name="'+p+'" src="'+m.iframeSrc+'" />'),v.css({position:"absolute",top:"-1000px",left:"-1000px"})),g=v[0],x={aborted:0,responseText:null,responseXML:null,status:0,statusText:"n/a",getAllResponseHeaders:function(){},getResponseHeader:function(){},setRequestHeader:function(){},abort:function(t){var r="timeout"===t?"timeout":"aborted";a("aborting upload... "+r),this.aborted=1;try{g.contentWindow.document.execCommand&&g.contentWindow.document.execCommand("Stop")}catch(n){}v.attr("src",m.iframeSrc),x.error=r,m.error&&m.error.call(m.context,x,r,t),d&&e.event.trigger("ajaxError",[x,m,r]),m.complete&&m.complete.call(m.context,x,r)}},d=m.global,d&&0===e.active++&&e.event.trigger("ajaxStart"),d&&e.event.trigger("ajaxSend",[x,m]),m.beforeSend&&m.beforeSend.call(m.context,x,m)===!1)return m.global&&e.active--,S.reject(),S;if(x.aborted)return S.reject(),S;b=w.clk,b&&(y=b.name,y&&!b.disabled&&(m.extraData=m.extraData||{},m.extraData[y]=b.value,"image"==b.type&&(m.extraData[y+".x"]=w.clk_x,m.extraData[y+".y"]=w.clk_y)));var D=1,k=2,A=e("meta[name=csrf-token]").attr("content"),L=e("meta[name=csrf-param]").attr("content");L&&A&&(m.extraData=m.extraData||{},m.extraData[L]=A),m.forceSync?o():setTimeout(o,10);var E,M,F,O=50,X=e.parseXML||function(e,t){return window.ActiveXObject?(t=new ActiveXObject("Microsoft.XMLDOM"),t.async="false",t.loadXML(e)):t=(new DOMParser).parseFromString(e,"text/xml"),t&&t.documentElement&&"parsererror"!=t.documentElement.nodeName?t:null},C=e.parseJSON||function(e){return window.eval("("+e+")")},_=function(t,r,a){var n=t.getResponseHeader("content-type")||"",i="xml"===r||!r&&n.indexOf("xml")>=0,o=i?t.responseXML:t.responseText;return i&&"parsererror"===o.documentElement.nodeName&&e.error&&e.error("parsererror"),a&&a.dataFilter&&(o=a.dataFilter(o,r)),"string"==typeof o&&("json"===r||!r&&n.indexOf("json")>=0?o=C(o):("script"===r||!r&&n.indexOf("javascript")>=0)&&e.globalEval(o)),o};return S}if(!this.length)return a("ajaxSubmit: skipping submit process - no element selected"),this;var u,c,l,f=this;"function"==typeof t?t={success:t}:void 0===t&&(t={}),u=t.type||this.attr2("method"),c=t.url||this.attr2("action"),l="string"==typeof c?e.trim(c):"",l=l||window.location.href||"",l&&(l=(l.match(/^([^#]+)/)||[])[1]),t=e.extend(!0,{url:l,success:e.ajaxSettings.success,type:u||e.ajaxSettings.type,iframeSrc:/^https/i.test(window.location.href||"")?"javascript:false":"about:blank"},t);var m={};if(this.trigger("form-pre-serialize",[this,t,m]),m.veto)return a("ajaxSubmit: submit vetoed via form-pre-serialize trigger"),this;if(t.beforeSerialize&&t.beforeSerialize(this,t)===!1)return a("ajaxSubmit: submit aborted via beforeSerialize callback"),this;var d=t.traditional;void 0===d&&(d=e.ajaxSettings.traditional);var p,h=[],v=this.formToArray(t.semantic,h);if(t.data&&(t.extraData=t.data,p=e.param(t.data,d)),t.beforeSubmit&&t.beforeSubmit(v,this,t)===!1)return a("ajaxSubmit: submit aborted via beforeSubmit callback"),this;if(this.trigger("form-submit-validate",[v,this,t,m]),m.veto)return a("ajaxSubmit: submit vetoed via form-submit-validate trigger"),this;var g=e.param(v,d);p&&(g=g?g+"&"+p:p),"GET"==t.type.toUpperCase()?(t.url+=(t.url.indexOf("?")>=0?"&":"?")+g,t.data=null):t.data=g;var x=[];if(t.resetForm&&x.push(function(){f.resetForm()}),t.clearForm&&x.push(function(){f.clearForm(t.includeHidden)}),!t.dataType&&t.target){var b=t.success||function(){};x.push(function(r){var a=t.replaceTarget?"replaceWith":"html";e(t.target)[a](r).each(b,arguments)})}else t.success&&x.push(t.success);if(t.success=function(e,r,a){for(var n=t.context||this,i=0,o=x.length;o>i;i++)x[i].apply(n,[e,r,a||f,f])},t.error){var y=t.error;t.error=function(e,r,a){var n=t.context||this;y.apply(n,[e,r,a,f])}}if(t.complete){var T=t.complete;t.complete=function(e,r){var a=t.context||this;T.apply(a,[e,r,f])}}var j=e("input[type=file]:enabled",this).filter(function(){return""!==e(this).val()}),w=j.length>0,S="multipart/form-data",D=f.attr("enctype")==S||f.attr("encoding")==S,k=n.fileapi&&n.formdata;a("fileAPI :"+k);var A,L=(w||D)&&!k;t.iframe!==!1&&(t.iframe||L)?t.closeKeepAlive?e.get(t.closeKeepAlive,function(){A=s(v)}):A=s(v):A=(w||D)&&k?o(v):e.ajax(t),f.removeData("jqxhr").data("jqxhr",A);for(var E=0;h.length>E;E++)h[E]=null;return this.trigger("form-submit-notify",[this,t]),this},e.fn.ajaxForm=function(n){if(n=n||{},n.delegation=n.delegation&&e.isFunction(e.fn.on),!n.delegation&&0===this.length){var i={s:this.selector,c:this.context};return!e.isReady&&i.s?(a("DOM not ready, queuing ajaxForm"),e(function(){e(i.s,i.c).ajaxForm(n)}),this):(a("terminating; zero elements found by selector"+(e.isReady?"":" (DOM not ready)")),this)}return n.delegation?(e(document).off("submit.form-plugin",this.selector,t).off("click.form-plugin",this.selector,r).on("submit.form-plugin",this.selector,n,t).on("click.form-plugin",this.selector,n,r),this):this.ajaxFormUnbind().bind("submit.form-plugin",n,t).bind("click.form-plugin",n,r)},e.fn.ajaxFormUnbind=function(){return this.unbind("submit.form-plugin click.form-plugin")},e.fn.formToArray=function(t,r){var a=[];if(0===this.length)return a;var i=this[0],o=t?i.getElementsByTagName("*"):i.elements;if(!o)return a;var s,u,c,l,f,m,d;for(s=0,m=o.length;m>s;s++)if(f=o[s],c=f.name,c&&!f.disabled)if(t&&i.clk&&"image"==f.type)i.clk==f&&(a.push({name:c,value:e(f).val(),type:f.type}),a.push({name:c+".x",value:i.clk_x},{name:c+".y",value:i.clk_y}));else if(l=e.fieldValue(f,!0),l&&l.constructor==Array)for(r&&r.push(f),u=0,d=l.length;d>u;u++)a.push({name:c,value:l[u]});else if(n.fileapi&&"file"==f.type){r&&r.push(f);var p=f.files;if(p.length)for(u=0;p.length>u;u++)a.push({name:c,value:p[u],type:f.type});else a.push({name:c,value:"",type:f.type})}else null!==l&&l!==void 0&&(r&&r.push(f),a.push({name:c,value:l,type:f.type,required:f.required}));if(!t&&i.clk){var h=e(i.clk),v=h[0];c=v.name,c&&!v.disabled&&"image"==v.type&&(a.push({name:c,value:h.val()}),a.push({name:c+".x",value:i.clk_x},{name:c+".y",value:i.clk_y}))}return a},e.fn.formSerialize=function(t){return e.param(this.formToArray(t))},e.fn.fieldSerialize=function(t){var r=[];return this.each(function(){var a=this.name;if(a){var n=e.fieldValue(this,t);if(n&&n.constructor==Array)for(var i=0,o=n.length;o>i;i++)r.push({name:a,value:n[i]});else null!==n&&n!==void 0&&r.push({name:this.name,value:n})}}),e.param(r)},e.fn.fieldValue=function(t){for(var r=[],a=0,n=this.length;n>a;a++){var i=this[a],o=e.fieldValue(i,t);null===o||void 0===o||o.constructor==Array&&!o.length||(o.constructor==Array?e.merge(r,o):r.push(o))}return r},e.fieldValue=function(t,r){var a=t.name,n=t.type,i=t.tagName.toLowerCase();if(void 0===r&&(r=!0),r&&(!a||t.disabled||"reset"==n||"button"==n||("checkbox"==n||"radio"==n)&&!t.checked||("submit"==n||"image"==n)&&t.form&&t.form.clk!=t||"select"==i&&-1==t.selectedIndex))return null;if("select"==i){var o=t.selectedIndex;if(0>o)return null;for(var s=[],u=t.options,c="select-one"==n,l=c?o+1:u.length,f=c?o:0;l>f;f++){var m=u[f];if(m.selected){var d=m.value;if(d||(d=m.attributes&&m.attributes.value&&!m.attributes.value.specified?m.text:m.value),c)return d;s.push(d)}}return s}return e(t).val()},e.fn.clearForm=function(t){return this.each(function(){e("input,select,textarea",this).clearFields(t)})},e.fn.clearFields=e.fn.clearInputs=function(t){var r=/^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;return this.each(function(){var a=this.type,n=this.tagName.toLowerCase();r.test(a)||"textarea"==n?this.value="":"checkbox"==a||"radio"==a?this.checked=!1:"select"==n?this.selectedIndex=-1:"file"==a?/MSIE/.test(navigator.userAgent)?e(this).replaceWith(e(this).clone(!0)):e(this).val(""):t&&(t===!0&&/hidden/.test(a)||"string"==typeof t&&e(this).is(t))&&(this.value="")})},e.fn.resetForm=function(){return this.each(function(){("function"==typeof this.reset||"object"==typeof this.reset&&!this.reset.nodeType)&&this.reset()})},e.fn.enable=function(e){return void 0===e&&(e=!0),this.each(function(){this.disabled=!e})},e.fn.selected=function(t){return void 0===t&&(t=!0),this.each(function(){var r=this.type;if("checkbox"==r||"radio"==r)this.checked=t;else if("option"==this.tagName.toLowerCase()){var a=e(this).parent("select");t&&a[0]&&"select-one"==a[0].type&&a.find("option").selected(!1),this.selected=t}})},e.fn.ajaxSubmit.debug=!1});
