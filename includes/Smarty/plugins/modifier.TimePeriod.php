<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty plugin
 *
 * Type:     modifier<br>
 * Name:     ardate<br>
 * Date:     Feb 26, 2003
 * Purpose:  convert \r\n, \r or \n to <<br>>
 * Input:<br>
 *         - contents = contents to replace
 *         - preceed_test = if true, includes preceeding break tags
 *           in replacement
 * Example:  {$text|ardate}
 * @link http://smarty.php.net/manual/en/language.modifier.nl2br.php
 *          nl2br (Smarty online manual)
 * @version  1.0
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param string
 * @return string
 */
function smarty_modifier_TimePeriod($unixtime){
    if(! is_numeric($unixtime)) return true;
	$diff = time()-$unixtime;
	$days = floor($diff/84600);
	$hours = floor($diff/3600);
	$mints = floor($diff/60);

	if($mints <= 1) return 'منذ دقيقة';
	elseif($mints <= 2) return 'منذ دقيقتين';
	elseif($mints <= 59) return 'منذ '.$mints.' دقيقة';
	elseif($hours <= 1) return 'منذ ساعة';
	elseif($hours <= 2) return 'منذ ساعتين';
	elseif($hours <= 23) return 'منذ '.$hours.' ساعة';
	elseif($days <= 1) return 'منذ امس';
	elseif($days <= 2) return 'منذ يومين';
	elseif($days <= 30) return 'منذ '.$days.' يوم';
	else return date('d/m/Y', $unixtime);
}

/* vim: set expandtab: */

?>
