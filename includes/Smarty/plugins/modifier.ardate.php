<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty plugin
 *
 * Type:     modifier<br>
 * Name:     ardate<br>
 * Date:     Feb 26, 2003
 * Purpose:  convert \r\n, \r or \n to <<br>>
 * Input:<br>
 *         - contents = contents to replace
 *         - preceed_test = if true, includes preceeding break tags
 *           in replacement
 * Example:  {$text|ardate}
 * @link http://smarty.php.net/manual/en/language.modifier.nl2br.php
 *          nl2br (Smarty online manual)
 * @version  1.0
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param string
 * @return string
 */
function smarty_modifier_ardate($string, $showAM=true)
{
    $string = date('d-m-Y-h-i-A',$string);
    $time = explode('-', $string);
	$d = $time[0];
	$m = $time[1];
	if ($m == '01') {$m = "يناير";}
	elseif ($m == '02') { $m = "فبراير"; }
	elseif ($m == '03') { $m = "مارس"; }
	elseif ($m == '04') { $m = "أبريل"; }
	elseif ($m == '05') { $m = "مايو"; }
	elseif ($m == '06') { $m = "يونيو"; }
	elseif ($m == '07') { $m = "يوليو"; }
	elseif ($m == '08') { $m = "أغسطس"; }
	elseif ($m == '09') { $m = "سبتمبر"; }
	elseif ($m == '10') { $m = "أكتوبر"; }
	elseif ($m == '11') { $m = "نوفمبر"; }
	elseif ($m == '12') { $m = "ديسمبر"; }
	$y = $time[2];
	if($time[5] == 'AM'){
		$A = 'صباحاً';
	}
	else{
		$A = 'مساءً';
	}
	if($showAM) return "$d $m $y  ".$time[3].":".$time[4]." ".$A;
	else return "$d $m $y";
}

/* vim: set expandtab: */

?>
