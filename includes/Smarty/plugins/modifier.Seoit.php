<?php
/*
 * Smarty plugin
 * ————————————————————-
 * File:     function.SEO.php
 * Type:     function
 * Name:     Seoit
 * Purpose:  Convert url to SEO url
 * ————————————————————-
 */

 function smarty_modifier_Seoit($string)
{
    $string = mb_strtolower($string, 'UTF-8');
	$code_entities_match = array('“','“','”','-','--','&quot;','!','@','#','$','%','^','&','*','(',')','_','+','{','}','|',':','"','<','>','?','[',']','\\',';',"'",',','.','/','*','+','~','`','=');
	$code_entities_replace = array('','','','','','','','','','','','','','','','','','','','','','','','','','','','','');
	$string = str_replace($code_entities_match, $code_entities_replace, $string);
    $string = trim($string);
    $string = preg_replace("/\s\s/","_",$string);
    $string = preg_replace("/\s/","_",$string);
	return $string;
}

?>