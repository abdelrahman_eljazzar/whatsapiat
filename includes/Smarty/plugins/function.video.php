<?php

function smarty_function_video($params, &$smarty){
	include_once(INC_DIR.'/AutoEmbed/autoembed.class.php');

	$AE = new AutoEmbed();
	if($AE->parseUrl($params['url'])){
		if(isset($params['height'])){
	       	$AE->setHeight($params['height']);
	    }
	    if(isset($params['width'])){
	    	$AE->setWidth($params['width']);
	    }
		return $AE->getEmbedCode();
	}
}


/* vim: set expandtab: */
?>