<?php

function send_smtp($from,$to,$subject,$message){
  require_once("class.phpmailer.php");
  include("class.smtp.php");

  $sname="=?UTF-8?B?".base64_encode($from)."?=\n";
  $smail= $from;
  $rname="=?UTF-8?B?".base64_encode($to)."?=\n";
  $rmail= $to;
  $sub="=?UTF-8?B?".base64_encode($subject)."?=\n";
  $body= $message;
  $mail = new PHPMailer();
  $mail->IsSMTP();
  $mail->Host = $config['smtp_host'];
  $mail->SMTPAuth = true;
  $mail->Username = $config['smtp_username'];
  $mail->Password = $config['smtp_pass'];
  $mail->AddReplyTo($smail,$sname);
  $mail->AddAddress($rmail, $rname);
  $mail->From = $smail;
  $mail->FromName = $sname;
  $mail->Subject = $sub;
  $mail->MsgHTML($body);
  $mail->IsHTML(true);
  $mail->Send();
}

function send_php($to, $subject, $message){
  global $config;
  require_once("class.phpmailer.php");
  $sname = "=?UTF-8?B?".base64_encode($config['Sitename'])."?=\n";
  $smail = $config['Email'];
  $rname = "=?UTF-8?B?".base64_encode($to)."?=\n";
  $rmail = $to;
  $sub = "=?UTF-8?B?".base64_encode($subject)."?=\n";
  $body = $message;
  $mail = new PHPMailer();
  $mail->IsMail();
  $mail->AddReplyTo($smail, $sname);
  $mail->AddAddress($rmail, $rname);
  $mail->From = $smail;
  $mail->FromName = $sname;
  $mail->Subject = $sub;
  $mail->MsgHTML($body);
  $mail->IsHTML(true);
  $mail->Send();
}

?>