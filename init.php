<?php
if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start();
session_start();

define('SMARTY_DIR','includes/Smarty/');
require_once(SMARTY_DIR . "Smarty.class.php");
$Smarty = new Smarty();
$Smarty->template_dir = "templates/default";
$Smarty->compile_dir  = "templates/default/Cache";

include("includes/config.php");
include(MOD_DIR.'/helper/functions.php');

$helper = new corehelper();

$_POST = array_map('corehelper::Security', $_POST);
$_GET = array_map('corehelper::Security', $_GET);
$_REQUEST = array_map('corehelper::Security', $_REQUEST);

$image_path = "$homeurl/templates/default/images";
$css_path = "$homeurl/templates/default/css";
$js_path = "$homeurl/includes/js";
$Smarty->assign("js_path", $js_path);
$Smarty->assign("css_path", $css_path);
$Smarty->assign("image_path", $image_path);
$Smarty->assign("homeurl", $homeurl);

$config = $db->get_row("SELECT * FROM config WHERE ID='1'", 'ARRAY_A');
$Smarty->assign("config", $config);

function redirect($redirecturl) {
  global $Smarty;
  if($redirecturl == 'GOBACK')
    $redirecturl = 'javascript:history.back()';
  echo '
	<script type="text/javascript">
	function postaction(tourl){
		if (window.timer){
			clearInterval(timer);
			clearInterval(timer_2);
		}
		window.location = tourl;
	}
	function redirect(){
		var pausefor = 2;
		setTimeout("postaction(\''.$redirecturl.'\')", pausefor*1000);
	}
	</script>
	';
  $Smarty->assign("redirecturl", $redirecturl);
  $Smarty->assign('pagetitle', 'جاري اعادة التوجيه');
  $Smarty->display("header.tpl");
  $Smarty->display("redirect.tpl");
  echo "<script>redirect();</script>";
  $Smarty->display("footer.tpl");
  die();
}

?>