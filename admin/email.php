<?php
require_once("init.php");

if($_REQUEST['action'] == 'ajaxon'){
	include_once("../userditor/fckeditor.php");
	$oFCKeditor = new FCKeditor('message') ;
	$oFCKeditor->Config['ContentLangDirection'] = $_SESSION['langdir'];
	$oFCKeditor->BasePath = '../userditor/' ;
	$oFCKeditor->Height = '400';
	$oFCKeditor->Value = '' ;
	$oFCKeditor->Create() ;
	exit;
}

$Smarty->display("header.tpl");
cp_perm_check($_SESSION['Group_ID'], '33');

if($_REQUEST['action'] == 'send'){
  $Smarty->assign("user_group", Sqlfetch("SELECT ID,Title FROM user_groups WHERE Lang_ID='$_SESSION[langid]'") );
  $Smarty->display("email_main.tpl");
}
elseif($_REQUEST['action'] == 'email'){
	if($_POST['from'] && $_POST['subject'] && $_POST['message'] && $_POST['count']){
		$from = $_POST['from'];
		$subject = $_POST['subject'];
		$message = stripslashes($_POST['message']);
		$count = $_POST['count'];

		mysql_query("TRUNCATE `email_queue`");
        if($_POST['user_group']){
			foreach($_POST['user_group'] as $key=>$id){
				$emailsql = mysql_query("SELECT Email FROM user WHERE Group_ID='$id' AND Email!=''");
				while($email_list = mysql_fetch_array($emailsql)){
					mysql_query("INSERT INTO `email_queue` (Email,Message,Fromail,Subject) VALUES ('$email_list[0]','$message','$from','$subject')");
				}
			}
		}
		echo "<script>window.location='email.php?action=email&do=send&method=$config[Email_Send_Type]&count=$count&start=0'</script>";
	}
	else if($_REQUEST['do'] == 'send'){
		require_once('../include/email_function.php');
		$emailsend = mysql_query("SELECT * FROM `email_queue` LIMIT $_GET[start],$_GET[count]");
		if(mysql_affected_rows() > 0){
			$count = $_GET['start'];
			while($emails = mysql_fetch_array($emailsend)){
				if($config['Email_Send_Type'] == 'php')
					send_php($emails['fromail'], $emails['email'], $emails['subject'], $emails['message']);
				else
					send_smtp($emails['fromail'], $emails['email'], $emails['subject'], $emails['message']);
				echo "تم الارسال بنجاح الى $emails[email]<br />";
				$count++;
			}
			$url = "email.php?action=email&do=send&method=$_GET[method]&count=$_GET[count]&start=$count";
			echo "<meta http-equiv='refresh' content=\"2 ;url=$url\">";
	    }
	    else{
	    	redirect("email.php?action=send");
	    }
	}
}
elseif($_REQUEST['action'] == 'sendone'){
	if($_POST['message']){
		require_once('../include/email_function.php');
		if($config['Email_Send_Type'] == 'php')
			send_php($_POST['from'], $_POST['email'], $_POST['subject'], $_POST['message']);
		else
			send_smtp($_POST['from'], $_POST['email'], $_POST['subject'], $_POST['message']);
		redirect('job.php?action=request&do=view&id='.$_POST['redir']);
	}
	else{
		$Smarty->display("email_one.tpl");
	}
}

$Smarty->display("footer.tpl");
?>