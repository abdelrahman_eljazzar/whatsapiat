<?php
require_once("init.php");
require_once("includes/backuplibs/db.class.php");
require_once("includes/backuplibs/gonxtabs.class.php");
require_once("includes/backuplibs/backup.class.php");

$GonxAdmin["dbhost"] = $db_host;
$GonxAdmin["dbname"] = $db_name;
$GonxAdmin["dbuser"] = $db_user;
$GonxAdmin["dbpass"] = $db_pass;
$GonxAdmin["dbtype"] = "mysql";
$GonxAdmin["compression"] = array("bz2","zlib");
$GonxAdmin["compression_default"] = "zlib";
$GonxAdmin["pagedisplay"] = 10;
$GONX["backup"] = "نسخه احتياطيه من";
$GONX["iscorrectcreat"] = "هل تم انشائها بطريقه صحيحه";
$GONX["iscorrectimport"] = "هل تم تصدير قاعدة البيانات بطرقه صحيحه";
$GONX["selectbackupfile"] = "اختار نسخه من المتاحه بالاعلى لاسترجاعها";
$GONX["importbackupfile"] = "او قم برفع النسخه من جهازك";
$GONX["delete"] = "حذف";
$GONX["nobckupfile"] = "لا يوجد نسخ احتياطيه تم اخذها مسبقا. اضغط هنا <a href=\"?go=create\">لانشاء نسخه جديده</a>";
$GONX["importbackup"] = "استرجاع قاعدة البيانات";
$GONX["importbackupdump"] = "Use MySQL Dump";
$GONX["totalbackupsize"] = "حجم النسخ الموجود الان بمجلد النسخ الاحتياطي ";
$GONX["chgdisplayorder"] = "اعادة ترتيب النتائج على حسب";
$GONX["next"] = "الصفحه التاليه";
$GONX["prev"] = "الصفحه السابقه";
$GONX["structonly"] = "نسخ بنية الجداول فقط بدون البيانات";

$Smarty->display("header.tpl");
cp_perm_check($_SESSION['Group_ID'], '26');
$Smarty->display("repaire_db_navbar.tpl");

@extract($_GET);
@extract($_POST);

$t = new gonxtabs();

switch ($go) {
    case "create":
        $db = new backup;
        $db->dbconnect($GonxAdmin["dbhost"], $GonxAdmin["dbuser"], $GonxAdmin["dbpass"], $GonxAdmin["dbname"]);
        $signature = $db->signature();
        $page .= $db->tables_menu($signature);
        break;
    case "backuptables":
        $b = new backup;
        $b->dbconnect($GonxAdmin["dbhost"], $GonxAdmin["dbuser"], $GonxAdmin["dbpass"], $GonxAdmin["dbname"]);
        $page = $b->tables_backup($tables, $structonly);
        $page = $page . $b->listbackups();
        break;
    case "generate":
        $b = new backup;
        $b->dbconnect($GonxAdmin["dbhost"], $GonxAdmin["dbuser"], $GonxAdmin["dbpass"], $GonxAdmin["dbname"]);
        $page = $b->generate();
        $page = $page . $b->listbackups();
        break;
    case "list":
        $b    = new backup;
        $page = $b->listbackups();
        break;
    case "delete":
        $b    = new backup;
        $page = $b->delete($fname);
        $page = $page . $b->listbackups();
        break;
    case "import":
        $b = new backup;
        $b->dbconnect($GonxAdmin["dbhost"], $GonxAdmin["dbuser"], $GonxAdmin["dbpass"], $GonxAdmin["dbname"]);
        $page = $b->import($bfile);
        $page = $page . $b->listbackups();
        break;
    case "importfromfile":
        $b = new backup;
        $b->dbconnect($GonxAdmin["dbhost"], $GonxAdmin["dbuser"], $GonxAdmin["dbpass"], $GonxAdmin["dbname"]);
        $page = $b->importfromfile();
        $page = $page . $b->listbackups();
        break;
    case "getbackup":
        $b = new backup;
        $b->getbackup($bfile);
        break;
}

$res .= $t->block($page, 755);
echo $res;

$Smarty->display("footer.tpl");
?>