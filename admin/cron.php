<?php
include("includes/config.php");
include("includes/notification_mail.php");
include("includes/class.sms.php");

$results = $db->get_results("SELECT ID FROM user WHERE MONTH(FROM_UNIXTIME(BirthDate)) = MONTH(CURRENT_DATE() + INTERVAL 3 DAY) AND DAYOFMONTH(FROM_UNIXTIME(BirthDate)) = DAYOFMONTH(CURRENT_DATE() + INTERVAL 3 DAY)");
if($results) {
  foreach($results as $result) {
   $results_friends = $db->get_results("select FROM_USER_IDD as FROM_USER_ID ,TO_USER_IDD as TO_USER_ID from ( SELECT From_User_ID as FROM_USER_IDD , To_User_ID as TO_USER_IDD FROM `user_friends` where Active = 1 and Send_Birthday=0 union SELECT To_User_ID as FROM_USER_IDD , From_User_ID as TO_USER_IDD FROM `user_friends` where Active = 1  and Send_Birthday=0) a WHERE FROM_USER_IDD=".$result->ID);
   if($results_friends) {
    foreach($results_friends as $result_friend) {
      $db->query("UPDATE `user_friends` SET Send_Birthday=1 WHERE (From_User_ID='".$result_friend->FROM_USER_ID."' && To_User_ID='".$result_friend->TO_USER_ID."') OR (To_User_ID='".$result_friend->FROM_USER_ID."' && From_User_ID='".$result_friend->TO_USER_ID."')");
      NotifyBirthDayByMail($result_friend->TO_USER_ID,$result_friend->FROM_USER_ID);
     }
   }
  }
}

$results = $db->get_results("SELECT pay_id,user_id,Days FROM (SELECT payments.id as pay_id,user.ID as user_id,DATEDIFF(FROM_UNIXTIME(payments.endtime),CURRENT_TIMESTAMP()) AS Days FROM user INNER JOIN payments on user.ID=payments.userid WHERE DATEDIFF(FROM_UNIXTIME(payments.endtime),CURRENT_TIMESTAMP())>=1 AND DATEDIFF(FROM_UNIXTIME(payments.endtime),CURRENT_TIMESTAMP())<=7 ORDER BY payments.id DESC ) a group by user_id ");
if ($results) {
  foreach ($results as $result) {
    NotifyMemberShipByMail($result->user_id, $result->Days);
  }
}

//Update ages by birthdate
$curyear = date('Y');
$curday = date('d');
$curmonth = date('m');
$birthdates = $db->get_results("SELECT ID,YEAR(BirthDate) AS year FROM user WHERE DAY(BirthDate)='$curday' AND MONTH(BirthDate)='$curmonth'", 'ARRAY_A');
if($birthdates){
  foreach($birthdates as $birthdate){
    $db->query("UPDATE user SET Age='".($curyear-$birthdate['year'])."' WHERE ID='$birthdate[ID]'");
  }
}

//Advertisments endtime check
$config = $db->get_row("SELECT * FROM artgine_config WHERE ID='1'", 'ARRAY_A');
$ads = $db->get_results("SELECT * FROM adver WHERE Active='1'", 'ARRAY_A');
if($ads){
  foreach($ads as $ad){
    if(TIMENOW > $ad['End']){
      $db->query("UPDATE adver SET Active='0' WHERE ID='$ad[ID]'");
      send_php($config['Email'], $ad['Email'], 'انتهاء الفترة الأعلانية', 'نرسل اليكم لننبهكم بانتهاء الفترة الأعلانية الخاصة بكم لدينا وفي حالة الرغبة بالتجديد يرجى التواصل معنا عن طريق احدى طرق التواصل المتاحة لنا');
    }
    if(($ad['Report_Time']+604800) < TIMENOW && !empty($ad['Report_Time'])){
      $db->query("UPDATE adver SET Report_Time='".TIMENOW."' WHERE ID='$ad[ID]'");
      send_php($config['Email'], $ad['Email'], '', 'عدد زيارات الوحدة الأعلانية الخاصة بكم حتى هذا الوقت = '.$ad['Read_Counter']);
    }
  }
}

//Payments endtime check
$users = $db->get_results("SELECT ID,Group_ID FROM user", 'ARRAY_A');
if($users){
  foreach($users as $user){
    $group = $db->get_row("SELECT * FROM user_groups WHERE ID='$user[Group_ID]'", 'ARRAY_A');
    $data = array();
    if($group['Msgs_Type'] == 1){
      $data[] = array('Num_Msgs' => 0);
    }
    if($group['Cards_Type'] == 1){
      $data[] = array('Num_Cards' => 0);
    }
    if($group['Voice_Type'] == 1){
      $data[] = array('Num_Voice' => 0);
    }
    if($group['SMS_Type'] == 1){
      $data[] = array('Num_Sms' => 0);
    }
    if($group['Chat_Type'] == 1){
      $data[] = array('Num_Chat' => 0);
    }
    if($group['Gifts_Type'] == 1){
      $data[] = array('Num_Gifts' => 0);
    }
    $db->update('user', $data, array('ID' => $user['ID']));
  }
}

//reset failed payment attempts
$failedpays = $db->get_results("SELECT * FROM pay_band WHERE ".TIMENOW.">endtime", 'ARRAY_A');
if($failedpays){
  foreach($failedpays as $failedpay){
    $db->query("UPDATE user SET Active='1',Pay_Failed='0' WHERE ID='$failedpay[userid]'");
    $db->query("DELETE FROM pay_band WHERE id='$failedpay[id]'");
  }
}

//payments cron
$paytimenow = TIMENOW;
$payments = $db->get_results("SELECT * FROM payments WHERE $paytimenow>endtime", 'ARRAY_A');
if($payments){
  foreach($payments as $payment){
    if($payment['payment_type'] == 'inapp'){
      $bool = getReceiptData($payment['txnid'], true);
      if($bool == 1){
        $db->query("UPDATE payments SET createdtime='$paytimenow',endtime='".(TIMENOW+(30*24*60*60))."' WHERE ID='$payment[id]'");
        continue;
      }
      elseif($bool == 2){
        continue;
      }
    }
    $db->query("UPDATE user SET Group_ID='1',ExpireDate='' WHERE ID='$payment[userid]'");
  }
}

function getReceiptData($receipt, $isSandbox = false){
  // determine which endpoint to use for verifying the receipt
  if ($isSandbox) {
      $endpoint = 'https://sandbox.itunes.apple.com/verifyReceipt';
  }
  else {
      $endpoint = 'https://buy.itunes.apple.com/verifyReceipt';
  }

  // build the post data
  $postData = json_encode(array('receipt-data' => base64_encode($receipt), "password" => '93bb68dd1351498b85e707a2adea7a4f'));

  // create the cURL request
  $ch = curl_init($endpoint);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: '.strlen($postData)));

  // execute the cURL request and fetch response data
  $response = curl_exec($ch);
  $errno    = curl_errno($ch);
  $errmsg   = curl_error($ch);
  curl_close($ch);

  // ensure the request succeeded
  if ($errno != 0){
    throw new Exception($errmsg, $errno);
  }

  // parse the response data
  $data = json_decode($response);

  // ensure response data was a valid JSON string
  if (!is_object($data)) {
      return 2;
  }

  // ensure the expected data is present
  if (!isset($data->status)) {
      return 2;
  }

  if(time() < strtotime($data->latest_expired_receipt_info->expires_date_formatted)){
    return 1;
  }
  else{
    return 0;
  }
}

?>