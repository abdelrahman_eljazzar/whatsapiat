<?php /* Smarty version Smarty-3.1.16, created on 2014-06-25 08:07:20
         compiled from "style/templates/sidebar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5058631865396d85ae72fc0-86096181%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd79b052811d2f8fe41bde0a77fcc008cbdae13b7' => 
    array (
      0 => 'style/templates/sidebar.tpl',
      1 => 1403701636,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5058631865396d85ae72fc0-86096181',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5396d85b6e5d85_92589500',
  'variables' => 
  array (
    'spec_meta' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5396d85b6e5d85_92589500')) {function content_5396d85b6e5d85_92589500($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>لوحة التحكم</title>
      <link type="text/css" rel="stylesheet" href="style/style.css" />
      <link rel="stylesheet" type="text/css" href="style/mbContainer.css" />
      <!--[if IE]>
      <link type="text/css" rel="stylesheet" href="style/IE.css" />
      <![endif]-->
      <script type="text/javascript" src="style/jquery.js"></script>
      <script type="text/javascript" src="style/effects.core.js"></script>
      <script type="text/javascript" src="style/effects.blind.js"></script>
      <script type="text/javascript" src="style/ui.core.min.js"></script>
      <script type="text/javascript" src="style/ui.resizable.min.js"></script>
      <script type="text/javascript" src="style/jquery.metadata.js"></script>
      <script type="text/javascript" src="style/mbContainer.js"></script>
      <?php if (isset($_smarty_tpl->tpl_vars['spec_meta']->value)) {?><?php echo $_smarty_tpl->tpl_vars['spec_meta']->value;?>
<?php }?>
   </head>
   <body>
      
      <script type="text/javascript">
       $(function(){
         $(".containerPlus").buildContainers({
           containment:"document",
           elementsPath:"style/images/container/",
         });
       });
      </script>
      
      <div id="right">
         <a target="leftFrame" href="home.php"><img alt="" src="style/images/logo.png" id="logo"/></a>
         <br /><br />
         <input type="button" class="bttn" value="Expand / Collapse All" onclick="$('.containerPlus').mb_toggle()" />
         <br />
         <input type="button" class="bttn btnsmall" value="Expand All" onclick="$('.containerPlus').mb_expandall()" />
         <input type="button" class="bttn btnsmall" value="Collapse All" onclick="$('.containerPlus').mb_unexpand()" />
         <br /><br />
         <table cellspacing="0" cellpadding="0" border="0">
          <?php echo $_smarty_tpl->getSubTemplate ("container_open.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"اعدادات الموقع",'id'=>"setting"), 0);?>

            <tr>
               <td class="tdnor"><a style="color:#990000" href="setting.php?action=save" target="leftFrame">اعدادات الموقع</a></td>
            </tr>
            <tr>
               <td class="tdnor"><a href="contact.php" target="leftFrame">رسائل اتصل بنا</a></td>
            </tr>
            <?php echo $_smarty_tpl->getSubTemplate ("container_close.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

         <?php echo $_smarty_tpl->getSubTemplate ("container_open.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"التحكم فى الاقسام",'id'=>"users_group"), 0);?>

            <tr>
               <td class="tdnor"><a href="categories.php" target="leftFrame">التحكم فى الاقسام</a></td>
            </tr>
            <tr>
               <td class="tdnor"><a href="categories.php?add" target="leftFrame">اضافة قسم جديد</a></td>
            </tr>
            <?php echo $_smarty_tpl->getSubTemplate ("container_close.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <?php echo $_smarty_tpl->getSubTemplate ("container_open.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"التحكم فى الاعضاء",'id'=>"users"), 0);?>

            <tr>
               <td class="tdnor"><a href="users.php" target="leftFrame">التحكم فى الاعضاء</a></td>
            </tr>
             <tr>
               <td class="tdnor"><a href="users.php?action=add" target="leftFrame">اضافة عضو جديد</a></td>
            </tr>
            <?php echo $_smarty_tpl->getSubTemplate ("container_close.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


            <?php echo $_smarty_tpl->getSubTemplate ("container_open.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"التحكم فى الرسائل الجاهزة",'id'=>"msg_cat"), 0);?>

             <tr>
               <td class="tdnor"><a href="msg_templates.php" target="leftFrame">التحكم فى الرسائل الجاهزة</a></td>
            </tr>
            <tr>
               <td class="tdnor"><a href="msg_templates.php?action=add" target="leftFrame">اضافة رسالة جديدة</a></td>
            </tr>
            <tr>
               <td class="tdnor"><a href="msg_categories.php" target="leftFrame">التحكم فى اقسام الرسائل الجاهزه</a></td>
            </tr>
            <tr>
               <td class="tdnor"><a href="msg_categories.php?action=add" target="leftFrame">اضافة قسم جديد</a></td>
            </tr>
            <?php echo $_smarty_tpl->getSubTemplate ("container_close.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


             <?php echo $_smarty_tpl->getSubTemplate ("container_open.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"احصائيات الدفع",'id'=>"payments"), 0);?>

            <tr>
               <td class="tdnor"><a href="payments.php" target="leftFrame">احصائيات الدفع</a></td>
            </tr>
            <?php echo $_smarty_tpl->getSubTemplate ("container_close.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


            <?php echo $_smarty_tpl->getSubTemplate ("container_open.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"التحكم فى المدراء",'id'=>"managers"), 0);?>

            <tr>
               <td class="tdnor"><a href="managers.php" target="leftFrame">التحكم فى المدراء</a></td>
            </tr>
             <tr>
               <td class="tdnor"><a href="managers.php?action=add" target="leftFrame">اضافة مدير جديد</a></td>
            </tr>
            <?php echo $_smarty_tpl->getSubTemplate ("container_close.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


             <?php echo $_smarty_tpl->getSubTemplate ("container_open.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"التحكم فى النقاط",'id'=>"managers"), 0);?>

            <tr>
               <td class="tdnor"><a href="membership.php" target="leftFrame">التحكم فى النقاط</a></td>
            </tr>
            <?php echo $_smarty_tpl->getSubTemplate ("container_close.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

            <?php echo $_smarty_tpl->getSubTemplate ("container_open.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"التحكم فى الصفحات",'id'=>"pages"), 0);?>

            <tr>
               <td class="tdnor"><a href="pages.php" target="leftFrame">التحكم فى الصفحات</a></td>
            </tr>
             <tr>
               <td class="tdnor"><a href="pages.php?action=add" target="leftFrame">اضافة صفحة جديدة</a></td>
            </tr>
            <?php echo $_smarty_tpl->getSubTemplate ("container_close.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

         </table>
      </div>
   </body>
</html><?php }} ?>
