<?php /* Smarty version Smarty-3.1.16, created on 2014-06-12 08:54:01
         compiled from "style/templates/setting_show.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16266863435399ae6f8a0285-59896880%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0748b558a99b44ac1e401469001d2f204d480f98' => 
    array (
      0 => 'style/templates/setting_show.tpl',
      1 => 1402581212,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16266863435399ae6f8a0285-59896880',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5399ae6f9c0d15_33509582',
  'variables' => 
  array (
    'setting' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5399ae6f9c0d15_33509582')) {function content_5399ae6f9c0d15_33509582($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ('setting_navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<form method="post" action="setting.php?action=save">
<table id="predtable">

<tr><th class="th_rad" colspan="2">اعدادات الموقع</th></tr>

<tr><td class="norm">اسم الموقع</td>
<td class="altpoll"><input name="sitename" type="text" size="40" value="<?php echo $_smarty_tpl->tpl_vars['setting']->value['Sitename'];?>
" /></td></tr>

<tr><td class="norm">البريد الالكتروني</td>
<td class="altpoll"><input dir="ltr" name="email" type="text" size="40" value="<?php echo $_smarty_tpl->tpl_vars['setting']->value['Email'];?>
" /><br /><br />
البريد الالكتروني المستخدم في عمليات المراسلة في كامل الموقع يفضل ان يكون بامتداد الموقع.</td></tr>

<tr><td class="norm">وصف الموقع</td><td class="altpoll">
<input name="metadesc" type="text" size="60" value="<?php echo $_smarty_tpl->tpl_vars['setting']->value['Metadesc'];?>
" /></td></tr>

<tr><td class="norm">كلمات مفتاحية</td><td class="altpoll">
<input name="metakeyword" type="text" size="60" value="<?php echo $_smarty_tpl->tpl_vars['setting']->value['Metakeyword'];?>
" /></td></tr>

<tr><td class="norm">نص حقوق الموقع</td><td class="altpoll">
<input name="copyright" type="text" size="60" value="<?php echo $_smarty_tpl->tpl_vars['setting']->value['Copyright'];?>
" /></td></tr>

<tr><td class="norm">فتج/اغلاق الموقع</td><td class="altpoll">
<input name="active" type="radio" value="1" <?php if ($_smarty_tpl->tpl_vars['setting']->value['Active']==1) {?>checked="checked"<?php }?> />فتح&nbsp;&nbsp;
<input name="active" type="radio" value="0" <?php if ($_smarty_tpl->tpl_vars['setting']->value['Active']==0) {?>checked="checked"<?php }?> />اغلاق</td></tr>

<tr><td class="norm">رسالة الاغلاق</td><td class="altpoll">
<textarea name="message" cols="30" rows="10"><?php echo $_smarty_tpl->tpl_vars['setting']->value['Closemsg'];?>
</textarea></td></tr>

<tr><td class="norm">النقاط المجانية عند التسجيل</td><td class="altpoll">
<input name="points" type="text" size="60" value="<?php echo $_smarty_tpl->tpl_vars['setting']->value['Default_Points'];?>
" /></td></tr>

<tr><th colspan="2">اعدادات شبكة مزود SMS</th></tr>

<tr><td class="norm">مزود الخدمة:</td>
<td class="altpoll"><select name="api_provider">
<option value="0">اختر مزود خدمة</option>
<option value="4jawaly.net" <?php if ($_smarty_tpl->tpl_vars['setting']->value['Api_Provider']=='4jawaly.net') {?>selected="selected"<?php }?>>شركة فور جوالي</option>
<option value="mobily.com" <?php if ($_smarty_tpl->tpl_vars['setting']->value['Api_Provider']=='mobily.com') {?>selected="selected"<?php }?>>شركة موبايلي</option>
<option value="stc.com" <?php if ($_smarty_tpl->tpl_vars['setting']->value['Api_Provider']=='stc.com') {?>selected="selected"<?php }?>>شركة الاتصالات السعودية</option>
<option value="mobily.ws" <?php if ($_smarty_tpl->tpl_vars['setting']->value['Api_Provider']=='mobily.ws') {?>selected="selected"<?php }?>>شركة موبايلي WS</option>
<option value="resalty.net" <?php if ($_smarty_tpl->tpl_vars['setting']->value['Api_Provider']=='resalty.net') {?>selected="selected"<?php }?>>شركة رسالتي.نت</option>
</select>
<img onclick="CheckBalance()" id="SMSBal_do" src="style/images/sms-dollar.png" title="رصيدك بمزود الخدمة" class="pointer" />
<span id="SMSBal" style="color: green;display:none;"><img src="style/images/progressbar2.gif" alt="" class="pointer" /></span>
</td></tr>

<tr><td class="norm">اسم المستخدم</td>
<td class="altpoll"><input name="api_user" type="text" size="40" value="<?php echo $_smarty_tpl->tpl_vars['setting']->value['Api_User'];?>
" dir="ltr" /></td></tr>

<tr><td class="norm">كلمة المرور</td>
<td class="altpoll"><input name="api_pass" type="text" size="40" value="<?php echo $_smarty_tpl->tpl_vars['setting']->value['Api_Pass'];?>
" dir="ltr" /></td></tr>

<tr><td class="norm">اسم المرسل الافتراضي</td>
<td class="altpoll"><input name="sndrname" type="text" size="40" value="<?php echo $_smarty_tpl->tpl_vars['setting']->value['Sndrname'];?>
" /></td></tr>

<tr><th colspan="2">اعدادات ارسال البريد</th></tr>

<tr><td class="norm">طريقة الارسال</td>
<td class="altpoll"><select name="email_send_type">
<option value="php">PHP</option>
<option value="smtp" <?php if ($_smarty_tpl->tpl_vars['setting']->value['Email_Send_Type']=='smtp') {?>selected="selected"<?php }?>>SMTP</option>
</select></td></tr>

<tr><td class="norm">الهوست</td>
<td class="altpoll"><input name="smtp_host" type="text" value="<?php echo $_smarty_tpl->tpl_vars['setting']->value['Smtp_Host'];?>
" size="30" dir="ltr" /></td></tr>

<tr><td class="norm">اسم المستخدم</td>
<td class="altpoll"><input name="smtp_username" type="text" value="<?php echo $_smarty_tpl->tpl_vars['setting']->value['Smtp_Username'];?>
" dir="ltr" /></td></tr>

<tr><td class="norm">كلمة المرور</td>
<td class="altpoll"><input name="smtp_pass" type="text" value="<?php echo $_smarty_tpl->tpl_vars['setting']->value['Smtp_Pass'];?>
" dir="ltr" /></td></tr>

<tr><td colspan="2" class="retb_tab th_rad"><center><input type="submit" class="button" value="حفـــظ" />&nbsp;
	<input class="button" type="reset" value="استعادة التعديلات" /></center></td></tr>

</table>
</form><?php }} ?>
