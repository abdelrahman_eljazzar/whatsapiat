<?php /* Smarty version Smarty-3.1.16, created on 2014-06-11 08:03:14
         compiled from "style/templates/msg_templates/msg_templates_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:293858898539816936b3df5-43483070%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f46ee1d6cfb9728cfca6cdba046da59c8da9e1e4' => 
    array (
      0 => 'style/templates/msg_templates/msg_templates_form.tpl',
      1 => 1402491788,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '293858898539816936b3df5-43483070',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5398169372cc69_73232630',
  'variables' => 
  array (
    'msg' => 0,
    'depts' => 0,
    'action' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5398169372cc69_73232630')) {function content_5398169372cc69_73232630($_smarty_tpl) {?>
<script type="text/javascript" charset="utf-8">
function createXMLHttp() {
    if (typeof XMLHttpRequest != "undefined") {
        return new XMLHttpRequest();
    } else if (window.ActiveXObject) {
      var aVersions = [ "MSXML2.XMLHttp.5.0",
        "MSXML2.XMLHttp.4.0","MSXML2.XMLHttp.3.0",
        "MSXML2.XMLHttp","Microsoft.XMLHttp"
      ];
      for (var i = 0; i < aVersions.length; i++) {
        try {
            var oXmlHttp = new ActiveXObject(aVersions[i]);

            return oXmlHttp;

        } catch (oError) {

            //Do nothing
        }
      }
    }
    throw new Error("XMLHttp object could be created.");
}
function Delete_Msgs(id,type)
{

	req_obj=createXMLHttp();

	url="ajax/delete_row.php?id="+id+"&type="+type;

	req_obj.open("GET", url, true);

	req_obj.onreadystatechange = function()
	{
		if (req_obj.readyState == 4 && req_obj.status == 200)
		{
             document.getElementById(type).style.display = "none";
        }

	};
	req_obj.send(null);

}

</script>

<?php echo $_smarty_tpl->getSubTemplate ('msg_categories/msg_categories_navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<form method="post" action=""  enctype="multipart/form-data">
<input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['msg']->value['ID'];?>
" />
<input type="hidden" name="count" value="<?php echo $_smarty_tpl->tpl_vars['msg']->value['Count'];?>
" />

<table id="predtable">

<tr><th class="th_rad" colspan="2">التحكم فى الرسائل الجاهزة</th></tr>


<tr><td class="norm">القسم:</td><td class="altpoll">
<select name="dept_id" id="dept_id" style="width: 150px">
<option value="" >اختر القسم</option>
<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['op'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['op']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['name'] = 'op';
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['depts']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total']);
?>
  <option value="<?php echo $_smarty_tpl->tpl_vars['depts']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['ID'];?>
" <?php if ($_smarty_tpl->tpl_vars['depts']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['ID']==$_smarty_tpl->tpl_vars['msg']->value['Dept_ID']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['depts']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['Title'];?>
</option>
<?php endfor; endif; ?>
</select>
</td></tr>

<tr><td class="norm">العنوان:</td>
<td class="altpoll"><input type="text" name="title" value="<?php echo $_smarty_tpl->tpl_vars['msg']->value['Title'];?>
" size="50"/></td></tr>


<tr><td class="norm">محتوى الرسالة:</td>
<td class="altpoll"><textarea  name="content" rows="3" cols="40"><?php echo $_smarty_tpl->tpl_vars['msg']->value['Text'];?>
</textarea></td></tr>

<tr><td class="norm">الصورة:</td>
<td class="altpoll">
<div id="Image">
<?php if ($_smarty_tpl->tpl_vars['action']->value=='edit'&&$_smarty_tpl->tpl_vars['msg']->value['Image']!='') {?>
<a target="_blank" href="../uploads/<?php echo $_smarty_tpl->tpl_vars['msg']->value['Image'];?>
"><img src="../uploads/<?php echo $_smarty_tpl->tpl_vars['msg']->value['Image'];?>
" width='60px' hieght='60px'/></a> &nbsp;&nbsp;&nbsp; <a href="javascript:Delete_Msgs(<?php echo $_smarty_tpl->tpl_vars['msg']->value['ID'];?>
,'Image')">مسح هذه الصورة</a><br />
<?php }?>
</div>
<input name="upload" type="file" size="40" /><br />مسموح بامتدادات (jpg, jpeg, png, gif) باقصى حجم 1 ميجا.</td></tr>

<tr><td class="norm">رسالة صوتية:</td>
<td class="altpoll">
<div id="Audio">
<?php if ($_smarty_tpl->tpl_vars['action']->value=='edit'&&$_smarty_tpl->tpl_vars['msg']->value['Audio']!='') {?>
<a  href="download.php?download=<?php echo $_smarty_tpl->tpl_vars['msg']->value['Audio'];?>
">تحميل لسماع الرسالة الصوتية</a> &nbsp;&nbsp;&nbsp; <a href="javascript:Delete_Msgs(<?php echo $_smarty_tpl->tpl_vars['msg']->value['ID'];?>
,'Audio')">مسح هذه الرسالة</a><br />
<?php }?>
</div>
<input name="upload_audio" type="file" size="40" /><br />مسموح بامتدادات (mp3, mp4, wma) </td></tr>

<tr><td class="norm">فيديو:</td>
<td class="altpoll">
<div id="Video">
<?php if ($_smarty_tpl->tpl_vars['action']->value=='edit'&&$_smarty_tpl->tpl_vars['msg']->value['Video']!='') {?>
<a  href="download.php?download=<?php echo $_smarty_tpl->tpl_vars['msg']->value['Video'];?>
">تحميل لمشاهدة هذا الفيديو</a> &nbsp;&nbsp;&nbsp; <a href="javascript:Delete_Msgs(<?php echo $_smarty_tpl->tpl_vars['msg']->value['ID'];?>
,'Video')">مسح هذه الرسالة</a><br />
<?php }?>
</div>
<input name="upload_video" type="file" size="40" /><br />مسموح بامتدادات (mp3, mp4, wma) </td></tr>

<tr><td class="retb_tab" colspan="2"><center>
<input class="button" name="add" type="submit" value="حفظ واعادة تحميل" />
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" class="button" name="cancel"  onclick="history.back();" value="الغاء"></center></td></tr>

</table>
</form><?php }} ?>
