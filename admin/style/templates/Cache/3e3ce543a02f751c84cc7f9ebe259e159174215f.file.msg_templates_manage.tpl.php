<?php /* Smarty version Smarty-3.1.16, created on 2014-07-01 03:24:50
         compiled from "style/templates/msg_templates/msg_templates_manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:134857831653983fc31b3357-21867170%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3e3ce543a02f751c84cc7f9ebe259e159174215f' => 
    array (
      0 => 'style/templates/msg_templates/msg_templates_manage.tpl',
      1 => 1404203087,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '134857831653983fc31b3357-21867170',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53983fc3295898_49552004',
  'variables' => 
  array (
    'msgs' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53983fc3295898_49552004')) {function content_53983fc3295898_49552004($_smarty_tpl) {?>

<script type="text/javascript">

$(document).ready(function() {

    $("#predtable").tablesorter({headers: { 0: { sorter: false}, 3: {sorter: false}, 4: {sorter: false}, 6: {sorter: false}, 7: {sorter: false}}}); 

});

</script>





<?php echo $_smarty_tpl->getSubTemplate ('msg_categories/msg_categories_navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>




<form method="post" action="msg_templates.php?action=multich">

<table id="predtable">

<thead>

<tr>

<th class="th_rad_right" style="text-align:center"><input type="checkbox" onclick="checkDelBoxes(this.form, 'delaction[]', this.checked)" /></th>

<th>ID</th>

<th>العنوان</th>
<th>المحتوى</th>
<th>الصورة</th>
<th>الرسالة الصوتية</th>
<th>الفيديو</th>
<th>القسم</th>
<th class="th_rad_left">عمليات</th>

</tr>

</thead>



<?php if (isset($_smarty_tpl->tpl_vars['msgs']->value)) {?>

<tbody>

<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['op'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['op']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['name'] = 'op';
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['msgs']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total']);
?>

<tr>

<td style="width:5%; text-align:center" class="norm"><input name="delaction[]" type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['msgs']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['ID'];?>
" /></td>

<td style="width:5%; text-align:center" class="altpoll"><?php echo $_smarty_tpl->tpl_vars['msgs']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['ID'];?>
</td>

<td class="norm"><?php echo $_smarty_tpl->tpl_vars['msgs']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['Title'];?>
</td>

<td class="norm"><?php echo $_smarty_tpl->tpl_vars['msgs']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['Text'];?>
</td>

<td class="norm"><?php if ($_smarty_tpl->tpl_vars['msgs']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['Image']!='') {?><a target="_blank" href="../uploads/<?php echo $_smarty_tpl->tpl_vars['msgs']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['Image'];?>
"><img src="../uploads/<?php echo $_smarty_tpl->tpl_vars['msgs']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['Image'];?>
" width='60px' hieght='60px'/></a><?php }?></td>

<td class="norm"><?php if ($_smarty_tpl->tpl_vars['msgs']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['Audio']!='') {?><a href="download.php?download=<?php echo $_smarty_tpl->tpl_vars['msgs']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['Audio'];?>
">تحميل</a><?php }?></td>

<td class="norm"><?php if ($_smarty_tpl->tpl_vars['msgs']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['Video']!='') {?><a href="download.php?download=<?php echo $_smarty_tpl->tpl_vars['msgs']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['Video'];?>
">تحميل</a><?php }?></td>

<td class="norm"><a href=""><?php echo $_smarty_tpl->tpl_vars['msgs']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['Dept_Title'];?>
</a></td>

<td class="norm">

<a href="msg_templates.php?action=edit&id=<?php echo $_smarty_tpl->tpl_vars['msgs']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['ID'];?>
"><img src="style/images/pencil.png" title="تعديل" alt="تعديل" /></a>

<a style="cursor:pointer" onclick="DeleteVer('msg_templates.php?action=delete&id=<?php echo $_smarty_tpl->tpl_vars['msgs']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['ID'];?>
')">

<img src="style/images/cross.png" title="حذف" alt="حذف" /></a>

</td>

</tr>

<?php endfor; endif; ?>

</tbody>

<?php } else { ?>

<tr><td style="text-align:center" colspan="9" class="norm">لا يوجد نتائج لعرض</td></tr>

<?php }?>



<tr><td class="retb_tab" colspan="9"><?php echo $_smarty_tpl->getSubTemplate ('paging_class.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<div style="float:right">

<select name="doaction">

    <option>اختر عملية للتنفيذ</option>

    <option value="delete">حذف</option>

</select>

<input type="submit" class="button" value="تنفيذ على المختار" /></div>

</td></tr>



</table>

</form><?php }} ?>
