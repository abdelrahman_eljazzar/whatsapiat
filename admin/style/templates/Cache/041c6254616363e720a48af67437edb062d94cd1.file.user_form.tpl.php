<?php /* Smarty version Smarty-3.1.16, created on 2014-06-25 08:05:22
         compiled from "style/templates/users/user_form.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1670612001539945da1dd025-45236677%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '041c6254616363e720a48af67437edb062d94cd1' => 
    array (
      0 => 'style/templates/users/user_form.tpl',
      1 => 1403701520,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1670612001539945da1dd025-45236677',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_539945da3dd2e4_85725055',
  'variables' => 
  array (
    'user' => 0,
    'groups' => 0,
    'plans' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_539945da3dd2e4_85725055')) {function content_539945da3dd2e4_85725055($_smarty_tpl) {?><?php echo $_smarty_tpl->getSubTemplate ('users/user_navbar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<form method="post" action="" enctype="multipart/form-data">

<input value="<?php echo $_smarty_tpl->tpl_vars['user']->value['ID'];?>
" name="id" type="hidden" />

<table id="predtable">



<tr><th class="th_rad" colspan="2">التحكم فى الاعضاء</th></tr>

<tr><td class="norm">الاهتمامات:</td><td class="altpoll">
<select name="dept_id" id="dept_id" style="width: 150px">
<option value="" >اختر</option>
<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['op'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['op']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['name'] = 'op';
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['groups']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total']);
?>
  <option value="<?php echo $_smarty_tpl->tpl_vars['groups']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['ID'];?>
" <?php if ($_smarty_tpl->tpl_vars['groups']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['ID']==$_smarty_tpl->tpl_vars['user']->value['Cat_ID']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['groups']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['Title'];?>
</option>
<?php endfor; endif; ?>
</select>
</td></tr>

<tr><td class="norm">نوعية الحساب :</td><td class="altpoll">
<select name="type_user" id="type_user" style="width: 150px">
<option value="" >اختر</option>
<option value="1" <?php if ($_smarty_tpl->tpl_vars['user']->value['User_Type']==1) {?> selected="selected"<?php }?>>شركة ناشئة</option>
<option value="2" <?php if ($_smarty_tpl->tpl_vars['user']->value['User_Type']==2) {?> selected="selected"<?php }?>>مطور منفرد</option>
</select>
</td></tr>

<tr><td class="norm">نوعية التسجيل:</td><td class="altpoll">
<select name="type_regitser" id="type_regitser" style="width: 150px">
<option value="" >اختر</option>
<option value="1" <?php if ($_smarty_tpl->tpl_vars['user']->value['Register_Type']==1) {?> selected="selected"<?php }?>>عضو يستخدم الحساب الخاص به</option>
<option value="2" <?php if ($_smarty_tpl->tpl_vars['user']->value['Register_Type']==2) {?> selected="selected"<?php }?>>عضو عادى</option>
<option value="3" <?php if ($_smarty_tpl->tpl_vars['user']->value['Register_Type']==3) {?> selected="selected"<?php }?>>شركه تريد رقم دعم فني خاص بها </option>
</select>
</td></tr>

<tr><td class="norm">العضوية:</td><td class="altpoll">
<select name="plan_id" id="plan_id" style="width: 150px">
<option value="" >اختر</option>
<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['op'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['op']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['name'] = 'op';
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['plans']->value) ? count($_loop) : max(0, (int) $_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['op']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['op']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['op']['total']);
?>
  <option value="<?php echo $_smarty_tpl->tpl_vars['plans']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['ID'];?>
" <?php if ($_smarty_tpl->tpl_vars['plans']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['ID']==$_smarty_tpl->tpl_vars['user']->value['Plan_ID']) {?>selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['plans']->value[$_smarty_tpl->getVariable('smarty')->value['section']['op']['index']]['Title'];?>
</option>
<?php endfor; endif; ?>
</select>
</td></tr>

<tr><td class="norm">الاسم بالكامل:</td>

<td class="altpoll"><input value="<?php echo $_smarty_tpl->tpl_vars['user']->value['Title'];?>
" name="title" type="text" size="40" /></td></tr>



<tr><td class="norm">الايميل:</td>

<td class="altpoll"><input value="<?php echo $_smarty_tpl->tpl_vars['user']->value['Email'];?>
" name="email" type="text" size="40" /></td></tr>



<tr><td class="norm">الموبايل:</td>

<td class="altpoll"><input value="<?php echo $_smarty_tpl->tpl_vars['user']->value['Number'];?>
" name="mobile" type="text" size="40" /></td></tr>


<tr><td class="norm">كلمة السر:</td>

<td class="altpoll"><input value="" name="pass" type="password" size="40" /></td></tr>

<tr><td class="norm">النقط:</td>

<td class="altpoll"><input name="points" type="text" size="40" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['Points'];?>
"/></td></tr>

<tr><td class="norm">النقاط المستخدمة:</td>

<td class="altpoll"><input name="used_points" type="text" size="40" value="<?php echo $_smarty_tpl->tpl_vars['user']->value['Used_Points'];?>
"/></td></tr>



<tr><td class="norm">تفعيل:</td>

<td class="altpoll"><input value="1" name="active" type="checkbox" size="40" <?php if ($_smarty_tpl->tpl_vars['user']->value['Active']==1) {?> checked <?php }?>/></td></tr>



<tr><td class="retb_tab" colspan="2"><center>

	<input class="button" name="edit" type="submit" value="حفظ البيانات" />

	<input class="button" type="reset" value="استعادة التعديلات" /></center></td></tr>



</table>

</form><?php }} ?>
