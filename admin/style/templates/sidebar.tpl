<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>لوحة التحكم</title>
      <link type="text/css" rel="stylesheet" href="style/style.css" />
      <link rel="stylesheet" type="text/css" href="style/mbContainer.css" />
      <!--[if IE]>
      <link type="text/css" rel="stylesheet" href="style/IE.css" />
      <![endif]-->
      <script type="text/javascript" src="style/jquery.js"></script>
      <script type="text/javascript" src="style/effects.core.js"></script>
      <script type="text/javascript" src="style/effects.blind.js"></script>
      <script type="text/javascript" src="style/ui.core.min.js"></script>
      <script type="text/javascript" src="style/ui.resizable.min.js"></script>
      <script type="text/javascript" src="style/jquery.metadata.js"></script>
      <script type="text/javascript" src="style/mbContainer.js"></script>
      {if isset($spec_meta)}{$spec_meta}{/if}
   </head>
   <body>
      {literal}
      <script type="text/javascript">
       $(function(){
         $(".containerPlus").buildContainers({
           containment:"document",
           elementsPath:"style/images/container/",
         });
       });
      </script>
      {/literal}
      <div id="right">
         <a target="leftFrame" href="home.php"><img alt="" src="style/images/logo.png" id="logo"/></a>
         <br /><br />
         <input type="button" class="bttn" value="Expand / Collapse All" onclick="$('.containerPlus').mb_toggle()" />
         <br />
         <input type="button" class="bttn btnsmall" value="Expand All" onclick="$('.containerPlus').mb_expandall()" />
         <input type="button" class="bttn btnsmall" value="Collapse All" onclick="$('.containerPlus').mb_unexpand()" />
         <br /><br />
         <table cellspacing="0" cellpadding="0" border="0">
          {include file="container_open.tpl" title="اعدادات الموقع" id="setting"}
            <tr>
               <td class="tdnor"><a style="color:#990000" href="setting.php?action=save" target="leftFrame">اعدادات الموقع</a></td>
            </tr>
            <tr>
               <td class="tdnor"><a href="contact.php" target="leftFrame">رسائل اتصل بنا</a></td>
            </tr>
            {include file="container_close.tpl"}
         {include file="container_open.tpl" title="التحكم فى الاقسام" id="users_group"}
            <tr>
               <td class="tdnor"><a href="categories.php" target="leftFrame">التحكم فى الاقسام</a></td>
            </tr>
            <tr>
               <td class="tdnor"><a href="categories.php?add" target="leftFrame">اضافة قسم جديد</a></td>
            </tr>
            {include file="container_close.tpl"}
            {include file="container_open.tpl" title="التحكم فى الاعضاء" id="users"}
            <tr>
               <td class="tdnor"><a href="users.php" target="leftFrame">التحكم فى الاعضاء</a></td>
            </tr>
             <tr>
               <td class="tdnor"><a href="users.php?action=add" target="leftFrame">اضافة عضو جديد</a></td>
            </tr>
            {include file="container_close.tpl"}

            {include file="container_open.tpl" title="التحكم فى الرسائل الجاهزة" id="msg_cat"}
             <tr>
               <td class="tdnor"><a href="msg_templates.php" target="leftFrame">التحكم فى الرسائل الجاهزة</a></td>
            </tr>
            <tr>
               <td class="tdnor"><a href="msg_templates.php?action=add" target="leftFrame">اضافة رسالة جديدة</a></td>
            </tr>
            <tr>
               <td class="tdnor"><a href="msg_categories.php" target="leftFrame">التحكم فى اقسام الرسائل الجاهزه</a></td>
            </tr>
            <tr>
               <td class="tdnor"><a href="msg_categories.php?action=add" target="leftFrame">اضافة قسم جديد</a></td>
            </tr>
            {include file="container_close.tpl"}

             {include file="container_open.tpl" title="احصائيات الدفع" id="payments"}
            <tr>
               <td class="tdnor"><a href="payments.php" target="leftFrame">احصائيات الدفع</a></td>
            </tr>
            {include file="container_close.tpl"}

            {include file="container_open.tpl" title="التحكم فى المدراء" id="managers"}
            <tr>
               <td class="tdnor"><a href="managers.php" target="leftFrame">التحكم فى المدراء</a></td>
            </tr>
             <tr>
               <td class="tdnor"><a href="managers.php?action=add" target="leftFrame">اضافة مدير جديد</a></td>
            </tr>
            {include file="container_close.tpl"}

             {include file="container_open.tpl" title="التحكم فى النقاط" id="managers"}
            <tr>
               <td class="tdnor"><a href="membership.php" target="leftFrame">التحكم فى النقاط</a></td>
            </tr>
            {include file="container_close.tpl"}
            {include file="container_open.tpl" title="التحكم فى الصفحات" id="pages"}
            <tr>
               <td class="tdnor"><a href="pages.php" target="leftFrame">التحكم فى الصفحات</a></td>
            </tr>
             <tr>
               <td class="tdnor"><a href="pages.php?action=add" target="leftFrame">اضافة صفحة جديدة</a></td>
            </tr>
            {include file="container_close.tpl"}
         </table>
      </div>
   </body>
</html>