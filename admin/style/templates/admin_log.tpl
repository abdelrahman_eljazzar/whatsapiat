{literal}
<script type="text/javascript">
$(document).ready(function() {
    $("#predtable").tablesorter({headers: { 0: { sorter: false}, 4: {sorter: false} }});
});
</script>
{/literal}

<input type="image" src="{$site_home}/admin/style/images/print.gif" alt="print" onClick="window.print()" style="width: 40px; height: 40px; float: left"/>
<br/><br/><br/><br/>
<form method="post" action="admin_log.php?action=multich">
<table id="predtable">
<thead>
<tr>
<th>ID</th>
<th>الفعل</th>
<th>التاريخ</th>
</tr>
</thead>

{if $groupmanage}
<tbody>
{section name=op loop=$groupmanage}
<tr>

<td style="width:6%; text-align:center" class="altpoll">{$groupmanage[op].ID}</td>
<td class="norm">{$groupmanage[op].Action}</td>
<td class="norm">{$groupmanage[op].DateTime|date_format}</td>
</tr>
{/section}
</tbody>
{else}
<tr><td style="text-align:center" colspan="5" class="norm">لا يوجد سجل حتى الان</td></tr>
{/if}

<tr><td class="retb_tab" colspan="6">{include file='paging_class.tpl'}

</td></tr>

</table>
</form>