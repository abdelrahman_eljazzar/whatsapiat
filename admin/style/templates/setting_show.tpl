{include file='setting_navbar.tpl'}

<form method="post" action="setting.php?action=save">
<table id="predtable">

<tr><th class="th_rad" colspan="2">اعدادات الموقع</th></tr>

<tr><td class="norm">اسم الموقع</td>
<td class="altpoll"><input name="sitename" type="text" size="40" value="{$setting.Sitename}" /></td></tr>

<tr><td class="norm">البريد الالكتروني</td>
<td class="altpoll"><input dir="ltr" name="email" type="text" size="40" value="{$setting.Email}" /><br /><br />
البريد الالكتروني المستخدم في عمليات المراسلة في كامل الموقع يفضل ان يكون بامتداد الموقع.</td></tr>

<tr><td class="norm">وصف الموقع</td><td class="altpoll">
<input name="metadesc" type="text" size="60" value="{$setting.Metadesc}" /></td></tr>

<tr><td class="norm">كلمات مفتاحية</td><td class="altpoll">
<input name="metakeyword" type="text" size="60" value="{$setting.Metakeyword}" /></td></tr>

<tr><td class="norm">نص حقوق الموقع</td><td class="altpoll">
<input name="copyright" type="text" size="60" value="{$setting.Copyright}" /></td></tr>

<tr><td class="norm">فتج/اغلاق الموقع</td><td class="altpoll">
<input name="active" type="radio" value="1" {if $setting.Active eq 1}checked="checked"{/if} />فتح&nbsp;&nbsp;
<input name="active" type="radio" value="0" {if $setting.Active eq 0}checked="checked"{/if} />اغلاق</td></tr>

<tr><td class="norm">رسالة الاغلاق</td><td class="altpoll">
<textarea name="message" cols="30" rows="10">{$setting.Closemsg}</textarea></td></tr>

<tr><td class="norm">النقاط المجانية عند التسجيل</td><td class="altpoll">
<input name="points" type="text" size="60" value="{$setting.Default_Points}" /></td></tr>

<tr><th colspan="2">اعدادات شبكة مزود SMS</th></tr>

<tr><td class="norm">مزود الخدمة:</td>
<td class="altpoll"><select name="api_provider">
<option value="0">اختر مزود خدمة</option>
<option value="4jawaly.net" {if $setting.Api_Provider eq '4jawaly.net'}selected="selected"{/if}>شركة فور جوالي</option>
<option value="mobily.com" {if $setting.Api_Provider eq 'mobily.com'}selected="selected"{/if}>شركة موبايلي</option>
<option value="stc.com" {if $setting.Api_Provider eq 'stc.com'}selected="selected"{/if}>شركة الاتصالات السعودية</option>
<option value="mobily.ws" {if $setting.Api_Provider eq 'mobily.ws'}selected="selected"{/if}>شركة موبايلي WS</option>
<option value="resalty.net" {if $setting.Api_Provider eq 'resalty.net'}selected="selected"{/if}>شركة رسالتي.نت</option>
</select>
<img onclick="CheckBalance()" id="SMSBal_do" src="style/images/sms-dollar.png" title="رصيدك بمزود الخدمة" class="pointer" />
<span id="SMSBal" style="color: green;display:none;"><img src="style/images/progressbar2.gif" alt="" class="pointer" /></span>
</td></tr>

<tr><td class="norm">اسم المستخدم</td>
<td class="altpoll"><input name="api_user" type="text" size="40" value="{$setting.Api_User}" dir="ltr" /></td></tr>

<tr><td class="norm">كلمة المرور</td>
<td class="altpoll"><input name="api_pass" type="text" size="40" value="{$setting.Api_Pass}" dir="ltr" /></td></tr>

<tr><td class="norm">اسم المرسل الافتراضي</td>
<td class="altpoll"><input name="sndrname" type="text" size="40" value="{$setting.Sndrname}" /></td></tr>

<tr><th colspan="2">اعدادات ارسال البريد</th></tr>

<tr><td class="norm">طريقة الارسال</td>
<td class="altpoll"><select name="email_send_type">
<option value="php">PHP</option>
<option value="smtp" {if $setting.Email_Send_Type eq 'smtp'}selected="selected"{/if}>SMTP</option>
</select></td></tr>

<tr><td class="norm">الهوست</td>
<td class="altpoll"><input name="smtp_host" type="text" value="{$setting.Smtp_Host}" size="30" dir="ltr" /></td></tr>

<tr><td class="norm">اسم المستخدم</td>
<td class="altpoll"><input name="smtp_username" type="text" value="{$setting.Smtp_Username}" dir="ltr" /></td></tr>

<tr><td class="norm">كلمة المرور</td>
<td class="altpoll"><input name="smtp_pass" type="text" value="{$setting.Smtp_Pass}" dir="ltr" /></td></tr>

<tr><td colspan="2" class="retb_tab th_rad"><center><input type="submit" class="button" value="حفـــظ" />&nbsp;
	<input class="button" type="reset" value="استعادة التعديلات" /></center></td></tr>

</table>
</form>