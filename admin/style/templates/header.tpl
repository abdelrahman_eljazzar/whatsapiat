<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">



<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>لوحة التحكم</title>



<link type="text/css" rel="stylesheet" href="style/style.css" />

<!--[if IE]>

<link type="text/css" rel="stylesheet" href="style/IE.css" />

<![endif]-->



<script type="text/javascript" src="style/jquery.1.7.3.js"></script>

<script type="text/javascript" src="style/jquery.validationEngine.js"></script>

{literal}

<script type="text/javascript">

var jqnc = jQuery.noConflict();

jqnc( document ).ready(function() {

  jqnc(".form-validation").validationEngine();

});

</script>

{/literal}

<script type="text/javascript" src="style/jquery.js"></script>

<script type="text/javascript" src="style/effects.core.js"></script>

<script type="text/javascript" src="style/effects.blind.js"></script>

<script type="text/javascript" src="style/jquery.tablesorter.js"></script>

<script type="text/javascript" src="style/function.js"></script>

<script type="text/javascript" src="style/multi_input.js"></script>

<script type="text/javascript" src="style/jquery.progressbar.min.js"></script>

{if isset($spec_meta)}{$spec_meta}{/if}



</head>
<body>



<div id="topholder">



<div style="float:right;margin-top: 5px;width: 170px;overflow: hidden;">

  مرحبا بك, <span>{$admin_se_user}</span>

</div>

<ul>
  <li><a href="home.php">رئيسية لوحة التحكم</a></li>

  <li><a href="setting.php?action=save">اعدادات الموقع</a></li>

  <li class="nobg"><a href="contact.php">صندوق البريد ({$inboxcount})</a></li>

</ul>

<div style="float:right;margin-right:6%;">

  <a href="../" class="bttn btnsmall" target="_blank">الصفحة الرئيسية</a>

  <a href="login.php?action=logout" class="bttn btnsmall">تسجيل خروج</a>

</div>



</div>



<div id="left">