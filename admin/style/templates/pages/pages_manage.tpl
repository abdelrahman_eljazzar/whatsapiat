{literal}
<script type="text/javascript">
$(document).ready(function() {
    $("#predtable").tablesorter({headers: { 0: { sorter: false}, 4: {sorter: false} }});
});
</script>
{/literal}

{include file='pages/pages_navbar.tpl'}

<form method="post" action="pages.php?action=multich">
<table id="predtable">
<thead>
<tr>
<th class="th_rad_right" style="text-align:center"><input type="checkbox" onclick="checkDelBoxes(this.form, 'delaction[]', this.checked)" /></th>
<th>ID</th>
<th>العنوان</th>
<th>الحالة</th>
<th class="th_rad_left">العمليات</th>
</tr>
</thead>

{if $usermanage}
<tbody>
{section name=op loop=$usermanage}
<tr>
<td style="width:5%; text-align:center" class="norm"><input name="delaction[]" type="checkbox" value="{$usermanage[op].ID}" /></td>
<td style="width:6%; text-align:center" class="altpoll">{$usermanage[op].ID}</td>
<td class="norm">{$usermanage[op].Title}</td>
<td class="norm" style="text-align:center">

{if $usermanage[op].Active eq 1}

<div id="Active_e_{$usermanage[op].ID}"><img style="cursor:pointer" onclick="ChangeStat('pages',{$usermanage[op].ID},'Active',0)" src="style/images/enable.gif" title="تعطيل" alt="تعطيل" /></div>


<div id="Active_d_{$usermanage[op].ID}" style="display:none;cursor:pointer"><img onclick="ChangeStat('pages',{$usermanage[op].ID},'Active',1)" src="style/images/disable.gif" title="تفعيل" alt="تفعيل" /></div>


{else}

<div id="Active_d_{$usermanage[op].ID}"><img style="cursor:pointer" onclick="ChangeStat('pages',{$usermanage[op].ID},'Active',1)" src="style/images/disable.gif" title="تفعيل" alt="تفعيل" /></div>


<div id="Active_e_{$usermanage[op].ID}" style="display:none;cursor:pointer"><img onclick="ChangeStat('pages',{$usermanage[op].ID},'Active',0)" src="style/images/enable.gif" title="تعطيل" alt="تعطيل" /></div>

{/if}
</td>
<td class="norm">
 <a href="pages.php?action=edit&id={$usermanage[op].ID}"><img src="style/images/pencil.png" title="تعديل" alt="تعديل"></a>
<a onclick="DeleteVer('pages.php?action=delete&id={$usermanage[op].ID}')"><img src="style/images/cross.png" title="حذف" alt="حذف"></a>
</td>
</tr>
{/section}
</tbody>
{else}
<tr><td style="text-align:center" colspan="5" class="norm">لا يوجد بيانات</td></tr>
{/if}

<tr><td class="retb_tab" colspan="6">{include file='paging_class.tpl'}
<div style="float:right">
<select name="doaction">
    <option>اختر اي عملية للتنفيذ...</option>
    <option value="delete">حذف</option>
</select>
<input type="submit" class="button" value="تنفيذ على المختار" /></div>
</td></tr>

</table>
</form>