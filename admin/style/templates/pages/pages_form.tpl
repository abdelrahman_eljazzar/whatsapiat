<script src="includes/editor/ckeditor.js"></script>
<link rel="stylesheet" href="includes/editor/sample.css"/>
{include file='pages/pages_navbar.tpl'}

<form method="post" action="">
<input value="{$userinfo.ID}" name="id" type="hidden" />
<table id="predtable">

<tr><th class="th_rad" colspan="2">التحكم فى الصفحات</th></tr>

<tr><td class="norm">العنوان:</td>
<td class="altpoll"><input value="{$userinfo.Title}" name="title" type="text" size="40" /></td></tr>

<tr><td class="norm">المحتوى:</td>
<td class="altpoll"><textarea  name="content" id="content" class="ckeditor">{$userinfo.Content}</textarea></td></tr>

<tr><td class="norm">تنشيط الصفحة:</td>

<td class="altpoll"><input {if $userinfo.Active eq 1}checked="checked"{/if} name="active" type="checkbox" /></td></tr>

<tr><td class="norm">اللغة:</td>

<td class="altpoll">
<select name="lang" id="lang">
<option value="">اختر اللغة</option>
<option value="ar" {if $userinfo.Lang eq 'ar'} selected="selected" {/if}>العربية</option>
<option value="en" {if $userinfo.Lang eq 'en'} selected="selected" {/if}>الانجليزية</option>
</select>
</td></tr>


<tr><td class="retb_tab" colspan="2"><center>
	<input class="button" name="edit" type="submit" value="حفظ واعادة تحميل" />
	&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" class="button" name="cancel"  onclick="history.back();" value="الغاء"></center></td></tr>

</table>
</form>