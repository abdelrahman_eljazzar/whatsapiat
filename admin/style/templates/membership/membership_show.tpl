{literal}
<script type="text/javascript">
$(document).ready(function() {
    $("#predtable").tablesorter({headers: { 0: { sorter: false}, 4: {sorter: false} }});
});
</script>
{/literal}

{include file='membership/membership_navbar.tpl'}

<form method="post" action="membership.php?action=multich">
<table id="predtable">
<thead>
<tr>
<th class="th_rad_right" style="text-align:center"><input type="checkbox" onclick="checkDelBoxes(this.form, 'delaction[]', this.checked)" /></th>
<th>ID</th>
<th>العنوان</th>
<th>عدد النقط</th>
<th>السعر</th>
<th class="th_rad_left">العمليات</th>
</tr>
</thead>

{if $infos}
<tbody>
{section name=op loop=$infos}
<tr>

<td style="width:5%; text-align:center" class="norm">{if $infos[op].ID neq 1 && $infos[op].ID neq 2}<input name="delaction[]" type="checkbox" value="{$infos[op].ID}" />{/if}</td>

<td style="width:6%; text-align:center" class="altpoll">{$infos[op].ID}</td>
<td class="norm">{$infos[op].Title}</td>
<td class="norm">{$infos[op].Points}</td>
<td class="altpoll">{$infos[op].Cost}</td>
<td class="norm">
 <a href="membership.php?action=edit&id={$infos[op].ID}"><img src="style/images/pencil.png" title="تعديل" alt="تعديل"></a>
{if $infos[op].ID neq 1 && $infos[op].ID neq 2}
<a onclick="DeleteVer('membership.php?action=delete&id={$infos[op].ID}')"><img src="style/images/cross.png" title="حذف" alt="حذف"></a>
{/if}
</td>
</tr>
{/section}
</tbody>
{else}
<tr><td style="text-align:center" colspan="5" class="norm">لا يوجد بيانات</td></tr>
{/if}

<tr><td class="retb_tab" colspan="6">{include file='paging_class.tpl'}
<div style="float:right">
<select name="doaction">
    <option>اختر اي عملية للتنفيذ...</option>
    <option value="delete">حذف</option>
</select>
<input type="submit" class="button" value="تنفيذ على المختار" /></div>
</td></tr>

</table>
</form>