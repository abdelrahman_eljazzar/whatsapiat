{literal}

<script type="text/javascript">

$(document).ready(function() {

    $("#predtable").tablesorter({headers: { 0: { sorter: false}, 4: {sorter: false} }});

});

</script>

{/literal}

{include file='users/user_navbar.tpl'}

<form method="post" action="users.php?action=multich">

<table id="predtable">

<thead>

<tr>

<th>ID</th>

<th>عنوان المجموعة</th>

<th>عدد الاعضاء داخل المجموعة</th>

</tr>
</thead>



{if $groupmanage}

<tbody>

{section name=op loop=$groupmanage}

<tr>


<td style="width:6%; text-align:center" class="altpoll">{$groupmanage[op].ID}</td>

<td class="norm">{$groupmanage[op].Title}</td>

<td class="norm">{$groupmanage[op].contacts_count}</td>

</tr>

{/section}

</tbody>

{else}

<tr><td style="text-align:center" colspan="18" class="norm">لا يوجد بيانات</td></tr>

{/if}



<tr><td class="retb_tab" colspan="18">{include file='paging_class.tpl'}

</td></tr>



</table>

</form>