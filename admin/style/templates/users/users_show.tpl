{literal}

<script type="text/javascript">

$(document).ready(function() {

    $("#predtable").tablesorter({headers: { 0: { sorter: false}, 4: {sorter: false} }});

});

</script>

{/literal}

{include file='users/user_navbar.tpl'}

<form method="post" action="users.php?action=multich">

<table id="predtable">

<thead>

<tr>

<th class="th_rad_right" style="text-align:center"><input type="checkbox" onclick="checkDelBoxes(this.form, 'delaction[]', this.checked)" /></th>

<th>ID</th>

<th>الاسم</th>

<th>رقم الموبايل</th>

<th>نوعية الحساب</th>

<th>نوعية التسجيل</th>

<th>النقط</th>

<th>النقط المستخدمة</th>

<th>الاهتمامات</th>

<th>المجموعات</th>

<th>الرسائل الجاهزة المرسلة</th>

<th>الرسائل المرسلة</th>

<th>ارسال رسالة</th>

<th>الحالة</th>

<th class="th_rad_left">العمليات</th>



</tr>
</thead>



{if $groupmanage}

<tbody>

{section name=op loop=$groupmanage}

<tr>

<td style="width:5%; text-align:center" class="norm"><input name="delaction[]" type="checkbox" value="{$groupmanage[op].ID}" /></td>

<td style="width:6%; text-align:center" class="altpoll">{$groupmanage[op].ID}</td>

<td class="norm">{$groupmanage[op].Title}</td>

<!--td class="norm">{$groupmanage[op].Email}</td-->

<td class="norm">{$groupmanage[op].Number}</td>

<td class="norm">
{if $groupmanage[op].User_Type eq 1}
شركة ناشئة
{else if $groupmanage[op].User_Type eq 2}
مطور منفرد
{/if}
</td>

<td class="norm">
{if $groupmanage[op].Register_Type 	 eq 1}
عضو يستخدم الحساب الخاص به
{else if $groupmanage[op].Register_Type eq 2}
عضو عادى
{else if $groupmanage[op].Register_Type eq 3}
شركه تريد رقم دعم فني خاص بها
{/if}
</td>

<td class="norm">{$groupmanage[op].Points}</td>

<td class="norm">{$groupmanage[op].Used_Points}</td>

<td class="norm">{$groupmanage[op].Category_Title}</td>

<td class="norm"><a href="users.php?action=user_groups&id={$groupmanage[op].ID}">مشاهدة المجموعات</a></td>

<td class="norm"><a href="users.php?action=sent_templates&id={$groupmanage[op].ID}">مشاهدة</a></td>

<td class="norm"><a href="users.php?action=sent_messages&id={$groupmanage[op].ID}">مشاهدة</a></td>

<td class="norm"><a href="users.php?action=send_one_message&id={$groupmanage[op].ID}">ارسال</a></td>

<td class="norm" style="text-align:center">

{if $groupmanage[op].Active eq 1}

<div id="Active_e_{$groupmanage[op].ID}"><img style="cursor:pointer" onclick="ChangeStat('users',{$groupmanage[op].ID},'Active',0)" src="style/images/enable.gif" title="تعطيل" alt="تعطيل" /></div>


<div id="Active_d_{$groupmanage[op].ID}" style="display:none;cursor:pointer"><img onclick="ChangeStat('users',{$groupmanage[op].ID},'Active',1)" src="style/images/disable.gif" title="تفعيل" alt="تفعيل" /></div>


{else}

<div id="Active_d_{$groupmanage[op].ID}"><img style="cursor:pointer" onclick="ChangeStat('users',{$groupmanage[op].ID},'Active',1)" src="style/images/disable.gif" title="تفعيل" alt="تفعيل" /></div>


<div id="Active_e_{$groupmanage[op].ID}" style="display:none;cursor:pointer"><img onclick="ChangeStat('users',{$groupmanage[op].ID},'Active',0)" src="style/images/enable.gif" title="تعطيل" alt="تعطيل" /></div>

{/if}
</td>

<td class="norm">

<a href="users.php?action=edit&id={$groupmanage[op].ID}"><img src="style/images/pencil.png" title="تعديل" alt="تعديل" /></a>

<a onclick="DeleteVer('users.php?action=delete&id={$groupmanage[op].ID}')"><img src="style/images/cross.png" title="حذف" alt="حذف"></a>

</td>

</tr>

{/section}

</tbody>

{else}

<tr><td style="text-align:center" colspan="18" class="norm">لا يوجد اعضاء</td></tr>

{/if}



<tr><td class="retb_tab" colspan="18">{include file='paging_class.tpl'}
<div style="float:right">

<select name="doaction">

    <option>اختر اي عملية للتنفيذ...</option>

    <option value="delete">حذف</option>

</select>

<input type="submit" class="button" value="تنفيذ على المختار" /></div>
</td></tr>



</table>

</form>