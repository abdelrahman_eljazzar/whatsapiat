{literal}

<script type="text/javascript">

$(document).ready(function() {

    $("#predtable").tablesorter({headers: { 0: { sorter: false}, 4: {sorter: false} }});

});

</script>

{/literal}

{include file='users/user_navbar.tpl'}

<form method="post" action="users.php?action=multich">

<table id="predtable">

<thead>

<tr>

<th>ID</th>

<th>عنوان الرسالة</th>

<th>المحتوى</th>

<th>الصورة</th>

<th>الرسالة الصوتية</th>

<th>الفيديو</th>

<th>المجموعة المرسل لها</th>

</tr>
</thead>



{if $groupmanage}

<tbody>

{section name=op loop=$groupmanage}

<tr>


<td style="width:6%; text-align:center" class="altpoll">{$groupmanage[op].ID}</td>

<td class="norm">{$groupmanage[op].Message}</td>

<td class="norm">{$groupmanage[op].Text}</td>

<td class="norm">{if $groupmanage[op].Image neq ''}<a target="_blank" href="../uploads/{$groupmanage[op].Image}"><img src="../uploads/{$groupmanage[op].Image}" width='60px' hieght='60px'/></a>{/if}</td>

<td class="norm">{if $groupmanage[op].Audio neq ''}<a href="download.php?download={$groupmanage[op].Audio }">تحميل</a>{/if}</td>

<td class="norm">{if $groupmanage[op].Video neq ''}<a href="download.php?download={$groupmanage[op].Video }">تحميل</a>{/if}</td>

<td class="norm">{$groupmanage[op].Group_Title}</td>

</tr>

{/section}

</tbody>

{else}

<tr><td style="text-align:center" colspan="18" class="norm">لا يوجد بيانات</td></tr>

{/if}



<tr><td class="retb_tab" colspan="18">{include file='paging_class.tpl'}

</td></tr>



</table>

</form>