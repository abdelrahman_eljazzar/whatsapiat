{include file='users/user_navbar.tpl'}

<form method="post" action="" enctype="multipart/form-data">

<input value="{$user.ID}" name="id" type="hidden" />

<table id="predtable">



<tr><th class="th_rad" colspan="2">التحكم فى الاعضاء</th></tr>

<tr><td class="norm">الاهتمامات:</td><td class="altpoll">
<select name="dept_id" id="dept_id" style="width: 150px">
<option value="" >اختر</option>
{section name=op loop=$groups}
  <option value="{$groups[op].ID}" {if $groups[op].ID eq $user.Cat_ID}selected="selected"{/if}>{$groups[op].Title}</option>
{/section}
</select>
</td></tr>

<tr><td class="norm">نوعية الحساب :</td><td class="altpoll">
<select name="type_user" id="type_user" style="width: 150px">
<option value="" >اختر</option>
<option value="1" {if $user.User_Type eq 1} selected="selected"{/if}>شركة ناشئة</option>
<option value="2" {if  $user.User_Type eq 2} selected="selected"{/if}>مطور منفرد</option>
</select>
</td></tr>

<tr><td class="norm">نوعية التسجيل:</td><td class="altpoll">
<select name="type_regitser" id="type_regitser" style="width: 150px">
<option value="" >اختر</option>
<option value="1" {if $user.Register_Type eq 1} selected="selected"{/if}>عضو يستخدم الحساب الخاص به</option>
<option value="2" {if $user.Register_Type eq 2} selected="selected"{/if}>عضو عادى</option>
<option value="3" {if $user.Register_Type eq 3} selected="selected"{/if}>شركه تريد رقم دعم فني خاص بها </option>
</select>
</td></tr>

<tr><td class="norm">العضوية:</td><td class="altpoll">
<select name="plan_id" id="plan_id" style="width: 150px">
<option value="" >اختر</option>
{section name=op loop=$plans}
  <option value="{$plans[op].ID}" {if $plans[op].ID eq $user.Plan_ID}selected="selected"{/if}>{$plans[op].Title}</option>
{/section}
</select>
</td></tr>

<tr><td class="norm">الاسم بالكامل:</td>

<td class="altpoll"><input value="{$user.Title}" name="title" type="text" size="40" /></td></tr>



<tr><td class="norm">الايميل:</td>

<td class="altpoll"><input value="{$user.Email}" name="email" type="text" size="40" /></td></tr>



<tr><td class="norm">الموبايل:</td>

<td class="altpoll"><input value="{$user.Number}" name="mobile" type="text" size="40" /></td></tr>


<tr><td class="norm">كلمة السر:</td>

<td class="altpoll"><input value="" name="pass" type="password" size="40" /></td></tr>

<tr><td class="norm">النقط:</td>

<td class="altpoll"><input name="points" type="text" size="40" value="{$user.Points}"/></td></tr>

<tr><td class="norm">النقاط المستخدمة:</td>

<td class="altpoll"><input name="used_points" type="text" size="40" value="{$user.Used_Points}"/></td></tr>



<tr><td class="norm">تفعيل:</td>

<td class="altpoll"><input value="1" name="active" type="checkbox" size="40" {if $user.Active eq 1} checked {/if}/></td></tr>



<tr><td class="retb_tab" colspan="2"><center>

	<input class="button" name="edit" type="submit" value="حفظ البيانات" />

	<input class="button" type="reset" value="استعادة التعديلات" /></center></td></tr>



</table>

</form>