{literal}

<script type="text/javascript">

$(document).ready(function() {

    $("#predtable").tablesorter({headers: { 0: { sorter: false}, 3: {sorter: false}, 4: {sorter: false}, 6: {sorter: false}, 7: {sorter: false}}}); 

});

</script>

{/literal}



{include file='msg_categories/msg_categories_navbar.tpl'}



<form method="post" action="msg_categories.php?action=multich">

<table id="predtable">

<thead>

<tr>

<th class="th_rad_right" style="text-align:center"><input type="checkbox" onclick="checkDelBoxes(this.form, 'delaction[]', this.checked)" /></th>

<th>ID</th>

<th>العنوان</th>

<th class="th_rad_left">عمليات</th>

</tr>

</thead>



{if isset($depts)}

<tbody>

{section name=op loop=$depts}

<tr>

<td style="width:5%; text-align:center" class="norm"><input name="delaction[]" type="checkbox" value="{$depts[op].ID}" /></td>

<td style="width:5%; text-align:center" class="altpoll">{$depts[op].ID}</td>

<td class="norm">{$depts[op].Title}</td>

<td class="norm">

<a href="msg_categories.php?action=edit&id={$depts[op].ID}"><img src="style/images/pencil.png" title="تعديل" alt="تعديل" /></a>

<a style="cursor:pointer" onclick="DeleteVer('msg_categories.php?action=delete&id={$depts[op].ID}')">

<img src="style/images/cross.png" title="حذف" alt="حذف" /></a>

</td>

</tr>

{/section}

</tbody>

{else}

<tr><td style="text-align:center" colspan="8" class="norm">لا يوجد نتائج لعرض</td></tr>

{/if}



<tr><td class="retb_tab" colspan="8">{include file='paging_class.tpl'}
<div style="float:right">

<select name="doaction">

    <option>اختر عملية للتنفيذ</option>

    <option value="delete">حذف</option>

</select>

<input type="submit" class="button" value="تنفيذ على المختار" /></div>

</td></tr>



</table>

</form>