{literal}
<script type="text/javascript">
$(document).ready(function() {
    $("#predtable").tablesorter({headers: { 0: { sorter: false}, 5: {sorter: false} }}); 
});
</script>
{/literal}
{include file='payments/payment_navbar.tpl'}
<input type="image" src="{$site_home}/admin/style/images/print.gif" alt="print" onClick="window.print()" style="width: 40px; height: 40px; float: left"/>
<br/><br/><br/><br/>
<form method="post" action="payments.php?action=multich">
<table id="predtable">
<thead>
<tr>
<th>ID</th>
<th>العضو</th>
<th>النقط</th>
<th>السعر</th>
<th>نظام الدفع</th>
<th>التاريخ</th>
</tr>
</thead>
{if isset($section)}
<tbody>
{section name=op loop=$section}
<tr>
<td style="width:5%; text-align:center" class="altpoll">{$section[op].id}</td>
<td class="norm">{$section[op].User_Title}</td>
<td class="norm">{$section[op].Points}</td>
<td class="norm">{$section[op].Cost}</td>
{if $section[op].Pay_Type eq 'paypal'}
{assign var="type" value="بواسطة باى بال"}
{else if $section[op].Pay_Type eq 'cashu'}
{assign var="type" value="بواسطه كاش يو"}
{else if $section[op].Pay_Type eq 'onecard'}
{assign var="type" value="بواسطة وان كارد"}
{else if $section[op].Pay_Type eq 'inapp'}
{assign var="type" value="بواسطة الموبايل"}
{else if $section[op].Pay_Type eq 'bank_transfer'}
{assign var="type" value="بواسطة التحويل البنكى"}
{else}
{assign var="type" value=""}
{/if}
<td class="norm">{$type}</td>
<td class="norm">{$section[op].From_Date|date_format}</td>
</tr>
{/section}
</tbody>
{else}
<tr><td colspan="10" style="text-align:center" class="norm">لم يتم العثور على اي نتائج</td></tr>
{/if}

<tr><td class="retb_tab" colspan="10">{include file='paging_class.tpl'}

</td></tr>

</table>
</form>