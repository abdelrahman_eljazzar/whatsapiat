{literal}
<script type="text/javascript" charset="utf-8">
function createXMLHttp() {
    if (typeof XMLHttpRequest != "undefined") {
        return new XMLHttpRequest();
    } else if (window.ActiveXObject) {
      var aVersions = [ "MSXML2.XMLHttp.5.0",
        "MSXML2.XMLHttp.4.0","MSXML2.XMLHttp.3.0",
        "MSXML2.XMLHttp","Microsoft.XMLHttp"
      ];
      for (var i = 0; i < aVersions.length; i++) {
        try {
            var oXmlHttp = new ActiveXObject(aVersions[i]);

            return oXmlHttp;

        } catch (oError) {

            //Do nothing
        }
      }
    }
    throw new Error("XMLHttp object could be created.");
}
function Delete_Msgs(id,type)
{

	req_obj=createXMLHttp();

	url="ajax/delete_row.php?id="+id+"&type="+type;

	req_obj.open("GET", url, true);

	req_obj.onreadystatechange = function()
	{
		if (req_obj.readyState == 4 && req_obj.status == 200)
		{
             document.getElementById(type).style.display = "none";
        }

	};
	req_obj.send(null);

}

</script>
{/literal}
{include file='msg_categories/msg_categories_navbar.tpl'}

<form method="post" action=""  enctype="multipart/form-data">
<input type="hidden" name="id" value="{$msg.ID}" />
<input type="hidden" name="count" value="{$msg.Count}" />

<table id="predtable">

<tr><th class="th_rad" colspan="2">التحكم فى الرسائل الجاهزة</th></tr>


<tr><td class="norm">القسم:</td><td class="altpoll">
<select name="dept_id" id="dept_id" style="width: 150px">
<option value="" >اختر القسم</option>
{section name=op loop=$depts}
  <option value="{$depts[op].ID}" {if $depts[op].ID eq $msg.Dept_ID}selected="selected"{/if}>{$depts[op].Title}</option>
{/section}
</select>
</td></tr>

<tr><td class="norm">العنوان:</td>
<td class="altpoll"><input type="text" name="title" value="{$msg.Title}" size="50"/></td></tr>


<tr><td class="norm">محتوى الرسالة:</td>
<td class="altpoll"><textarea  name="content" rows="3" cols="40">{$msg.Text}</textarea></td></tr>

<tr><td class="norm">الصورة:</td>
<td class="altpoll">
<div id="Image">
{if $action eq  'edit' && $msg.Image neq ''}
<a target="_blank" href="../uploads/{$msg.Image}"><img src="../uploads/{$msg.Image}" width='60px' hieght='60px'/></a> &nbsp;&nbsp;&nbsp; <a href="javascript:Delete_Msgs({$msg.ID},'Image')">مسح هذه الصورة</a><br />
{/if}
</div>
<input name="upload" type="file" size="40" /><br />مسموح بامتدادات (jpg, jpeg, png, gif) باقصى حجم 1 ميجا.</td></tr>

<tr><td class="norm">رسالة صوتية:</td>
<td class="altpoll">
<div id="Audio">
{if $action eq  'edit' && $msg.Audio neq ''}
<a  href="download.php?download={$msg.Audio}">تحميل لسماع الرسالة الصوتية</a> &nbsp;&nbsp;&nbsp; <a href="javascript:Delete_Msgs({$msg.ID},'Audio')">مسح هذه الرسالة</a><br />
{/if}
</div>
<input name="upload_audio" type="file" size="40" /><br />مسموح بامتدادات (mp3, mp4, wma) </td></tr>

<tr><td class="norm">فيديو:</td>
<td class="altpoll">
<div id="Video">
{if $action eq  'edit' && $msg.Video neq ''}
<a  href="download.php?download={$msg.Video}">تحميل لمشاهدة هذا الفيديو</a> &nbsp;&nbsp;&nbsp; <a href="javascript:Delete_Msgs({$msg.ID},'Video')">مسح هذه الرسالة</a><br />
{/if}
</div>
<input name="upload_video" type="file" size="40" /><br />مسموح بامتدادات (mp3, mp4, wma) </td></tr>

<tr><td class="retb_tab" colspan="2"><center>
<input class="button" name="add" type="submit" value="حفظ واعادة تحميل" />
&nbsp;&nbsp;&nbsp;&nbsp;
<input type="button" class="button" name="cancel"  onclick="history.back();" value="الغاء"></center></td></tr>

</table>
</form>