{literal}

<script type="text/javascript">

$(document).ready(function() {

    $("#predtable").tablesorter({headers: { 0: { sorter: false}, 3: {sorter: false}, 4: {sorter: false}, 6: {sorter: false}, 7: {sorter: false}}}); 

});

</script>

{/literal}



{include file='msg_categories/msg_categories_navbar.tpl'}



<form method="post" action="msg_templates.php?action=multich">

<table id="predtable">

<thead>

<tr>

<th class="th_rad_right" style="text-align:center"><input type="checkbox" onclick="checkDelBoxes(this.form, 'delaction[]', this.checked)" /></th>

<th>ID</th>

<th>العنوان</th>
<th>المحتوى</th>
<th>الصورة</th>
<th>الرسالة الصوتية</th>
<th>الفيديو</th>
<th>القسم</th>
<th class="th_rad_left">عمليات</th>

</tr>

</thead>



{if isset($msgs)}

<tbody>

{section name=op loop=$msgs}

<tr>

<td style="width:5%; text-align:center" class="norm"><input name="delaction[]" type="checkbox" value="{$msgs[op].ID}" /></td>

<td style="width:5%; text-align:center" class="altpoll">{$msgs[op].ID}</td>

<td class="norm">{$msgs[op].Title}</td>

<td class="norm">{$msgs[op].Text}</td>

<td class="norm">{if $msgs[op].Image neq ''}<a target="_blank" href="../uploads/{$msgs[op].Image}"><img src="../uploads/{$msgs[op].Image}" width='60px' hieght='60px'/></a>{/if}</td>

<td class="norm">{if $msgs[op].Audio neq ''}<a href="download.php?download={$msgs[op].Audio }">تحميل</a>{/if}</td>

<td class="norm">{if $msgs[op].Video neq ''}<a href="download.php?download={$msgs[op].Video}">تحميل</a>{/if}</td>

<td class="norm"><a href="">{$msgs[op].Dept_Title}</a></td>

<td class="norm">

<a href="msg_templates.php?action=edit&id={$msgs[op].ID}"><img src="style/images/pencil.png" title="تعديل" alt="تعديل" /></a>

<a style="cursor:pointer" onclick="DeleteVer('msg_templates.php?action=delete&id={$msgs[op].ID}')">

<img src="style/images/cross.png" title="حذف" alt="حذف" /></a>

</td>

</tr>

{/section}

</tbody>

{else}

<tr><td style="text-align:center" colspan="9" class="norm">لا يوجد نتائج لعرض</td></tr>

{/if}



<tr><td class="retb_tab" colspan="9">{include file='paging_class.tpl'}
<div style="float:right">

<select name="doaction">

    <option>اختر عملية للتنفيذ</option>

    <option value="delete">حذف</option>

</select>

<input type="submit" class="button" value="تنفيذ على المختار" /></div>

</td></tr>



</table>

</form>