{literal}

<script type="text/javascript">

$(document).ready(function() {

    $("#predtable").tablesorter({headers: { 0: { sorter: false}, 1: {sorter: false}, 6: {sorter: false}, 7: {sorter: false}, 8: {sorter: false}, 9: {sorter: false} }}); 

});

</script>

{/literal}



{include file='page_navbar.tpl'}



<form method="post" action="page.php?action=multich">

<table id="predtable">

<thead>

<tr>

<th class="th_rad_right" style="text-align:center"><input type="checkbox" onclick="checkDelBoxes(this.form, 'delaction[]', this.checked)" /></th>

<th>ID</th>

<th>عنوان الصفحه</th>

<th>عنوان SEO</th>

{if !isset($subpage)}<th>صفحات فرعية

</th>

{/if}

<th style="width:8%; text-align:center">زيارات</th>
{if ($sub_permession[7] neq '' && in_array(4,$sub_permession[7])) || $Group_ID_Session eq 0}
<th style="width:8%; text-align:center">الحاله</th>
{/if}
<!--th style="width:12%; text-align:center">قائمة جانبية</th><-->
{if ($sub_permession[7] neq ''  && ( in_array(2,$sub_permession[7])  ||  in_array(3,$sub_permession[7]))) || $Group_ID_Session eq 0 }
<th class="th_rad_left">عمليات</th>
{/if}
</tr>

</thead>



{if isset($pages)}

<tbody>

{section name=op loop=$pages}

<tr>

<td style="width:5%; text-align:center" class="norm"><input name="delaction[]" type="checkbox" value="{$pages[op].ID}" /></td>



<td style="width:5%; text-align:center" class="altpoll">{$pages[op].ID}</td>



<td class="norm">

{if $pages[op].Secid eq -1}

<a href="page.php?id={$pages[op].ID}">{$pages[op].Title}</a>

{else}{$pages[op].Title}{/if}

</td>

<td class="norm">{$pages[op].Seotitle}</td>



{if !isset($subpage)}

<td style="width:10%; text-align:center" class="altpoll"><div class="post-com-count-wrapper">

<a href="page.php?id={$pages[op].ID}" class="post-com-count">

<span class="comment-count">{$pages[op].subcount}</span>

</a></div></td>

{/if}



<td style="width:10%; text-align:center" class="altpoll"><div class="post-com-count-wrapper">

<a href="#" class="post-com-count">

<span class="comment-count">{$pages[op].Counter}</span>

</a></div></td>


{if ($sub_permession[7] neq '' && in_array(4,$sub_permession[7])) || $Group_ID_Session eq 0}
<td class="norm" style="text-align:center">

{if $pages[op].Status eq 1}

<div id="status_e_{$pages[op].ID}"><img style="cursor:pointer" onclick="ChangeStat('page',{$pages[op].ID},'status',0)" src="style/images/enable.gif" title="تعطيل" alt="تعطيل" /></div>

<div id="status_d_{$pages[op].ID}" style="display:none;cursor:pointer"><img onclick="ChangeStat('page',{$pages[op].ID},'status',1)" src="style/images/disable.gif" title="تفعيل" alt="تفعيل" /></div>

{else}

<div id="status_d_{$pages[op].ID}"><img style="cursor:pointer" onclick="ChangeStat('page',{$pages[op].ID},'status',1)" src="style/images/disable.gif" title="تفعيل" alt="تفعيل" /></div>

<div id="status_e_{$pages[op].ID}" style="display:none;cursor:pointer"><img onclick="ChangeStat('page',{$pages[op].ID},'status',0)" src="style/images/enable.gif" title="تعطيل" alt="تعطيل" /></div>

{/if}

</td>
{/if}


<!--td class="altpoll" style="text-align:center">

{if $pages[op].Sidebar eq 1}

<div id="sidebar_e_{$pages[op].ID}"><img style="cursor:pointer" onclick="ChangeStat('page',{$pages[op].ID},'sidebar',0)" src="style/images/enable.gif" title="تعطيل" alt="تعطيل" /></div>

<div id="sidebar_d_{$pages[op].ID}" style="display:none;cursor:pointer"><img onclick="ChangeStat('page',{$pages[op].ID},'sidebar',1)" src="style/images/disable.gif" title="تفعيل" alt="تفعيل" /></div>

{else}

<div id="sidebar_d_{$pages[op].ID}"><img style="cursor:pointer" onclick="ChangeStat('page',{$pages[op].ID},'sidebar',1)" src="style/images/disable.gif" title="تفعيل" alt="تفعيل" /></div>

<div id="sidebar_e_{$pages[op].ID}" style="display:none;cursor:pointer"><img onclick="ChangeStat('page',{$pages[op].ID},'sidebar',0)" src="style/images/enable.gif" title="تعطيل" alt="تعطيل" /></div>

{/if}

</td-->

{if ($sub_permession[7] neq ''  && ( in_array(2,$sub_permession[7])  ||  in_array(3,$sub_permession[7]))) || $Group_ID_Session eq 0 }

<td class="norm">
{if ($sub_permession[7] neq '' && in_array(2,$sub_permession[7])) || $Group_ID_Session eq 0}

<a href="page.php?action=edit&id={$pages[op].ID}"><img src="style/images/pencil.png" title="تعديل" alt="تعديل" /></a>
{/if}
{if ($sub_permession[7] neq '' && in_array(3,$sub_permession[7])) || $Group_ID_Session eq 0}
<a style="cursor:pointer" onclick="DeleteVer('page.php?action=delete&id={$pages[op].ID}')"><img src="style/images/cross.png" title="حذف" alt="حذف" /></a>
{/if}

</td>
{/if}
</tr>

{/section}

</tbody>

{else}

<tr><td style="text-align:center" colspan="10" class="norm">لا يوجد صفحات تم اضافتها من قبل</td></tr>

{/if}



<tr><td class="retb_tab" colspan="10">{include file='paging_class.tpl'}
{if ($sub_permession[7] neq '' && in_array(3,$sub_permession[7])) || $Group_ID_Session eq 0}
<div style="float:right">

<select name="doaction">

    <option value="order">اعادة ترتيب العناصر</option>

    <option value="enable">تفعيل</option>

    <option value="disable">تعطيل</option>

    <!--option value="show">اظهار بالقائمة</option>

    <option value="hide">اخفاء من القائمة

    </option-->

    <option value="delete">حذف</option>

</select>

<input type="submit" class="button" value="تنفيذ على المختار" /></div>
{/if}
</td></tr>



</table>

</form>