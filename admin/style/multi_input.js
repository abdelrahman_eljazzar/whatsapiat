﻿var arrInput = new Array(0);
var arrInputValue = new Array(0);

function addInput(type,name) {
    arrInput.push(arrInput.length);
    arrInputValue.push("");
    display(type,name);
}

function display(type,name) {
    document.getElementById('parah').innerHTML = "";
    for (var intI = 0; intI < arrInput.length; intI++) {
        if (type == 'text') {
            document.getElementById('parah').innerHTML += createInput(arrInput[intI], arrInputValue[intI],name);
        } else if (type == 'upload') {
            document.getElementById('parah').innerHTML += createFile(arrInput[intI], arrInputValue[intI],name);
        }
    }
}

function saveValue(intId, strValue) {
    arrInputValue[intId] = strValue;
}

function createInput(id, value,name) {
    return "<input size='60' name='"+ name +"[]' type='text' id='inputplus " + id + "' onchange='javascript:saveValue(" + id + ",this.value)' value='" + value + "'><br />";
}

function createFile(id, value,name) {
    return "<input size='40' name='"+ name +"[]' type='file' id='inputplus " + id + "' onchange='javascript:saveValue(" + id + ",this.value)' value='" + value + "'><br />";
}

function deleteInput(type) {
    if (arrInput.length > 0) {
        arrInput.pop();
        arrInputValue.pop();
    }
    display(type,'text');
}