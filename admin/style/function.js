﻿//==========Ajax on/off============//


function ajaxon(type) {
    if (type == 1){
        document.getElementById('ajax_editor').innerHTML = "<img src='style/images/ajax-loader.gif' alt='جاري اضافة المحرر...' />";
        $.get('email.php', {action: 'ajaxon'},
        function (result){
            document.getElementById('ajax_editor').innerHTML = result;
        });
    }
    else if(type == 0){
        document.getElementById('ajax_editor').innerHTML = '<textarea style="width:90%" name="message" cols="30" rows="20"></textarea>';
    }
}

function PageEditor(type, id) {
  document.getElementById('ajax_editor').innerHTML = '';
  document.getElementById('Eprocess').style.display = '';
  $.get('page.php', {action: 'editor', type: type, id: id},
  function (result){
      document.getElementById('Eprocess').style.display = 'none';
      document.getElementById('ajax_editor').innerHTML = result;
  });
}
function PageOption(value) {
  if(value == -1){
    document.getElementById('ContentHolder').style.display = 'none';
  }else{
    document.getElementById('ContentHolder').style.display = '';
  }
}

//================Ajax admin Notes=============//
function ajax_admin_notes() {
    var notes = document.getElementById('notes');
    document.getElementById('ajax_show_admnot').style.display = '';

    $.get('home.php?action=ajax&do=notes', {
        value: notes.value
    },
    function (result) {
        document.getElementById('ajax_show_admnot').style.display = 'none';
    });
}


//=============Check All Code==========//
function checkDelBoxes(pForm, boxName, parent) {
    for (i = 0; i < pForm.elements.length; i++)
    if (pForm.elements[i].name == boxName) pForm.elements[i].checked = parent;
}
function changeselect(index) {
   var select = document.getElementsByTagName('select');
   var count = select.length;
   for(i=0;i<count;i++) {
		select[i].selectedIndex = index;
   }
}

//================Redirect Url=============//
function RedirectUrl(url, value) {
    window.location = url + value;
}
function RedirectFrame(value){
  var selfurl = location.href;
  //selfurl = selfurl.replace(/username/g, visitorName);
  if (selfurl.match(/\?/g)){
    var newhref = selfurl+'&clang='+value;
  }else{
    var newhref = selfurl+'?&clang='+value;
  }
  window.location = newhref;
}

//================Change Status=============//
function ChangeStat(file, id, action, status) {
    $.get(file + '.php?action=change', {
        id: id,
        doit: action,
        status: status
    },
    function (result) {
        if (status == 0) {
            document.getElementById(action + "_e_" + id).style.display = 'none';
            document.getElementById(action + "_d_" + id).style.display = '';
        } else {
			document.getElementById(action + "_d_" + id).style.display = 'none';
            document.getElementById(action + "_e_" + id).style.display = '';
        }
    });
}



//=====Show / Hide Function====//
function ShowHide(element) {
    if (document.getElementById(element).style.display == 'none') {
        var options = {};
        $("#" + element).show('blind', options, 500);
    } else {
        var options = {};
        $("#" + element).hide('blind', options, 500);
    }
}
function ShowHide_Test(element,element_hide_1,element_hide_2,element_hide_3,element_hide_4) {
   var options = {};

 if (document.getElementById(element).style.display == 'none') {
        var options = {};
        $("#" + element).show('blind', options, 500);
        $("#" + element_hide_1).hide();
        $("#" + element_hide_2).hide();
        $("#" + element_hide_3).hide();
        $("#" + element_hide_4).hide();
    } else {
        var options = {};
        $("#" + element).hide('blind', options, 500);
    }
}
//=====Ads Alter====//
function AdsChange(element) {
  document.getElementById('imagetype').style.display = 'none';
  document.getElementById('codetype').style.display = 'none';
  
  if(element == 'image' || element == 'flash'){
    $("#imagetype").show('blind', [], 500);
  }
  else if(element == 'adsense' || element == 'text'){
    $("#codetype").show('blind', [], 500);
  }
}

function PluginChange(element) {
  document.getElementById('sourcecode').style.display = 'none';
  document.getElementById('sourcefile').style.display = 'none';
  
  if(element == 'file'){
    $("#sourcefile").show('blind', [], 500);
  }
  else{
    $("#sourcecode").show('blind', [], 500);
  }
}

//======Delete Vefication=========//
function DeleteVer(url) {
if (confirm("هل انت متاكد من اكمال تنفيذ العمليه..لا يمكن التراجع بها؟")) {
window.location = url;
}}

//=====SMS message function system====//
function CheckBalance() {
  document.getElementById('SMSBal_do').style.display = 'none';
  document.getElementById('SMSBal').style.display = '';
  $.get('setting.php?action=checkbal', {},
  function (result) {
      if (result == 0) { alert('خطأ'); }
      else {
          document.getElementById('SMSBal').innerHTML = result;
      }
  });
}

function strippingEnters(message) {
    // use regexp to remove 'return' char on windows platform
    if (message.indexOf("\r\n") == -1) {
        var strSingleLineText = message.replace(new RegExp("\n", "g"), "\n\r");
    } else {
        var strSingleLineText = message.replace(new RegExp("\n", "g"), "\n");
    }
    return strSingleLineText;
}

function msgCounter(message, charCount) {
    var maxMsgLen = 0;
    var maxMsgLen = 0;
    var msgTxt = strippingEnters(message.value);
    charCount.value = msgTxt.length;
    msgLimit(message);
    if (!unicode(msgTxt)) {
        if ((x = (charCount.value) / 160) <= 1) {
            charCount.value = charCount.value + "  = " + "1 رسالة";
        } else {
            x = (charCount.value) / 153;
            var remender = (charCount.value) % 153;
            if (parseInt(x) > 0) {
                if (remender > 0) {
                    x = x + 1;
                }
                charCount.value = charCount.value + "  = " + parseInt(x) + "رسالة";
            }
        }
    } else if ((x = (charCount.value) / 70) <= 1) {
        charCount.value = charCount.value + "  = " + " 1 رسالة";
    } else {
        x = (charCount.value) / 67;
        var remender = (charCount.value) % 67;
        if (parseInt(x) > 0) {
            if (remender > 0) {
                x = x + 1;
            }
            charCount.value = charCount.value + "  = " + parseInt(x) + "رسالة";
        }
    }
}

function unicode(strText) {
    str = "دجحخهعغفقثصضطكمنتالبيسشظزوةىلارؤءئإلإألأآلآ";
    for (i = 0; i < strText.length; i++)
    if (str.indexOf(strText.charAt(i)) != -1) return 1;
    return 0;
}

function setTemplate(Mess, Temp) {
    sellength = Temp.length;
    for (i = 0; i < sellength; i++) {
        if (Temp.options[i].selected == true) {
            Mess.value = Temp.options[i].value;
        }
    }
}

function getElementValue(formElement) {
    if (formElement.length != null) var type = formElement[0].type;
    if ((typeof(type) == 'undefined') || (type == 0)) var type = formElement.type;
    switch (type) {
    case 'undefined':
        return;
    case 'radio':
        for (var x = 0; x < formElement.length; x++)
        if (formElement[x].checked == true) return formElement[x].value;
    case 'select-multiple':
        var myArray = new Array();
        for (var x = 0; x < formElement.length; x++)
        if (formElement[x].selected == true) myArray[myArray.length] = formElement[x].value;
        return myArray;
    case 'checkbox':
        return formElement.checked;
    default:
        return formElement.value;
    }
}

function limitText(field) {
    if (field.value.match(/^\d+$/)) {
        limitNum = 16;
    } else {
        limitNum = 11;
    }
    if (field.value.length > limitNum) {
        field.value = field.value.substring(0, limitNum);
    }
}

function countEnters(field) {
    var count = 0;
    var msg = field;
    for (var i in msg) {
        if (msg[i] == '\n') {
            count++;
        }
    }
    return count;
}

function msgLimit(field) {
    var count = countEnters(field.value)
    if (!unicode(field.value)) {
        limitNum = 918;
    } else {
        limitNum = 402;
    }
    if (count != '0') {
        limitNum = limitNum - count;
    }
    if (field.value.length > limitNum) {
        field.value = field.value.substring(0, limitNum);
    }
}

/*============Plugin ajax system=========*/
function DelPlugin(id){
  if(! confirm('هل ترغب في استكمال عملية الحذف؟') ){
    return false;
  }
  document.getElementById('PlugProgress').style.display = '';
  $.get('plugin.php?action=ajax&do=delete', {id: id},
  function (result) {
      if (result == 0) { alert('خطأ'); }
      else {
        $('#sortelm_'+id).fadeOut('300', function() {
            $('#sortelm_'+id).remove();
        });
      }
  });
  document.getElementById('PlugProgress').style.display = 'none';
}

function ActivatPlug(id, status){
  document.getElementById('PlugProgress').style.display = '';
  $.get('plugin.php?action=ajax&do=activate', {id: id, status: status},
  function (result) {
      if (result == 0) { alert('خطأ'); }
      else {
        if(status == 1){
          $('#PlugActive_'+id).hide();
          $('#PluginActive_'+id).fadeIn('slow');
        }
        else{
          $('#PluginActive_'+id).hide();
          $('#PlugActive_'+id).fadeIn('slow');
        }
      }
  });
  document.getElementById('PlugProgress').style.display = 'none';
}

function SavejSort(){
  document.getElementById('jSort_done').style.display = 'none';
  document.getElementById('jSort_work').style.display = '';
  var jorderRight = $('#jsort_rcolumn').sortable('toArray');
  var jorderMed = $('#jsort_mcolumn').sortable('toArray');
  var jorderLeft = $('#jsort_lcolumn').sortable('toArray');

  $.get('plugin.php?action=ajax&do=saveorder', {
    'jorderRight[]': jorderRight,
    'jorderMed[]': jorderMed,
    'jorderLeft[]': jorderLeft
  },
  function (result) {
      if (result == 0) { alert('خطأ'); }
      else {}
  });
  document.getElementById('jSort_work').style.display = 'none';
  document.getElementById('jSort_done').style.display = '';
}