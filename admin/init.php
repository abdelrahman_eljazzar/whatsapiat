<?php
session_start();

if($_SESSION['adminper'] != 1) {
  echo "<script>window.location='login.php?do=relogin'</script>";
  exit;
}

require ('includes/Smarty/Smarty.class.php');

$Smarty = new Smarty();
$Smarty->setTemplateDir('style/templates');
$Smarty->setCompileDir('style/templates/Cache');

require_once ("includes/config.php");
require_once ("includes/function.php");
require_once ("includes/db_class.php");

$Smarty->assign("homeurl", $homeurl);
$Smarty->assign("site_home", $site_home);
$Smarty->assign("inboxcount", CountSql("artgine_contact WHERE Status='0'"));
$Smarty->assign("admin_se_user", $_SESSION['adminuser']);
$Smarty->assign("admin_se_id", $_SESSION['adminid']);



/*elseif(!isset ($_SESSION['langdir'])) {
$sellang = Sqlread("SELECT ID,Direction FROM language WHERE `Default`='1'");
if(empty ($sellang)) {
$_SESSION['langid'] = 1;
$_SESSION['langdir'] = 'rtl';
}
else{
$_SESSION['langid'] = $sellang['ID'];
$_SESSION['langdir'] = $sellang['Direction'];
}
}
*/

?>