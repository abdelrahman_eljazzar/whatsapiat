<?php
    $name_of_file = $_GET["download"];

    header('Content-Description: File Transfer');
    // We'll be outputting a MP3
    header('Content-type: application/mp3');
    // It will be called file.mp3
    header('Content-Disposition: attachment; filename=' .$name_of_file);
    header('Content-Length: '.filesize($name_of_file));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    // The MP3 source is in somefile.pdf
    //readfile("somefile.mp3");

    readfile_chunked($name_of_file);

    function readfile_chunked($filename) {
        $chunksize = 1*(1024*1024); // how many bytes per chunk
        $buffer = '';
        $handle = fopen($filename, 'rb');

        if ($handle === false) {
            return false;
        }

        while (!feof($handle)) {
            $buffer = fread($handle, $chunksize);
            print $buffer;
        }

        return fclose($handle);
    }
?>