<?php

require_once('cron.class.php');

$cron = new CronParser();

$crontasks = mysql_query("SELECT * FROM cron WHERE active <> 0");

while ($crontask = mysql_fetch_array($crontasks)) {

    /*if ($crontask['ok'] == false) {
    	//notify admin if cron jop error
        send_alert_email("A cron task was failed", "'$crontask[task]' crashed when we tried to run it.");
    }*/

    if ($cron->calcLastRan($crontask['mhdmd'])) {
        $lastRan = $cron->getLastRan();
        $my_logs = '';


        if ($cron->getLastRanUnix() > $crontask['ran_at']) {
            //print('<br />' . "'$crontask[task]' \r\ndue to be run at: $lastRan[5]-$lastRan[3]-$lastRan[2] $lastRan[1]:$lastRan[0]\r\nlast ran at: " . date('Y-m-d H:i:s', $crontask['ran_at']) . "\r\nTime now is: " . date('Y-m-d H:i:s'));
            //print('<br />' . "Begin processing '$crontask[task]'");

            mysql_query("
				UPDATE cron
				SET ran_at = " . time() . ", ok = false
				WHERE cron_id = $crontask[cron_id]
			");

            run_cron_task($crontask['file']);

            if ($crontask['log_level'] AND $my_logs) {
                log_debug($my_logs);

                mysql_query("
					INSERT INTO $settings[table_cron_log]
					SET log = '" . addslashes($my_logs) . "', cron_id = $crontask[cron_id], log_time = " . time());
            }

            mysql_query("UPDATE cron SET ok = true WHERE cron_id = $crontask[cron_id]");
        } else {
            //print('<br />' . "'$crontask[task]' is not due.\r\nLast due at: $lastRan[5]-$lastRan[3]-$lastRan[2] $lastRan[1]:$lastRan[0]\r\nlast ran at: " . date('Y-m-d H:i:s', $crontask['ran_at']) . "\r\nTime now is: " . date('Y-m-d H:i:s'));
        }

    } else {
        //print('<br />' . "Unable to calculate LastRan for cron id: $crontask[cron_id]");
    }
}

function run_cron_task($file)
{
    global $my_logs;
    include('cron/'.$file);
}

?>