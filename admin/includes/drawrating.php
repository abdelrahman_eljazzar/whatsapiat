<?php
function rating_bar($rating_tableName, $id, $units = '', $static = '')
{
    require('config.php'); // get the db connection info

    //set some variables
    $ip = $_SERVER['REMOTE_ADDR'];
    if (!$units) {
        $units = 10;
    }
    if (!$static) {
        $static = FALSE;
    }

    // get votes, values, ips for the current rating bar
    $query = mysql_query("SELECT total_votes, total_value, used_ips FROM $rating_tableName WHERE newsid='$id' ");

    $numbers = mysql_fetch_assoc($query);


    if ($numbers['total_votes'] < 1) {
        $count = 0;
    } else {
        $count = $numbers['total_votes']; //how many votes total
    }
    $current_rating = $numbers['total_value']; //total number of rating added together and stored
    $tense          = ($count == 1) ? "vote" : "votes"; //plural form votes/vote

    // determine whether the user has voted, so we know how to draw the ul/li
    $voted = mysql_num_rows(mysql_query("SELECT used_ips FROM $rating_tableName WHERE used_ips LIKE '%" . $ip . "%' AND newsid='" . $id . "' "));

    // now draw the rating bar
    $rating_width = @number_format($current_rating / $count, 2) * $rating_unitwidth;
    $rating1      = @number_format($current_rating / $count, 1);
    $rating2      = @number_format($current_rating / $count, 2);

    $rater = '';
    $rater .= '<div class="ratingblock">';

    $rater .= '<div id="unit_long' . $id . '">';
    $rater .= '  <ul id="unit_ul' . $id . '" class="unit-rating" style="width:' . $rating_unitwidth * $units . 'px;">';
    $rater .= '     <li class="current-rating" style="width:' . $rating_width . 'px;"></li>';

    for ($ncount = 1; $ncount <= $units; $ncount++) { // loop from 1 to the number of units
        if (!$voted) { // if the user hasn't yet voted, draw the voting stars
            $rater .= '<li><a href="' . $homeurl . '/includes/db.php?j=' . $ncount . '&amp;q=' . $id . '&amp;t=' . $ip . '&amp;c=' . $units . '&amp;rt=' . $rating_tableName . '" title="' . $ncount . ' out of ' . $units . '" class="r' . $ncount . '-unit rater" rel="nofollow"></a></li>';
        }
    }
    $ncount = 0; // resets the count

    $rater .= '  </ul>';
    $rater .= '  <p';
    if ($voted) {
        $rater .= ' class="voted"';
    }
    $rater .= '><strong> ' . $rating1 . '</strong>/' . $units . '';
    $rater .= '  </p>';
    $rater .= '</div>';
    $rater .= '</div>';
    return $rater;
}
?>