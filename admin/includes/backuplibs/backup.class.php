<?php

class backup extends db
{
    var $backupdir = "backup";
    var $compression = "bz2";
    var $color1 = "#F57E1D";
    var $color2 = "#F5F6C4";
    var $color3 = "#FFFF40";
    var $mysqldump = true;
    var $default_sort_order = "DateDesc";
    var $file_mod = "0777";
    function backup()
    {
        global $GonxAdmin;
        $this->compression = $GonxAdmin["compression_default"];
    }
    function tables_menu($signature)
    {
        global $GONX;
        $color3  = $this->color3;
        $color2  = $this->color2;
        $color1  = $this->color1;
        $res     = "\n<form action='?go=backuptables' method='post'>
<table id='predtable'>
<tr>
\t<th class='th_rad_right' align=center><input type='checkbox' onclick=\"checkDelBoxes(this.form, 'tables[]', this.checked)\" /></th>
\t<th>Table</th>
\t<th>Rows</th>
\t<th>Create_time</th>
\t<th>Update_time</th>
\t<th class='th_rad_left'>Check_time</th>
</tr>\n\n";
        $result  = $this->query('SHOW TABLE STATUS');
        $i       = 0;
        $bgcolor = 'altpoll';
        while ($table = @$this->fetch_array($result)) {
            if ($table["Update_time"] != $table["Create_time"]) {
                $l1 = "<label for=\"tables$i\">";
                $l2 = "</label>";
            } else {
                $l1 = $l2 = "";
            }
            ;
            $res .= "<tr class=\"$bgcolor\">
\t<td align=center><input type='checkbox' id='tables$i' name='tables[]' value='" . $table["Name"] . "' /></td>
\t<td align=right>" . $table["Name"] . "</td>
\t<td>" . $table["Rows"] . "</td>
\t<td>$l1" . $table["Create_time"] . "$l2</td>
\t<td>$l1" . $table["Update_time"] . "$l2</td>
\t<td>" . $table["Check_time"] . "</td>
</tr>
";
            if ($bgcolor == 'altpoll') {
                $bgcolor = 'norm';
            } else
                $bgcolor = 'altpoll';
            $i++;
        }
        $res .= "<tr><td class='retb_tab' colspan='6'>
<div style='float:right'>
<input type='checkbox' name='structonly' value='Yes'> " . $GONX["structonly"] . "<br><br>
<p align=right><input type='submit' class='button' value='البدا في عملية النسخ'></p></div>
<div style='float:left'>".$signature."</div>
</td></tr>

</table></form>\n";
        return $res;
    }
    function tables_backup($tables, $structonly)
    {
        global $GONX;
        foreach ($tables as $v) {
            $res = $this->query("SHOW CREATE TABLE " . $this->dbName . "." . $v);
            while ($resu[] = $this->get_data()) {
            }
        }
        foreach ($resu as $key => $val) {
            $tbl_name_status = $this->valid_table_name($val[0]);
            if (trim($val[0]) !== "" and $tbl_name_status) {
                $Dx_Create_Tables .= "
# Drop table '" . $val[0] . "' if exist
<xquery>
DROP TABLE IF EXISTS " . $val[0] . ";
</xquery>
# create table '" . $val[0] . "'
<xquery>
" . $val[1] . " ;
</xquery>\r\n";
                if ($structonly != "Yes") {
                    $query = "Insert into `$val[0]` (";
                    $this->query("LOCK TABLES $val[0] WRITE");
                    $qresult = $this->query("Select * from $val[0]");
                    while ($line = $this->fetch_array($qresult)) {
                        unset($fields, $values);
                        $j = 0;
                        while (list($col_name, $col_value) = each($line)) {
                            if (!is_int($col_name)) {
                                $fields .= "`$col_name`,";
                                $values .= "'" . $this->escape_string($col_value) . "',";
                            }
                        }
                        $fields  = substr($fields, 0, strlen($fields) - 1);
                        $values  = substr($values, 0, strlen($values) - 1);
                        $myquery = $query . $fields . ") values (" . $values . ");";
                        $Dx_Create_Tables .= "\r\n<xquery>
" . $myquery . "
</xquery>\r\n";
                    }
                    $this->query("UNLOCK TABLES;");
                }
            } elseif (!$tbl_name_status) {
                $err_msg .= "<font color=red>" . $GONX["ignoredtables"] . " " . $val[0] . " - " . $GONX["reservedwords"] . ".</font><br>\n";
            }
        }
        if (!is_dir($this->backupdir)) {
            @mkdir($this->backupdir, 0755);
        }
        if (sizeof($tables) == 1) {
            $prefix = "[" . $tables[0] . "]";
        } else
            $prefix = "[" . sizeof($tables) . "tables]";
        switch ($this->compression) {
            case "bz2":
                $fname = $this->dbName . "-$prefix-" . date("Y-m-d H-i-s") . ".bz2";
                touch($this->backupdir . "/" . $fname);
                $fp = bzopen($this->backupdir . "/" . $fname, "w");
                bzwrite($fp, $Dx_Create_Tables);
                bzclose($fp);
                break;
            case "zlib":
                $fname = $this->dbName . "-$prefix-" . date("Y-m-d H-i-s") . ".gz";
                touch($this->backupdir . "/" . $fname);
                $fp = gzopen($this->backupdir . "/" . $fname, "w");
                gzwrite($fp, $Dx_Create_Tables);
                gzclose($fp);
                break;
            default:
                $fname = $this->dbName . "-$prefix-" . date("Y-m-d H-i-s") . ".sql";
                touch($this->backupdir . "/" . $fname);
                $fp = fopen($this->backupdir . "/" . $fname, "w");
                fwrite($fp, $Dx_Create_Tables);
                fclose($fp);
                break;
        }
        return "$err_msg<font color=green>" . $GONX["backup"] . " " . $this->dbName . " " . $GONX["iscorrectcreat"] . " : " . $this->backupdir . "/$fname</font>";
    }
    function generate()
    {
        global $GONX;
        $result = @$this->list_tables($this->dbName);
        while ($table = @$this->fetch_row($result)) {
            $res = $this->query("SHOW CREATE TABLE " . $this->dbName . "." . $table[0]);
            while ($resu[] = $this->get_data()) {
            }
        }
        foreach ($resu as $key => $val) {
            $tbl_name_status = $this->valid_table_name($val[0]);
            if (trim($val[0]) !== "" and $tbl_name_status) {
                $Dx_Create_Tables .= "
# Drop table '" . $val[0] . "' if exist
<xquery>
DROP TABLE IF EXISTS " . $val[0] . ";
</xquery>
# create table '" . $val[0] . "'
<xquery>
" . $val[1] . " ;
</xquery>\r\n";
                $query = "Insert into `$val[0]` (";
                $this->query("LOCK TABLES $val[0] WRITE");
                $qresult = $this->query("Select * from $val[0]");
                while ($line = $this->fetch_array($qresult)) {
                    unset($fields, $values);
                    $j = 0;
                    while (list($col_name, $col_value) = each($line)) {
                        if (!is_int($col_name)) {
                            $fields .= "`$col_name`,";
                            $values .= "'" . $this->escape_string($col_value) . "',";
                        }
                    }
                    $fields  = substr($fields, 0, strlen($fields) - 1);
                    $values  = substr($values, 0, strlen($values) - 1);
                    $myquery = $query . $fields . ") values (" . $values . ");";
                    $Dx_Create_Tables .= "\r\n<xquery>
" . $myquery . "
</xquery>\r\n";
                }
                $this->query("UNLOCK TABLES;");
            } elseif (!$tbl_name_status) {
                $err_msg .= "<font color=red>Ignored table " . $val[0] . " - Reserved SQL word.</font><br>\n";
            }
        }
        if (!is_dir($this->backupdir)) {
            @mkdir($this->backupdir, octdec($this->file_mod));
        }
        switch ($this->compression) {
            case "bz2":
                $fname = $this->dbName . "-" . date("Y-m-d H-i-s") . ".bz2";
                touch($this->backupdir . "/" . $fname);
                $fp = bzopen($this->backupdir . "/" . $fname, "w");
                bzwrite($fp, $Dx_Create_Tables);
                bzclose($fp);
                break;
            case "zlib":
                $fname = $this->dbName . "-" . date("Y-m-d H-i-s") . ".gz";
                touch($this->backupdir . "/" . $fname);
                $fp = gzopen($this->backupdir . "/" . $fname, "w");
                gzwrite($fp, $Dx_Create_Tables);
                gzclose($fp);
                break;
            default:
                $fname = $this->dbName . "-" . date("Y-m-d H-i-s") . ".sql";
                touch($this->backupdir . "/" . $fname);
                $fp = fopen($this->backupdir . "/" . $fname, "w");
                fwrite($fp, $Dx_Create_Tables);
                fclose($fp);
                break;
        }
        return "$err_msg<font color=green>" . $GONX["backup"] . " " . $this->dbName . " " . $GONX["iscorrectcreat"] . " : " . $this->backupdir . "/$fname</font>";
    }
    function import($bfile = "")
    {
        global $GONX, $GonxAdmin;
        set_time_limit(0);
        if (isset($_GET["importdump"])) {
            if (is_file($this->backupdir . "/" . $bfile)) {
                switch ($GonxAdmin["compression_default"]) {
                    case "bz2":
                        $bz = bzopen($this->backupdir . "/" . $bfile, "r");
                        while (!feof($bz)) {
                            $contents .= bzread($bz, 4096);
                        }
                        bzclose($bz);
                        break;
                    case "zlib":
                        $bz       = gzopen($this->backupdir . "/" . $bfile, "r");
                        $contents = gzread($bz, filesize($this->backupdir . "/" . $bfile) * 1000);
                        gzclose($bz);
                        break;
                    default:
                        $bz       = fopen($this->backupdir . "/" . $bfile, "r");
                        $contents = fread($bz, filesize($this->backupdir . "/" . $bfile) * 1000);
                        fclose($bz);
                        break;
                }
                $contents = str_replace("<xquery>", "", $contents);
                $contents = str_replace("</xquery>", "", $contents);
                touch($this->backupdir . "/temp.sql");
                $fp = fopen($this->backupdir . "/temp.sql", "w");
                fwrite($fp, $contents);
                fclose($fp);
                unset($contents);
                @shell_exec($GonxAdmin["mysqldump"] . " --host " . $GonxAdmin["dbhost"] . " --user=" . $GonxAdmin["dbuser"] . " --pass=" . $GonxAdmin["dbpass"] . " --databases " . $GonxAdmin["dbname"] . " < " . $this->backupdir . "/temp.sql");
                @unlink($this->backupdir . "/temp.sql");
                return "<font color=green> $bfile " . $GONX["iscorrectimport"] . " </font>";
            } else
                return FALSE;
        }
        if (is_file($this->backupdir . "/" . $bfile)) {
            switch ($GonxAdmin["compression_default"]) {
                case "bz2":
                    $bz = bzopen($this->backupdir . "/" . $bfile, "r");
                    while (!feof($bz)) {
                        $contents .= bzread($bz, 4096);
                    }
                    bzclose($bz);
                    break;
                case "zlib":
                    $bz       = gzopen($this->backupdir . "/" . $bfile, "r");
                    $contents = gzread($bz, filesize($this->backupdir . "/" . $bfile) * 1000);
                    gzclose($bz);
                    break;
                default:
                    $bz       = fopen($this->backupdir . "/" . $bfile, "r");
                    $contents = fread($bz, filesize($this->backupdir . "/" . $bfile) * 1000);
                    fclose($bz);
                    break;
            }
            preg_match_all("'<xquery[?>]*?>(.*?)</xquery>'si", $contents, $requetes);
            unset($contents);
            foreach ($requetes[1] as $key => $val) {
                $this->query(trim($val));
            }
            return "<font color=green> $bfile " . $GONX["iscorrectimport"] . " </font>";
        } else {
            return false;
        }
    }
    function importfromfile()
    {
        global $GONX, $HTTP_POST_FILES;
        @set_time_limit(0);
        $bfile       = $HTTP_POST_FILES["backupfile"];
        $pathinfo    = pathinfo($bfile["name"]);
        $compression = $pathinfo["extension"];
        if ($bfile["error"] == 0) {
            switch ($compression) {
                case "bz2":
                    $bz       = bzopen($bfile["tmp_name"], "r");
                    $contents = bzread($bz, $bfile["size"]);
                    bzclose($bz);
                    break;
                case "gz":
                    $gz       = gzopen($bfile["tmp_name"], "r");
                    $contents = gzread($gz, $bfile["size"]);
                    bzclose($gz);
                    break;
                default:
                    $f        = fopen($bfile["tmp_name"], "r");
                    $contents = fread($f, $bfile["size"]);
                    fclose($f);
                    break;
            }
            preg_match_all("'<xquery[?>]*?>(.*?)</xquery>'si", $contents, $requetes);
            foreach ($requetes[1] as $key => $val) {
                $this->query(trim($val));
            }
            return "<font color=green> " . $bfile["name"] . " " . $GONX["iscorrectimport"] . " </font>";
        } else {
            return $this->listbackups();
        }
    }
    function listbackups()
    {
        global $GONX, $GonxAdmin, $page, $orderby;
        $pagesize  = $GonxAdmin["pagedisplay"];
        $GonxOrder = array(
            "DateAsc",
            "NameAsc",
            "NameDesc",
            "SizeAsc",
            "SizeDesc",
            "DateDesc"
        );
        if ($orderby == "" or !in_array($orderby, $GonxOrder)) {
            $orderby = $this->default_sort_order;
        }
        if (!isset($page) or ($page <= 0)) {
            $page = 1;
            $from = $page - 1;
            $to   = ($pagesize * $page);
        } elseif ($page == 1) {
            $from = 0;
            $to   = ($pagesize * ($page + 1) - $pagesize);
        } else {
            $from = $pagesize * ($page - 1);
            $to   = ($pagesize * ($page + 1) - $page * $pagesize);
        }
        $res = "
<script language=\"JavaScript\" type=\"text/javascript\">
<!--
function IECColor1(el) {
	IEC_obj2.IECColor(el);
	IEC_obj1.IECColor(el);
}
function IECColor2(el){
	IEC_obj1.IECColor(el);
	IEC_obj2.IECColor(el);
	if(ConfirmDelete()){
		return true;
	} else {
		document.getElementById(el).style.background = IEC_obj1.BG2;
		document.forms[\"bform\"].reset();
		return false;
	}
}

function IECColorClass(BG1, BG2){
	this.gvar = 10000;
	this.BG1 = BG1;
	this.BG2 = BG2;
	this.IECColor=IECColor;
}

function IECColor(el) {
	document.getElementById(el).style.background = this.BG1;
	if(this.gvar < 9000 && this.gvar != el)
		document.getElementById(this.gvar).style.background = this.BG2;

	this.gvar = el;
}
IEC_obj1 = new IECColorClass('khaki','#F6F6F6');
IEC_obj2 = new IECColorClass('#CF2B5A','#F6F6F6');
//-->
</script>

<form method=get action=\"?\" name=bform>";
        if (!is_dir($this->backupdir)) {
            @mkdir($this->backupdir, octdec($this->file_mod));
        }
        $d = dir($this->backupdir);
        $i = $BackupSize = 0;
        while (false !== ($entry = $d->read())) {
            if ($entry != "." and $entry != ".." and (ereg(".bz2$", $entry) or ereg(".gz$", $entry) or ereg(".sql$", $entry))) {
                $mtime                    = date("F d Y H:i:s.", filemtime($this->backupdir . "/" . $entry));
                $time                     = filemtime($this->backupdir . "/" . $entry);
                $size                     = filesize($this->backupdir . "/" . $entry);
                $fsize                    = round($size / 1024);
                $GonxBackups[$i]["fname"] = $entry;
                $GonxBackups[$i]["mtime"] = $mtime;
                $GonxBackups[$i]["time"]  = $time;
                $GonxBackups[$i]["fsize"] = $fsize;
                $GonxBackups[$i]["size"]  = $size;
                $BackupSize += $fsize;
                $i++;
            }
        }
        if ($i == 0) {
            $res .= "<ul><li>" . $GONX["nobckupfile"] . "</li></ul>";
        } else {
            $allpages = round(sizeof($GonxBackups) / $pagesize);
            $all_rest = $allpages - $allpages * $pagesize;
            if ($all_rest > 0) {
                $allpages++;
            }
            if ($page < $allpages) {
                $next = "<a href=\"?option=databaseAdmin&go=list&amp;page=" . ($page + 1) . "&orderby=$orderby\" class='paging_num'>" . $GONX["next"] . "</a>";
            } else
                $next = "";
            if ($page > 1) {
                $prev = "<a href=\"?option=databaseAdmin&go=list&amp;page=" . ($page - 1) . "&orderby=$orderby\" class='paging_num'>" . $GONX["prev"] . "</a>";
            } else
                $prev = "";
            $links = "";
            for ($i = 0; $i < $allpages; $i++) {
                if (($i + 1) == $page) {
                    $links .= "<span class='paging_num_current'> " . ($i + 1) . " </span>";
                } else {
                    $links .= "<a href=\"?option=databaseAdmin&go=list&amp;page=" . ($i + 1) . "&orderby=$orderby\" class='paging_num'> " . ($i + 1) . " </a>";
                }
            }
            $OrderMenu = "<select OnChange=\"location.href='?option=databaseAdmin&go=list&page=$page&orderby='+ChgOrder.options[selectedIndex].value\" name=\"ChgOrder\">\n";
            foreach ($GonxOrder as $v) {
                if ($v == $orderby) {
                    $sel = " selected";
                } else
                    $sel = "";
                $OrderMenu .= "<option$sel value=\"$v\">$v</option>\n";
            }
            $OrderMenu .= "</select>\n";
            switch ($orderby) {
                case "DateAsc":
                    usort($GonxBackups, array(
                        "backup",
                        "DateSortAsc"
                    ));
                    break;
                case "NameAsc":
                    usort($GonxBackups, array(
                        "backup",
                        "NameSortAsc"
                    ));
                    break;
                case "NameDesc":
                    usort($GonxBackups, array(
                        "backup",
                        "NameSortDesc"
                    ));
                    break;
                case "SizeAsc":
                    usort($GonxBackups, array(
                        "backup",
                        "SizeSortAsc"
                    ));
                    break;
                case "SizeDesc":
                    usort($GonxBackups, array(
                        "backup",
                        "SizeSortDesc"
                    ));
                    break;
                default:
                    usort($GonxBackups, array(
                        "backup",
                        "DateSortDesc"
                    ));
                    break;
            }
            if (is_array($GonxBackups)) {
                $GonxBackups = array_slice($GonxBackups, $from, $to);
            }
            $res .= "
			<table id=\"predtable\">
\t<tr>
			<th class='th_rad_right'></th>
			<th>DB</th>
			<th>Download</th>
			<th>Size</th>
			<th>Date</th>
			<th class='th_rad_left'>Delete</th>
</tr>";
            $bgcolor = 'altpoll';
            foreach ($GonxBackups as $k => $v) {
                $db = explode("-", $v['fname']);
                $db = $db[0];
                $res .= "\t<tr class='$bgcolor' id=$k>
			<td><input type=\"radio\" name=\"bfile\" value=\"" . $v['fname'] . "\" onclick=\"IECColor1($k);\"></td>
			<td>$db</td>
			<td align=center><font size=2px> <a href=\"?option=databaseAdmin&go=getbackup&bfile=" . urlencode($v['fname']) . "\" target=\"_new\" title=\"Download " . $v['fname'] . "\">" . $v['fname'] . "</a></td>
			<td align=center><em>size " . $v['fsize'] . " Ko</em></td>
			<td align=center>" . $v['mtime'] . "</td>
			<td align=center><a href=\"?option=databaseAdmin&go=delete&fname=" . $v['fname'] . "\" title=\"" . $GONX["delete"] . " " . $v['fname'] . "\" onclick=\"return confirm('عملية حذف الملف لا يمكن التراجع فيها؟')\"><img src=\"style/images/cross.png\" border=0></a></font><td>
</tr>\n\n";
                if ($bgcolor == 'altpoll') {
                    $bgcolor = 'norm';
                } else
                    $bgcolor = 'altpoll';
            }
            $BackupSize = number_format(($BackupSize / 1024), 3);
            $res .= "<tr><td class=\"retb_tab\" colspan=7><div style='float:right'>
            <em>" . $GONX["totalbackupsize"] . " : $BackupSize Mo</em><br /><br /> - " . $GONX["chgdisplayorder"] . " : $OrderMenu
		<br/><input type=hidden name=go value=import>".$GONX['selectbackupfile']."<br /><br />";
            if ($this->mysqldump)
            $res .= "  <input type=submit name=import value=\"" . $GONX["importbackup"] . "\" class=button></form>";
            $res .= "<br /><br />$prev $links $next</div>";
            $res .= "<div style='float:left'><form method=post action=\"?\" enctype=\"multipart/form-data\"><b>" . $GONX["importbackupfile"] . " :</b><br/><br/>
		<input type=file name=backupfile>
		<br/><br/><input type=hidden name=go value=importfromfile>
		<p align=right><input type=submit name=import value=\"" . $GONX["importbackup"] . "\" class=button></p></form></div></td></tr></table>";
        }
        $d->close();
        return $res;
    }
    function NameSortAsc($a, $b)
    {
        return strcmp($a["fname"], $b["fname"]);
    }
    function NameSortDesc($a, $b)
    {
        return !strcmp($a["fname"], $b["fname"]);
    }
    function SizeSortAsc($a, $b)
    {
        return ($a["size"] > $b["size"]) ? 1 : -1;
    }
    function SizeSortDesc($a, $b)
    {
        return ($a["size"] < $b["size"]) ? 1 : -1;
    }
    function DateSortAsc($a, $b)
    {
        return ($a["time"] > $b["time"]) ? 1 : -1;
    }
    function DateSortDesc($a, $b)
    {
        return ($a["time"] < $b["time"]) ? 1 : -1;
    }
    function delete($_fname)
    {
        if (is_file($this->backupdir . "/" . $_fname)) {
            unlink($this->backupdir . "/" . $_fname);
            return "<font color=green> Backup file $_fname is correctly removed </font>";
        } else
            return "<font color=red> Error while removing backup file $_fname</font>";
    }
    function keep($days = 4)
    {
        if (is_dir($this->backupdir)) {
            $d = dir($this->backupdir);
            while (false !== ($entry = $d->read())) {
                if ($entry != "." and $entry != ".." and (ereg(".bz2$", $entry) or ereg(".gz$", $entry) or ereg(".sql$", $entry))) {
                    if ((filemtime($this->backupdir . "/" . $entry)) < (strtotime('-' . $days . ' days'))) {
                        $this->delete($entry);
                    }
                }
            }
        }
    }
    function getbackup($bfile)
    {
        if (is_file($this->backupdir . "/" . $bfile) and !ereg("../", $bfile)) {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Content-Type: application/force-download");
            header("Content-Disposition: attachment; filename=" . basename($this->backupdir . "/" . $bfile));
            header("Content-Description: File Transfer");
            @readfile($this->backupdir . "/" . $bfile);
            exit;
        }
    }
}
?>