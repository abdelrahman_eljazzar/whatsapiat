<?
	class xml
	{
		var $path, $max, $number, $handle, $counter, $indexhand, $indexfile, $filename,$homeurl ;

		function __construct($homeurl, $pth="" , $urls=50000 ){
			$this->number = 0 ;
			$this->path = $pth ;
			$this->max = $urls ;
			$this->homeurl = $homeurl;
			$this->open();
		}

		function open(){
			$this->indexfile = "{$this->path}sitemap_index.xml";
			$x = ( $this->number == 0 ) ? $x = "" : $x = $this->number ;
			$this->filename = "{$this->path}sitemap$x.xml" ;
			$this->handle = fopen($this->filename,"w+");

			if($this->number == 0){
				$this->indexhand = fopen($this->indexfile,"w+");

				fwrite($this->indexhand,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                fwrite($this->indexhand,"\n<sitemapindex\n\t\txmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\n\t\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n\t\txsi:schemaLocation=\"\n\t\t\thttp://www.sitemaps.org/schemas/sitemap/0.9\n\t\t\thttp://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd\">\n");

			    fwrite($this->indexhand,"<sitemap>\n") ;
			    global $homeurl;
			    $loc = $homeurl."/sitemap/sitemap$x.xml";
			    fwrite($this->indexhand,"\t<loc>$loc</loc>\n") ;
			    $lastmod = date('Y-m-d');
			    fwrite($this->indexhand,"\t<lastmod>$lastmod</lastmod>\n") ;
			    fwrite($this->indexhand,"</sitemap>\n") ;
			}
			else{
				$this->indexhand = fopen($this->indexfile,"a");
				fwrite($this->indexhand,"<sitemap>\n") ;
			    $loc = $this->homeurl."/sitemap/sitemap$x.xml";
			    fwrite($this->indexhand,"\t<loc>$loc</loc>\n") ;
			    $lastmod = date('Y-m-d');
			    fwrite($this->indexhand,"\t<lastmod>$lastmod</lastmod>\n") ;
			    fwrite($this->indexhand,"</sitemap>\n") ;
			}

			fwrite($this->handle,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            fwrite($this->handle,"\n<urlset\n\t\txmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\n\t\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n\t\txsi:schemaLocation=\"\n\t\t\thttp://www.sitemaps.org/schemas/sitemap/0.9\n\t\t\thttp://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">\n");
		}

		function url($loc,$priority="",$lastmod="",$changefreq=""){
			if ( $this->counter == $this->max ){
				$this->number++ ;
				$this->counter = 0 ;
				$this->close() ;

				ob_implicit_flush(true);
				echo "سيتم فصل الروابط على اكثر من خريطه نظرا لتعدى الروابط الحد المسموح به من قبل محركت البحث
				...يرجى الانتظار 10 ثواني وذلك حفاظا على موارد السيرفر<br />";
				sleep(10);
				$this->open() ;
			}

			fwrite($this->handle,"<url>\n") ;
			$loc = str_replace("&", "&amp;", $loc) ;
			fwrite($this->handle,"\t<loc>$loc</loc>\n") ;
			if ($lastmod) fwrite($this->handle,"\t<lastmod>$lastmod</lastmod>\n") ;
			if ($changefreq) fwrite($this->handle,"\t<changefreq>$changefreq</changefreq>\n") ;
			if ($priority) fwrite($this->handle,"\t<priority>$priority</priority>\n") ;
			fwrite($this->handle,"</url>\n") ;

			$this->counter++ ;
		}

		function close(){
			fwrite($this->handle,"\n</urlset>") ;
			fclose($this->handle) ;
		}

		function closeIndex(){
			fwrite($this->indexhand,"\n</sitemapindex>");
			fclose($this->indexhand) ;
		}

		function __destruct(){
			$this->close() ;
			$this->closeIndex() ;
		}
	}
?>
