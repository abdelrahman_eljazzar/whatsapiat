<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty plugin
 *
 * Type:     modifier<br>
 * Name:     ardate<br>
 * Date:     Feb 26, 2003
 * Purpose:  convert \r\n, \r or \n to <<br>>
 * Input:<br>
 *         - contents = contents to replace
 *         - preceed_test = if true, includes preceeding break tags
 *           in replacement
 * Example:  {$text|ardate}
 * @link http://smarty.php.net/manual/en/language.modifier.nl2br.php
 *          nl2br (Smarty online manual)
 * @version  1.0
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param string
 * @return string
 */
function smarty_modifier_ardate($string, $fulltime=false)
{
  if(empty($string)) return;
  $string = date('d-M-Y-h-i-A', $string);
  $time = explode('-', $string);
  $d = $time[0];
  $m = $time[1];
  $y = $time[2];
  $A = $time[5];

  if($_SESSION['Direction'] == 'rtl'){
    if ($m == 'Jan') {$m = "يناير";}
    elseif ($m == 'Feb') { $m = "فبراير"; }
    elseif ($m == 'Mar') { $m = "مارس"; }
    elseif ($m == 'Apr') { $m = "أبريل"; }
    elseif ($m == 'May') { $m = "مايو"; }
    elseif ($m == 'Jun') { $m = "يونيو"; }
    elseif ($m == 'Jul') { $m = "يوليو"; }
    elseif ($m == 'Aug') { $m = "أغسطس"; }
    elseif ($m == 'Sep') { $m = "سبتمبر"; }
    elseif ($m == 'Oct') { $m = "أكتوبر"; }
    elseif ($m == 'Nov') { $m = "نوفمبر"; }
    elseif ($m == 'Dec') { $m = "ديسمبر"; }
    if($time[5] == 'AM'){
      $A = 'صباحا';
    }
    else{
      $A = 'مساءا';
    }
  }
  if($fulltime){
    return "$d $m $y ".$time[3].":".$time[4]." ".$A;
  }
  else{
    return "$d $m $y";
  }
}

/* vim: set expandtab: */

?>
