<?php
/**
 * Smarty plugin
 *
 * This plugin is only for Smarty2 BC
 * @package Smarty
 * @subpackage PluginsFunction
 */

/**
 * Smarty {math} function plugin
 *
 * Type:     function<br>
 * Name:     math<br>
 * Purpose:  handle math computations in template
 *
 * @link http://www.smarty.net/manual/en/language.function.math.php {math}
 *          (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param array                    $params   parameters
 * @param Smarty_Internal_Template $template template object
 * @return string|null
 */
function smarty_function_createform($params)
{
    if(empty($params['all_tag'])){
      $all = array();
    }
    else{
      $all = array('tagname'=>$params['all_tag'], 'tagclass'=>$params['all_class']);
    }
    if(empty($params['title_tag'])){
      $title = array();
    }
    else{
      $title = array('tagname'=>$params['title_tag'], 'tagclass'=>$params['title_class']);
    }
    if(empty($params['input_tag']) && empty($params['input_class'])){
      $input = array();
    }
    else{
      $input = array('tagname'=>$params['input_tag'], 'tagclass'=>$params['input_class']);
    }
    if(empty($params['nolabel'])){
      $params['nolabel'] = 0;
    }
    else{
      $params['nolabel'] = 1;
    }
    if(empty($params['tdclass'])){
      $tdclass = '';
    }
    else{
      $tdclass = $params['tdclass'];
    }
    if(empty($params['form_id'])){
      $form_id = '';
    }
    else{
      $form_id = $params['form_id'];
    }
    if(empty($params['form_attr'])){
      $form_attr = '';
    }
    else{
      $form_attr = $params['form_attr'];
    }
    $btn = array('tagname'=>$params['btn_value'], 'tagclass'=>$params['btn_class']);

    Construct_Form(
    $params['formid'],
    $all,
    $title,
    $input,
    $btn,
    $params['nolabel'],
    $tdclass,
    $form_id,
    $form_attr
    );
}
