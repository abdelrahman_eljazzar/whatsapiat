<?php

function Upload_Files($FILES, $suffix, $stamp){
	$file['name'] = strtolower($FILES['upload']['name']);
	$file['size'] = $FILES['upload']['size']/1000;
	$file['type'] = $FILES['upload']['type'];
	$tmpName = $FILES['upload']['tmp_name'];
	$valid = 1;
    $ext = strtolower(substr($file['name'], strrpos($file['name'], '.') + 1));
    $valid_exe = array('jpg', 'jpeg', 'png', 'gif', 'doc', 'docx', 'pdf', 'zip', 'txt');

    if (! in_array($ext, $valid_exe)){
    	$valid = 0;
    	$error = 'امتداد الملف غير مسموح به';
    }
    if($file['size'] > 1000){
    	$valid = 0;
        $error = 'حجم الملف اقصى من الحجم المسموح به';
    }

    if($valid == 1){
    	$secname = $suffix. "_" . rand(100,500) . "_" . time() .".".$ext;
    	if($stamp == 'admin'){
    		$target_path = "../uploads/";
            $target_path = $target_path . basename($secname);
            move_uploaded_file($tmpName, $target_path);
    	}
    	else if($stamp == 'stamp'){
    		require_once("class.image.php");
    		$secname = $suffix. "_" . rand(100,500) . "_" . time();
    		$img = new watermark;
			$img->name = $secname;
			$img->ext = $ext;
			$img->pos_x = "LEFT";
			$img->pos_y = "BOTTOM";
			$img->img_folder = '../uploads/';
			$img->saveBIG = 1;
			$img->saveTHUMB = 0;
			$img->AddLogo($tmpName, '../watermark.png');
            $secname = $secname.'.'.$ext;
    	}
    	else{
    		$target_path = "uploads/";
            $target_path = $target_path . basename($secname);
            move_uploaded_file($tmpName, $target_path);
    	}
        return $secname;
    }
    else{
    	echo "<script>alert('$error')</script>";
    }
}


function Upload_Audio($FILES, $suffix, $stamp,$name){
	$file['name'] = strtolower($FILES[$name]['name']);
	$file['size'] = $FILES[$name]['size']/1000;
	$file['type'] = $FILES[$name]['type'];
	$tmpName = $FILES[$name]['tmp_name'];
	$valid = 1;
    $ext = strtolower(substr($file['name'], strrpos($file['name'], '.') + 1));
    $valid_exe = array("mp3", "mp4", "wma");

    if (! in_array($ext, $valid_exe)){
    	$valid = 0;
    	$error = 'امتداد الملف غير مسموح به';
    }
    if($file['size'] > 5000000){
    	$valid = 0;
        $error = 'حجم الملف اقصى من الحجم المسموح به';
    }

    if($valid == 1){
    	$secname = $suffix. "_" . rand(100,500) . "_" . time() .".".$ext;
    		$target_path = "../uploads/files/";
            $target_path = $target_path . basename($secname);
            move_uploaded_file($tmpName, $target_path);
        return $secname;
    }
    else{
    	echo "<script>alert('$error')</script>";
    }
}

?>