<?php

session_start();

if($_SESSION['adminper'] != 1){
	echo "<script>window.location='login.php'</script>";
	exit;
}

require_once("init.php");

$Smarty->display("home.tpl");

?>