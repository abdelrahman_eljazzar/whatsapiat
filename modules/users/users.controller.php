<?php

class users extends users_model {
  public $lang;

  public function __construct() {
    parent::__construct();
    //$this->load_language();
  }
 public function index() {
    if(!empty($_SESSION['account_id'])){
      header('Location: /home');
    }
    $this->Smarty->assign('pagetitle', 'تسجيل الدخول');
    $this->Smarty->display('frontend/header.tpl');
    $this->Smarty->display('home.tpl');
    $this->Smarty->display('frontend/footer.tpl');
  }
  public function register(){
   if($_POST){
        if($this->isDuplicateData($_POST['username'], 'Title')){
            $this->JSONMSG(0, 'الاسم موجود مسبقا لدينا');
        }
        if($this->isDuplicateData($_POST['email'], 'Email')){
            $this->JSONMSG(0, 'البريد الألكتروني موجود مسبقاً لدينا');
        }
        if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
            $this->JSONMSG(0, 'ادخلت البريد الألكتروني بشكل غير صحيح');
        }
        if($this->isDuplicateData($_POST['number'], 'Number')){
            $this->JSONMSG(0, 'الرقم مسجل مسبقا لدينا');
        }
        if($_POST['password'] != $_POST['pass2']){
             $this->JSONMSG(0, 'كلمة المرور غير متطابقة');
        }
        $this->add_new_user($_POST);
             $this->JSONMSG(0, 'من فضلك انتظر موافقة مدير الموقع');
        exit;
    }
    else
    {
         $groups=$this->get_interests();
         $this->Smarty->assign('groups', $groups);
         $this->Smarty->display('register.tpl');
    }
  }

  public function editprofile(){
    global $homeurl;
    if($_POST){
      if($this->isDuplicateData_Edit($_REQUEST['username'], 'Title')){
         $this->show_mess_error("الاسم موجود مسبقا لدينا", "1");
        }
        if($this->isDuplicateData_Edit($_REQUEST['email'], 'Email')){
             $this->show_mess_error("البريد الألكتروني موجود مسبقاً لدينا", "1");
        }
        if(!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)){
             $this->show_mess_error("ادخلت البريد الألكتروني بشكل غير صحيح", "1");
        }
        if($_REQUEST['password'] != $_REQUEST['pass2']){
             $this->show_mess_error("كلمة المرور غير متطابقة", "1");
        }
      $data['username'] = $_REQUEST['username'];
      $data['email'] = $_REQUEST['email'];
      $data['cat_id'] = $_REQUEST['cat_id'];
      $data['user_type'] = $_REQUEST['user_type'];
      $data['register_type'] = $_REQUEST['register_type'];
      if($_REQUEST['password']){
        $data['password'] = md5($_REQUEST['password']);
      }
      $this->update_account_info($data);
     // $this->JSONMSG(1, 'تم حفظ البيانات بنجاح');
      redirect($homeurl.'/home');
    }
    else{
       $user=$this->get_user_info();
     $this->Smarty->assign('user_info', $user);
      $this->Smarty->display('header.tpl');
      $this->Smarty->display('edit_account.tpl');
      $this->Smarty->display('footer.tpl');
    }
  }
  public function account_conclusion(){
     $user=$this->get_user_info();
     $this->Smarty->assign('user_info', $user);
     $this->Smarty->display('header.tpl');
     $this->Smarty->display('conclusion.tpl');
      $this->Smarty->display('footer.tpl');
  }
   public function transfer(){
     $this->Smarty->display('header.tpl');
     $this->Smarty->display('transfer.tpl');
      $this->Smarty->display('footer.tpl');
  }
   public function create_group(){
     global $homeurl;
      if($_POST){
        if($this->isDuplicateData_Group($_REQUEST['title'], 'Title')){
             $this->show_mess_error("الاسم موجود مسبقا لدينا فى المجموعات الخاصة بك", "1");
        }
        $this->add_new_group($_REQUEST);
       redirect($homeurl.'/users/user_groups');
    }
    else
    {
        $this->Smarty->display('header.tpl');
        $this->Smarty->display('create_group.tpl');
        $this->Smarty->display('footer.tpl');
    }
  }
  public function edit_group(){
     global $homeurl;
   if($_POST){
        if($this->isDuplicateData_Edit_Group($_REQUEST['title'], 'Title',$_REQUEST['id'])){
            $this->show_mess_error("الاسم موجود مسبقا لدينا فى المجموعات الخاصة بك", "1");
        }
        $this->update_group($_REQUEST);
        // $this->JSONMSG(1, 'تم حفظ البيانات بنجاح');
        redirect($homeurl.'/users/user_groups');
    }
    else
    {
        $group=$this->get_group($_GET[ID]);
        $this->Smarty->assign('group', $group);
        $this->Smarty->assign('ID', $_GET[ID]);
        $this->Smarty->display('header.tpl');
        $this->Smarty->display('edit_group.tpl');
        $this->Smarty->display('footer.tpl');
    }
  }
   public function delete_group(){
     global $homeurl;
     $group_id=$_REQUEST[ID];
        $this->delete_user_group($group_id);
        redirect($homeurl.'/users/user_groups');
  }
   public function add_contacts(){
      global $homeurl;
      if($_POST){
        $this->add_group_contacts($_POST);
        redirect($homeurl.'/users/user_groups');
      }
      else
      {
         $this->Smarty->assign('ID', $_GET[ID]);
        $this->Smarty->display('header.tpl');
        $this->Smarty->display('add_contact.tpl');
        $this->Smarty->display('footer.tpl');
      }

  }
   public function user_groups(){
     $groups=$this->get_user_groups();
     $this->Smarty->assign('groups', $groups);
     $this->Smarty->display('header.tpl');

     $this->Smarty->display('user_groups.tpl');
     $this->Smarty->display('footer.tpl');
  }
   public function charge(){
     $this->Smarty->display('header.tpl');
     $this->Smarty->display('charge.tpl');
     $this->Smarty->display('footer.tpl');
  }
   public function send_group(){
     $groups=$this->get_user_groups();
     $this->Smarty->assign('groups', $groups);
     global $homeurl;
     if($_POST)
     {
         $message = $this->auto_load('messages');
         $message->send_group_message($_POST);
          redirect($homeurl.'/home');
     }
     else
     {
     $this->Smarty->display('header.tpl');
     $this->Smarty->display('send_group.tpl');
     $this->Smarty->display('footer.tpl');
   }
  }
   public function send_message(){
     global $homeurl;
     if($_POST)
     {
         $message = $this->auto_load('messages');
         $message->send_message($_POST);
         redirect($homeurl.'/home');
     }
     else
     {
         $this->Smarty->display('header.tpl');
         $this->Smarty->display('send_message.tpl');
        $this->Smarty->display('footer.tpl');
     }

  }
    public function archive(){
     $this->Smarty->display('header.tpl');
     $this->Smarty->display('archive.tpl');
     $this->Smarty->display('footer.tpl');
  }
   public function log(){
     $logs=$this->get_user_log();
     $this->Smarty->assign('logs', $logs);
     $this->Smarty->display('header.tpl');

     $this->Smarty->display('user_log.tpl');
     $this->Smarty->display('footer.tpl');
  }
 public function transfer_points(){
   global $homeurl;
    if(isset($_REQUEST)){
        $check=$this->send_points($_REQUEST);
        if($check == 1)
        {
             $this->show_mess_error("رصيدك لا يسمح", "1");
        }
        else if ($check == 2)
        {
           $this->show_mess_error("لقد ادخلت رقم غير صحيح", "1");

        }
        redirect($homeurl.'/users/dashboard');
    }
  }
}


?>