﻿<?php

class users_model extends corehelper {

  public function __construct() {
    parent::__construct();
  }

public function isDuplicateData($value, $column){
    return $this->db->get_row("SELECT ID,Email,Title,Number FROM users WHERE `$column`='$value'");
}

public function isDuplicateData_Edit($value, $column){
   $userid = $_SESSION['account_id'];
    return $this->db->get_row("SELECT ID,Email,Title,Number FROM users WHERE `$column`='$value' AND NOT ID=".$userid);
}

public function isDuplicateData_Group($value, $column){
   $userid = $_SESSION['account_id'];
    return $this->db->get_row("SELECT Title FROM user_groups WHERE `$column`='$value' AND User_ID=".$userid);
}

public function isDuplicateData_Edit_Group($value, $column,$ID){
   $userid = $_SESSION['account_id'];
    return $this->db->get_row("SELECT Title FROM user_groups WHERE `$column`='$value' AND User_ID=".$userid." AND NOT ID = ".$ID);
}
public function newPass($userid){
    $newpass = $this->salt();
    $md5pass = md5($newpass);
    $this->db->query("UPDATE users SET Password='$md5pass' WHERE ID='$userid'");
    return $newpass;
}

public function checkUser($username, $password){
    $password = md5($password);
    $user = $this->db->get_row("SELECT users.ID,users.Title,users.Points,users.Used_Points,users.Active,points_cost.Title As Plan_Title FROM users INNER JOIN points_cost ON points_cost.ID = users.Plan_ID WHERE users.Title='$username' AND users.Password='$password'");
    if($user){
      return $user;
    }
    else{
      return false;
    }
  }
   public function get_interests(){
     $groups = $this->db->get_results("SELECT * FROM categories  ORDER BY ID ASC ", 'ARRAY_A');
     return $groups;
  }
  public function add_new_user($data){
    $free_points = $this->db->get_row("SELECT Points FROM points_cost WHERE ID='1'", 'ARRAY_A');
    $password= md5($data['password']);
    $timeofnow=time();

    $this->db->query("INSERT INTO users (Title,Email,Number,Cat_ID,User_Type,Register_Type,Password,Points) VALUES ('$data[username]','$data[email]','$data[number]','$data[cat_id]','$data[user_type]','$data[register_type]','$password','$free_points[Points]')");
    $user_id = $this->db->insert_id;
    $this->db->query("INSERT INTO users_log (Text,Points,User_ID,Date) VALUES ('ﻧﻘﺎﻁ ﻣﺠﺎﻧﻴﺔ ﻋﻨﺪ اﻟﺘﺴﺠﻴﻞ ﺑﺎﻟﻤﻮﻗﻊ ','$free_points[Points]','$user_id','$timeofnow')");
     $_SESSION['account_id'] = $user_id;
     $_SESSION['account_name'] = $data[username];
  }

  public function update_account_info($data){
      $userid = $_SESSION['account_id'];
      $this->db->query("UPDATE users SET Title='$data[username]',Email='$data[email]',Cat_ID='$data[cat_id]',User_Type='$data[user_type]',Register_Type='$data[register_type]' WHERE ID=".$userid);
      if($data[password] != "")
      {
        $this->db->query("UPDATE users SET Password='".$data[password]."' WHERE ID=".$userid);
      }
  }
  public function get_user_info(){
     $userid = $_SESSION['account_id'];
     $user = $this->db->get_row("SELECT users.ID,users.Email,users.Number,users.Title,users.Points,users.Used_Points,points_cost.Title As Plan_Title FROM users INNER JOIN points_cost ON users.Plan_ID=points_cost.ID WHERE users.ID='".$userid."'");
     $user_info[Title]=$user->Title;
     $user_info[Points]=$user->Points;
     $user_info[Used_Points]=$user->Used_Points;
     $user_info[Rest_Points]=(int)$user->Points - (int)$user->Used_Points;
     $user_info[Used_Points]=$user->Used_Points;
     $user_info[Email]=$user->Email;
     $user_info[Number]=$user->Number;
     $user_info[Plan_Title]=$user->Plan_Title;
     return $user_info;
  }
  public function add_new_group($data){
     $userid = $_SESSION['account_id'];
    $this->db->query("INSERT INTO user_groups (Title,User_ID) VALUES ('$data[title]','$userid')");
    $groupid = $this->db->insert_id;
  }
   public function update_group($data){
    $this->db->query("UPDATE user_groups SET Title='$data[title]' WHERE ID=$data[id]");
    $groupid = $this->db->insert_id;
  }
   public function get_group($id){
     $group = $this->db->get_row("SELECT * FROM user_groups  WHERE ID='".$id."'");
     return $group;
  }
   public function add_group_contacts($data){
     $numbers=explode('-',$data['nums']);
     $contacts = $this->db->get_results("SELECT Number FROM contacts WHERE Group_ID=".$data[group_id], 'ARRAY_A');
     if($contacts)
     {
       foreach($contacts AS $contact)
       {
         $final_contacts[]=$contact[Number];
       }
        //print_r($final_contacts);die();
        foreach($numbers AS $number)
        {
            if(in_array($number,$final_contacts) || ($final_numbers != "" && in_array($number,$final_numbers)))
            {
              continue;
            }
            else
            {
              $final_numbers[]=$number;
            }
        }
     }
     else
     {
        foreach($numbers AS $number)
        {
            if($final_numbers != "" && in_array($number,$final_numbers))
            {
              continue;
            }
            else
            {
              $final_numbers[]=$number;
            }
        }
     }
     //print_r($final_numbers);die();
     if($final_numbers !="")
     {
       foreach($final_numbers AS $number)
        {
            $this->db->query("INSERT INTO contacts (Number,Group_ID) VALUES ('$number','$data[group_id]')");
        }

     }

  }
  public function delete_user_group($group_id){
    $this->db->query("DELETE FROM contacts WHERE Group_ID=$group_id");
    $this->db->query("DELETE FROM user_groups WHERE ID=$group_id");
  }
   public function get_user_groups(){
     $userid = $_SESSION['account_id'];
     $groups = $this->db->get_results("SELECT user_groups.*, COUNT(contacts.ID) AS contacts_count FROM user_groups LEFT JOIN contacts ON user_groups.ID = contacts.Group_ID WHERE user_groups.User_ID=".$userid." GROUP BY user_groups.ID ORDER BY user_groups.ID ASC ", 'ARRAY_A');
     return $groups;
  }
   public function get_user_log(){
     $userid = $_SESSION['account_id'];
     $logs = $this->db->get_results("SELECT * FROM users_log WHERE User_ID=".$userid." ORDER BY ID DESC ", 'ARRAY_A');
     return $logs;
  }
  public function send_points($data){
     $userid = $_SESSION['account_id'];
     $user = $this->db->get_row("SELECT Points,Used_Points FROM users WHERE ID=".$userid, 'ARRAY_A');
     $reciever_user=$this->db->get_row("SELECT ID,Points FROM users WHERE Number='".$data[reciver_num]."'", 'ARRAY_A');
     if($reciever_user)
     {
        $rest_points=$user[Points]-$user[Used_Points];
        if($data[points] <= $rest_points)
        {
            $reciever_points=$reciever_user[Points] + $data[points];
            $this->db->query("UPDATE users SET Points='$reciever_points' WHERE ID=$reciever_user[ID]");
            $user_new_points=$user[Points] - $data[points];
            $user_used_points = $user[Used_Points] + $data[points];
            $this->db->query("UPDATE users SET Used_Points='$user_used_points' WHERE ID = '$userid'");
        }
        else
        {
          return 1;
        }
     }
     else
     {
       return 2;
     }

     return $user;
  }

}


?>