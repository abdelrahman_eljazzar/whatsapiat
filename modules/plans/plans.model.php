<?php

class plans_model extends corehelper {

  public function __construct() {
    parent::__construct();
  }

  public function insertpayment($data){
    $plan = $this->get_plan($data['item_number']);
    if($plan['price'] != $data['payment_amount'] || $data['payment_currency'] != 'USD'){die();}
    $this->db->query("UPDATE users SET planid='$data[item_number]' WHERE id='$data[userid]'");
    $this->db->query("INSERT INTO `payments` (txnid,User_ID,Cost,Payment_Status,Item_ID, From_Date, Endtime) VALUES (
    '".$data['txn_id']."' ,
    '".$data['userid']."' ,
    '".$data['payment_amount']."' ,
    '".$data['payment_status']."' ,
    '".$data['item_number']."' ,
    '".TIMENOW."',
    '".(TIMENOW+(30*24*60*60))."'
    )");
  }

  public function get_payment(){
    $userid = $_SESSION['userinfo']['id']=10;
    $payment = $this->db->get_row("SELECT * FROM payments WHERE User_ID='$userid'", 'ARRAY_A');
    return $payment;
  }

  public function get_plan($id){
    $plan = $this->db->get_row("SELECT * FROM points_cost WHERE ID='$id'", 'ARRAY_A');
    return $plan;
  }

  public function get_plans($start,$end){
    $latest_plans = $this->db->get_results("SELECT * FROM points_cost WHERE Lang='ar' AND NOT ID IN (1,2) ORDER BY ID ASC LIMIT ".$start.",".$end, 'ARRAY_A');
    return $latest_plans;
  }

}


?>