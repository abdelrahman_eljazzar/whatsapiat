<?php

class plans extends plans_model {

  public function __construct() {
    parent :: __construct();
  }
  public function Latest_plans($start,$end){
     $plans_latest=$this->get_plans($start,$end);
      return $plans_latest;
  }
  public function get_user_payments(){
     $payments=$this->get_payment();
     //$this->Smarty->assign('mentions', $mentions);
      //$this->Smarty->display('mention_display.tpl');
     print_r(json_encode($payments));
  }
  public function processpaid() {
    if($_GET['userid']) {
      $req = 'cmd=_notify-validate';
      foreach($_POST as $key => $value) {
        $value = urlencode(stripslashes($value));
        $value = preg_replace('/(.*[^%^0^D])(%0A)(.*)/i', '${1}%0D%0A${3}', $value); // IPN fix
        $req .= "&$key=$value";
      }
      $header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
      $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
      $header .= "Content-Length: ".strlen($req)."\r\n\r\n";
      $fp = fsockopen('ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30);
      if(!$fp) {
        die('HTTP ERROR');
      }
      else{
        fputs($fp, $header.$req);
        while(!feof($fp)) {
          $res = fgets($fp, 1024);
          if(strcmp($res, "VERIFIED") == 0) {
            // PAYMENT VALIDATED & VERIFIED!
          }
          elseif(strcmp($res, "INVALID") == 0) {
            die('PAYMENT INVALID & INVESTIGATE MANUALY!');
          }
        }
        fclose($fp);
      }
      // assign posted variables to local variables
      $data['item_name'] = $_POST['item_name'];
      $data['userid'] = $_GET['userid'];
      $data['item_number'] = $_POST['item_number'];
      $data['payment_status'] = $_POST['payment_status'];
      $data['payment_amount'] = $_POST['mc_gross'];
      $data['payment_currency'] = $_POST['mc_currency'];
      $data['txn_id'] = $_POST['txn_id'];
      $data['receiver_email'] = $_POST['receiver_email'];
      $data['payer_email'] = $_POST['payer_email'];
      $data['custom'] = $_POST['custom'];
      // PAYMENT VALIDATED & VERIFIED!
      $this->insertpayment($data);
    }
  }

  public function successful() {
    $this->show_mess_error('تمت عملية الدفع بنجاح...قد تستغرق عملبة اعتماد حسابك بحد اقصى ساعة', 2);
  }

  public function cancelled() {
    $this->show_mess_error('فشلت عملبة الدفع من فضلك حاول مرة اخرى', 2);
  }

  public function pay() {
    global $config;
    $paypal_email = $config['paypal_email'];
    $return_url = SITEURL.'/plans/successful';
    $cancel_url = SITEURL.'/plans/cancelled';
    $notify_url = SITEURL.'/plans/processpaid/?userid='.$_SESSION['userinfo']['id'];
    $plan = $this->get_plan($_POST['item_number']);
    $item_name = $plan['title'];
    $item_amount = $plan['price'];
    $_POST['cmd'] = '_xclick';
    $_POST['no_note'] = '1';
    $_POST['lc'] = 'US';
    $_POST['currency_code'] = 'USD';
    $_POST['bn'] = 'PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest';
    if(!isset ($_POST["txn_id"]) && !isset ($_POST["txn_type"])) {
      $querystring .= "?business=".urlencode($paypal_email)."&";
      $querystring .= "item_name=".urlencode($item_name)."&";
      $querystring .= "amount=".urlencode($item_amount)."&";
      foreach($_POST as $key => $value) {
        $value = urlencode(stripslashes($value));
        $querystring .= "$key=$value&";
      }
      $querystring .= "return=".urlencode(stripslashes($return_url))."&";
      $querystring .= "cancel_return=".urlencode(stripslashes($cancel_url))."&";
      $querystring .= "notify_url=".urlencode($notify_url);
      // Append querystring with custom field
      //$querystring .= "&custom=".USERID;
      // Redirect to paypal IPN
      header('location:https://www.sandbox.paypal.com/cgi-bin/webscr'.$querystring);
    }
  }

}

?>