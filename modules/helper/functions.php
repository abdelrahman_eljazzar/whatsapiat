<?php

class corehelper {
  public $db;
  public $Smarty;

  public function __construct() {
    global $Smarty;
    global $db;
    $this->db = $db;
    $this->Smarty = $Smarty;
  }

  public function havePermission($action, $value='') {
    if(empty($_SESSION['userinfo'])){
      $_SESSION['userinfo'] = $this->db->get_row("SELECT * FROM users WHERE id='$_SESSION[account_id]'", 'ARRAY_A');
      if($_SESSION['userinfo']['managerid'] > 0){
        $manager = $this->db->get_row("SELECT quota,planid FROM users WHERE id='".$_SESSION['userinfo']['managerid']."'", 'ARRAY_A');
        $_SESSION['employee_id'] = $_SESSION['userinfo']['id'];
        $_SESSION['userinfo']['id'] = $_SESSION['userinfo']['managerid'];
        $_SESSION['userinfo']['quota'] = $manager['quota'];
        $_SESSION['userinfo']['planid'] = $manager['planid'];
        $_SESSION['account_id'] = $_SESSION['userinfo']['managerid'];
        $_SESSION['userinfo']['permission'] = unserialize($_SESSION['userinfo']['permission']);
        if(empty($_SESSION['userinfo']['permission']['alerts'])){
          $_SESSION['userinfo']['permission']['alerts'] = 0;
        }
        else{
          $_SESSION['userinfo']['permission']['alerts'] = implode(',', $_SESSION['userinfo']['permission']['alerts']);
        }
        $twitter = $this->db->get_row("SELECT * FROM social_tokens WHERE userid='$_SESSION[account_id]'", 'ARRAY_A');
        if($twitter){
          $_SESSION['userinfo']['twitter']['token'] = $twitter['token'];
          $_SESSION['userinfo']['twitter']['secret_token'] = $twitter['secret_token'];
        }
      }
      else{
        $twitter = $this->db->get_row("SELECT * FROM social_tokens WHERE userid='$_SESSION[account_id]'", 'ARRAY_A');
        if($twitter){
          $_SESSION['userinfo']['twitter']['token'] = $twitter['token'];
          $_SESSION['userinfo']['twitter']['secret_token'] = $twitter['secret_token'];
        }
        $_SESSION['userinfo']['permission'] = array('add' => 1,'edit' => 1, 'delete' => 1);
        return true;
      }
    }
    if($_SESSION['userinfo']['permission'][$action] == 1 OR $action == 'login'){
      if($action == 'add'){
        $log = 'لقد قام بإضافة تنبيه جديد ('.$value.')';
      }
      elseif($action == 'edit'){
        $log = 'لقد قام بتعديل التنبيه ('.$value.')';
      }
      elseif($action == 'delete'){
        $log = 'لقد قام بحذف التنبيه ('.$value.')';
      }
      else{
        $log = 'تسجيل الدخول';
      }
      $this->db->insert('employee_log', array('userid' => $_SESSION['employee_id'], 'managerid' => $_SESSION['userinfo']['managerid'],'log' => $log,'time' => TIMENOW));
      return true;
    }
    else{
      $this->show_mess_error('غير مصرح لك بالقيام بتلك العملية', 1);
    }
  }

  public function auto_load($module) {
    include_once(MOD_DIR.'/'.$module.'/'.$module.'.model.php');
    include_once(MOD_DIR.'/'.$module.'/'.$module.'.controller.php');
    $$module = new $module();
    return $$module;
  }

  public function salt() {
    $alph = array('A', 'a', 'B', 'b', 'C', 'c', 'D', 'd', 'E', 'e', 'F', 'f', 'G', 'g', 'H', 'h', 'I', 'i', 'J', 'K', 'k', 'L', 'l', 'M', 'm', 'N', 'n', 'O', 'o', 'P', 'p', 'Q', 'q', 'R', 'r', 'S', 's', 'T', 't', 'U', 'u', 'V', 'v', 'W', 'w', 'X', 'x', 'Y', 'y', 'Z', 'z');
    for($i = 0; $i < 6; $i++) {
      $randc = rand(1, 39);
      $salt .= $alph[$randc];
    }
    return $salt;
  }

  public function string_cut($string, $num) {
    $explode = split(' ', $string);
    for($i = 0; $i < $num; $i++) {
      $word .= $explode[$i].' ';
    }
    return $word;
  }

  public function JSONMSG($response, $message) {
    echo json_encode(array('status' => $response, 'message' => $message));
    exit;
  }

  public function IsEmail($email) {
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      return false;
    }
    return true;
  }

  public function auto_links_con($text) {
  // For http
    $text = str_replace("http://www.", "www.", $text);
    $text = str_replace("www.", "http://www.", $text);
    $text = preg_replace("/([\w]+:\/\/[\w-?&;#~=\.\/\@]+[\w\/])/i", "<a target=\"_blank\" href=\"$0\">$0</a>", $text);
    // For mail
    $text = preg_replace("/([\w-?&;#~=\.\/]+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?))/i", "<a href=\"mailto:$0\">$0</a>", $text);
    return $text;
  }

  public function Paging($sql, $paging_limit) {
    require ("class.paging.php");
    global $Smarty;
    if(!isset ($paging_limit)) {
      $paging_limit = 10;
    }
    $paging = new paging($paging_limit, 3);
    $paging->query($sql);
    $page = $paging->print_info();
    $current_page = $_SERVER['PHP_SELF']."?".$_SERVER['QUERY_STRING'];
    $Smarty->assign("page", $page);
    $Smarty->assign("paging", $paging->print_link());
    $Smarty->assign("current_page", $current_page);
    $Smarty->assign("paging_limit", $paging_limit);
    //$Smarty->display("paging_class.tpl");
    while($result = $paging->result_assoc()) {
      $resultem[] = $result;
    }
    return $resultem;
  }

  public function timediffer($time) {
    if(empty ($time))
      return true;
    $differ = '';
    $today = time();
    $difference = $today-$time;
    $count = 0;
    if(floor($difference/31104000) > 0) {
      if($_SESSION['lang_name'] == 'ar')
        $text = ' عام ';
      else
        $text = ' عام ';
      $differ .= floor($difference/31104000).$text;
      $difference -= 31104000*floor($difference/31104000);
      $count++;
    }
    if(floor($difference/2592000) > 0) {
      if($_SESSION['lang_name'] == 'ar')
        $text = ' شهر ';
      else
        $text = ' month ';
      $differ .= floor($difference/2592000).$text;
      $difference -= 2592000*floor($difference/2592000);
      $count++;
    }
    if(floor($difference/84600) > 0 && $count < 2) {
      if($_SESSION['lang_name'] == 'ar')
        $text = ' يوم ';
      else
        $text = ' day ';
      $differ .= floor($difference/84600).$text;
      $difference -= 84600*floor($difference/84600);
      $count++;
    }
    if(floor($difference/3600) > 0 && $count < 2) {
      if($_SESSION['lang_name'] == 'ar')
        $text = ' ساعه ';
      else
        $text = ' hour ';
      $differ .= floor($difference/3600).$text;
      $difference -= 3600*floor($difference/3600);
      $count++;
    }
    if(floor($difference/60) > 0 && $count < 2) {
      if($_SESSION['lang_name'] == 'ar')
        $text = ' دقيقه ';
      else
        $text = ' min ';
      $differ .= floor($difference/60).$text;
      $difference -= 60*floor($difference/60);
      $count++;
    }
    if($count < 2) {
      if($_SESSION['lang_name'] == 'ar')
        $text = ' ثانيه ';
      else
        $text = ' sec ';
      $differ .= $difference.$text;
    }
    return $differ;
  }

  public function ProtectFlood($action, $duration) {
    if(isset ($_SESSION['lastdo'][$action])) {
      $now = time();
      $diff = $_SESSION['lastdo'][$action]-$now;
      if($now < $_SESSION['lastdo'][$action]) {
        $text = 'لا يمكنك القيام بعملية اخرى الا بعد مرور '.date('s', $diff).' ثانيه';
        echo "<script>alert('$text');</script>";
        exit;
      }
      else{
        $_SESSION['lastdo'][$action] = time()+$duration;
      }
    }
    else{
      $_SESSION['lastdo'][$action] = time()+$duration;
    }
  }

  public function ardate($string) {
    $string = date('d-m-Y-h-i-A', $string);
    $time = explode('-', $string);
    $d = $time[0];
    $m = $time[1];
    if($m == '01') {
      $m = "يناير";
    }
    elseif($m == '02') {
      $m = "فبراير";
    }
    elseif($m == '03') {
      $m = "مارس";
    }
    elseif($m == '04') {
      $m = "أبريل";
    }
    elseif($m == '05') {
      $m = "مايو";
    }
    elseif($m == '06') {
      $m = "يونيو";
    }
    elseif($m == '07') {
      $m = "يوليو";
    }
    elseif($m == '08') {
      $m = "أغسطس";
    }
    elseif($m == '09') {
      $m = "سبتمبر";
    }
    elseif($m == '10') {
      $m = "أكتوبر";
    }
    elseif($m == '11') {
      $m = "نوفمبر";
    }
    elseif($m == '12') {
      $m = "ديسمبر";
    }
    $y = $time[2];
    if($time[5] == 'AM') {
      $A = 'صباحا';
    }
    else{
      $A = 'مساءا';
    }
  //return "$d $m $y  ".$time[3].":".$time[4]." ".$A;
    return "$d $m $y";
  }

  public static function Security($value) {
    if(!is_numeric($value)) {
      if(is_array($value)) {
        foreach($value AS $key => $v) {
          if(is_array($v))
            $value[$key] = Security($v);
          else{
            if(get_magic_quotes_gpc())
              $value[$key] = htmlspecialchars(trim($v));
            else
              $value[$key] = htmlspecialchars(addslashes(trim($v)));
          }
        }
      }
      else{
        if(get_magic_quotes_gpc())
          $value = htmlspecialchars(trim($value));
        else
          $value = htmlspecialchars(addslashes(trim($value)));
      }
    }
    return $value;
  }

  function jsredirect($redirecturl){
    echo '<script>window.opener.location.reload();window.close();</script>';
  }

  function jsredirectto($redirecturl){
    echo '<script>window.location="'.$redirecturl.'"</script>';
  }

  function GetSocialAvatar($link, $oldlink=''){
  	if(! empty($oldlink)){
  	  @unlink('uploads/'.$oldlink);
  	}
    $image = file_get_contents($link);
    $avatar = rand(100,500).'_'.time().'.jpeg';
  	$handle = fopen('uploads/'.$avatar, 'w+');
  	fwrite($handle, $image);
  	fclose($handle);
  	return $avatar;
  }

  function show_notify_msg($message) {
    echo '<p class="bg-info">'.$message.'</p>';
    die();
  }

  function show_mess_error($message, $ch) {
    $this->Smarty->assign("ch", $ch);
    $this->Smarty->assign('pagetitle', 'رسالة إدارية');
    $this->Smarty->assign("text", $message);
    $this->Smarty->display("header.tpl");
    $this->Smarty->display("error_msg.tpl");
    $this->Smarty->display("footer.tpl");
    die();
  }

  public function Upload_File($FILES, $folder='', $type='image') {
    $file['name'] = strtolower($FILES['name']);
    $file['size'] = $FILES['size']/1000000;
    $file['type'] = $FILES['type'];
    $tmpName = $FILES['tmp_name'];
    $ext = strtolower(substr($file['name'], strrpos($file['name'], '.')+1));
    $valid = 1;
    if($type == 'image') {
      $max_size = 10;
      $valid_exe = array('jpg', 'jpeg', 'png', 'gif');
    }
    elseif($type == 'video') {
      $max_size = 50;
      $valid_exe = array('avi', 'mpeg', 'flv', '3gp', 'mpg', 'mp4', 'caf', 'mov');
    }
    else{
      $max_size = 10;
      $valid_exe = array('jpg', 'jpeg', 'png', 'gif', 'doc', 'docx', 'pdf', 'zip', 'txt');
    }
    if(!in_array($ext, $valid_exe)) {
      $valid = 0;
      $error = 'نوع الملف غير مسموح به';
    }
    if($file['size'] > $max_size) {
      $valid = 0;
      $error = 'حجم الملف تعدى الحجم المسموح به';
    }
    if($valid == 1) {
      if(!file_exists(ROOT_DIR.'/uploads/'.$suffix)) {
        mkdir(ROOT_DIR.'/uploads/'.$suffix);
      }
      $secname = $suffix.'/'.md5(rand(10000, 50000).time()).'.'.$ext;
      $target_path = ROOT_DIR.'/uploads/'.$secname;
      move_uploaded_file($tmpName, $target_path);
      if(file_exists($target_path)){
        return $secname;
      }
      else{
        $this->JSONMSG(0, 'فشل رفع الملف من فضلك حاول مرة اخرى');
      }
    }
    else{
      $this->JSONMSG(0, $error);
    }
  }

}

?>