<?php

class page extends login_model {

  public function __construct() {
    parent::__construct();
  }

  public function page_display() {
    $ID=$_REQUEST[ID];
    $this->display_page($ID);
    $this->Smarty->display('frontend/header.tpl');
    $this->Smarty->display('frontend/page.tpl');
    $this->Smarty->display('frontend/footer.tpl');
  }

}

?>