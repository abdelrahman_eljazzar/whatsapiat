<?php

class login extends login_model {

  public function __construct() {
    parent::__construct();
  }

  public function submit() {
    if(!empty($_REQUEST['username']) AND !empty($_REQUEST['password'])){
       $user_object = $this->auto_load('users');
      $user = $user_object->checkUser($_REQUEST['username'], $_REQUEST['password']);
      if($user){
        if($user->Active == 1)
        {
            $_SESSION['account_id'] = $user->ID;
            $_SESSION['account_name'] = $user->Title;
            echo 1;
        }
        else
        {
             $this->JSONMSG(0, 'من فضلك انتظر تفعيل مدير الموقع لحسابك');
        }

      }
      else{
           $this->JSONMSG(0, 'خطا اسم المستخدم او كلمة المرور');
      }
    }
    else{
       $this->JSONMSG(0, 'نسيت ادخال اسم المستخدم او كلمة المرور');
    }
    exit;
  }
  public function recover() {
    if(!empty($_REQUEST['email'])){
      if(!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)){
        $this->JSONMSG(0, 'ادخلت البريد الألكتروني بشكل غير صحيح');
        exit;
      }
      $user_object = $this->auto_load('users');
      $user = $user_object->isDuplicateData($_REQUEST['email'], 'Email');
      if(!$user){
        $this->JSONMSG(0, 'لم نعثر على البريد الألكتروني');
        exit;
      }
      $newPass = $user_object->newPass($user->ID);
      require_once(INC_DIR.'/email_function.php');
      $this->Smarty->assign('message', $user->Title.'<br>نود اعلامك بأنه تم تعيين كلمة مرور جديدة خاصة بحسابك بنجاح ويمكنك الان الدخول بأستخدام كلمة المرور بالأسفل<br>'.$newPass);
      $template = $this->Smarty->fetch('email_templates/email_template.tpl');
      send_php($user->Email, 'اعادة تعيين كلمة المرور تمت بنجاح', $template);
      $this->JSONMSG(0, 'تم ارسال بيانات حسابك الى بريدك');
    }
    else{
      $this->JSONMSG(0, 'جميع الحقول مطلوبة');
    }
    exit;
  }

  public function singout() {
    global $homeurl;
    session_destroy();
    header('Location: '.$homeurl);
  }

  public function index() {
   /* if(!empty($_SESSION['account_id'])){
      header('Location: /home');
    }*/
    $this->Smarty->display('login.tpl');
  }

}

?>