<?php

class home extends home_model {
  public $lang;

  public function __construct() {
    parent::__construct();
    $this->load_language();
  }
  public function home(){
     $pages=$this->get_pages();
     $message = $this->auto_load('messages');
     $plans_obj = $this->auto_load('plans');
     $first_plans=$plans_obj->Latest_plans(0,4);
     $second_plans=$plans_obj->Latest_plans(4,8);
     $first_messages=$message->get_latest_message_templates(0,4);
     $second_messages=$message->get_latest_message_templates(4,8);
     $third_messages=$message->get_latest_message_templates(7,11);
     $this->Smarty->assign('pages', $pages);
     $this->Smarty->assign('first_messages', $first_messages);
     $this->Smarty->assign('second_messages', $second_messages);
     $this->Smarty->assign('third_messages', $third_messages);
     $this->Smarty->assign('first_plans', $first_plans);
     $this->Smarty->assign('second_plans', $second_plans);
  }
  public function display_page(){
    $this->home();
    $id=$_REQUEST[ID];
     $page_display=$this->get_one_page($id);
     $this->Smarty->assign('page_display', $page_display);
     $this->Smarty->display('frontend/header.tpl');
    $this->Smarty->display('frontend/page.tpl');
    $this->Smarty->display('frontend/footer.tpl');
  }

  private function load_language(){
    if(empty($_GET['lang'])){
      $this->lang = 'ar';
    }
    else{
      $this->lang = $_GET['lang'];
    }
    include(ROOT_DIR.'/lang/'.$this->lang.'.php');
    $this->Smarty->assign('lang', $lang);
    $this->Smarty->assign('sitelang', $this->lang);
  }

  public function frontend(){
     $this->home();
    $this->Smarty->display('frontend/header.tpl');
    $this->Smarty->display('frontend/home.tpl');
    $this->Smarty->display('frontend/footer.tpl');
  }

  public function index(){
  $user = $this->auto_load('users');
   $user_info=$user->get_user_info();
   $this->Smarty->assign('user_info', $user_info);
   $this->Smarty->display('header.tpl');
   $this->Smarty->display('dashboard.tpl');
   $this->Smarty->display('footer.tpl');
  }

}


?>