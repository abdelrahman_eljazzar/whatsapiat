<?php

class messages_model extends corehelper {

  public function __construct() {
    parent::__construct();
  }

  public function get_message_template_categories(){
    $categories = $this->db->get_row("SELECT * FROM messages_categories ORDER BY ID ASC", 'ARRAY_A');
    return $categories;
  }
   public function get_category_message_templates($id){
    $templates = $this->db->get_row("SELECT * FROM messages_templates WHERE Dept_ID = ".$id." ORDER BY ID DESC", 'ARRAY_A');
    return $templates;
  }
  public function get_latest_message_templates($start,$end){
    $templates = $this->db->get_results("SELECT messages_templates.ID,messages_templates.Title,messages_templates.Text,messages_categories.Title AS Cat_Title FROM messages_templates LEFT JOIN messages_categories ON messages_categories.ID=messages_templates.Dept_ID WHERE messages_templates.Text != '' ORDER BY messages_templates.ID DESC LIMIT ".$start.",".$end, 'ARRAY_A');
    return $templates;
  }

  public function send_message($data){
       $userid = $_SESSION['account_id'];
      $this->db->query("INSERT INTO sent_messages (Text,User_ID,Number) VALUES ('$data[message]','$userid','$data[number]')");
  }

  public function send_group_message($data){
       $userid = $_SESSION['account_id'];
       if($data[groups_send] !="")
       {
         foreach($data[groups_send] AS $group)
         {
            $this->db->query("INSERT INTO sent_messages (Text,User_ID,Group_ID) VALUES ('$data[message]','$userid','$group')");
         }
       }
  }

}


?>