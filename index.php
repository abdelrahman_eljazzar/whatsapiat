<?php

require("init.php");

$route = strtolower($_REQUEST['route']);
$route = explode('/', $route);

if(count($route) > 1){
  $module = $route[0];
  $method = $route[1];
}
else{
  $module = $route[0];
  $method = 'index';
}
if(empty($module)){
  $module = 'home';
  $method = 'frontend';
}

/*if(empty($_SESSION['account_id']) && $module != 'login' && $method != 'processpaid' && $method != 'frontend'){
  header('Location: /login');
}*/

if(file_exists(MOD_DIR.'/'.$module.'/'.$module.'.controller.php')){
  include(MOD_DIR.'/'.$module.'/'.$module.'.model.php');
  include(MOD_DIR.'/'.$module.'/'.$module.'.controller.php');
  $Smarty->assign('method', $method);
  $Smarty->assign('module', $module);
  $controller = new $module();
  if(method_exists($controller, $method)){
    $controller->$method();
  }
  else{
    die('Page is not found');
  }
}
else{
  die('Page is not found');
}

?>